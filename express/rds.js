
const mysql = require('mysql2/promise')

const fs = require('fs')

const { Client } = require('ssh2')
const sshClient = new Client()

const RDS = class {
    
  writable() {
    return new WritableClient()
  }

  readable() {
    return new ReadableClient()
  }
}

class RDSClient {

  host() { return '' }
  user() { return '' }
  password() { return '' }
  database() { return '' }
  
  #client = null

  currentClient() {
      return this.#client
  }

  client() {
    return new Promise(async (resolve, reject) => {
      try {
        
        if (process.cwd().includes('karl')) {
          sshClient.on('ready', () => {
            sshClient.forwardOut('127.0.0.1', '3306', this.host(), '3306', async (error, stream) => {
              if (error) {
                reject(error)
              }

              try {

              let connection = await mysql.createConnection({
                ...{
                host: this.host(), 
                  user: this.user(), 
                  password: this.password(), 
                  database: this.database()
                },
                stream
              })

              resolve(connection)

              } catch (e) {
                reject(e)
              }
            })
          }).connect({
            host: '13.209.236.48',
            port: 22,
            username: 'ec2-user',
            privateKey: fs.readFileSync('./ohing-public.pem')
          })
        } else {

          console.log(this.host())
          let client = await mysql.createConnection({host: this.host(), user: this.user(), password: this.password(), database: this.database(), timezone: 'Asia/Seoul'})
          this.#client = client
          resolve(client)
        }
      } catch(e) {
        throw e
      }
    })
  }

  async execute(query, parameters = null) {
  
    try {
        
      if (this.#client == null) {
        this.#client = await this.client()
      }
      // console.log('aaa', this.#client)
      // console.log(this.#client.authorized)

      // if (this.#client.state = 'connected') {
      //   this.#client = await this.client()
      // }

      console.log(this.#client)

      if (parameters == null) {
        const [rows, _] = await this.#client.execute(query)
        return rows
      } else {
        const [rows, _] = await this.#client.query(query, parameters)
        return rows
      }
        
    } catch(e) {

      this.#client = null
      console.log('query error')
      console.log(e)
      throw e
    }
  }

  async beginTransaction() {

    try {

      if (this.#client == null) {
        this.#client = await this.client()
      }

      await this.#client.beginTransaction()

    } catch(e) {
      
      console.log(e)
      throw e
    }
  }
  
  async commit() {

    try {

      if (this.#client == null) {
        return
      }

      await this.#client.commit()

    } catch(e) {
      
      console.log(e)
      throw e
    }
  }

  async rollback() {

    try {

      if (this.#client == null) {
        return
      }

      await this.#client.rollback()

    } catch(e) {
        
        console.log(e)
        throw e
    }
  }

  end() {
  
    if (this.#client != null) {
      this.#client.end()
      this.#client = null
    }
  }
}

class WritableClient extends RDSClient {

  host() { return process.env.stage == 'prod' ? 'ohing-prd-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' : 'ohing-migration-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' }
  user() { return process.env.stage == 'prod' ? 'ohingprdapp' : 'ohing_master' }
  password() { return process.env.stage == 'prod' ? '2021!ohing@SERVICE!SNS' : 'ohing123' }
  database() { return process.env.stage == 'prod' ? 'ohingprd' : 'ohingmig' }
}

class ReadableClient extends RDSClient {

  host() { return process.env.stage == 'prod' ? 'ohing-prd-cluster.cluster-ro-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' : 'ohing-migration-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' }
  user() { return process.env.stage == 'prod' ? 'ohingprdapp' : 'ohing_master' }
  password() { return process.env.stage == 'prod' ? '2021!ohing@SERVICE!SNS' : 'ohing123' }
  database() { return process.env.stage == 'prod' ? 'ohingprd' : 'ohingmig' }
}

module.exports = new RDS()
