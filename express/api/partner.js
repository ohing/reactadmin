var express = require('express');
var router = express.Router();
var axios = require('axios');
var parser = require('node-html-parser')


const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3()
const cloudfront = new AWS.CloudFront();
const lambda = new AWS.Lambda()

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let needsAll = req.query.needsAll

  let limit = ''

  if (needsAll == 0) {

    let page = req.query.currentPage || 0
    let rowCount = req.query.rowCount || 10

    limit = `
      limit
        ${page * rowCount}, ${rowCount}
    `
  }


  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')
    
    query = `
      select sql_calc_found_rows
        producer_id partner_id, name partner_name
      from 
        oi_banner_producer
      order by 
        name asc
      ${limit};
    `
    result = await mysql.execute(query)
    let partners = camelcase(result, {deep: true})

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      partners: partners,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/search', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')
    
    // query = `
    //   select sql_calc_found_rows
    //     tag_val tag, tag_cnt count
    //   from 
    //     oi_tag_info 
    //   order by 
    //     tag_cnt desc,
    //     tag_val asc
    //   limit
    //     ${page * rowCount}, ${rowCount};
    // `
    // result = await mysql.execute(query)
    // let populars = camelcase(result, {deep: true})

    // query = `
    //   select found_rows() rows
    // `
    // result = await mysql.execute(query)
    // let rows = result[0].rows

    res.send(JSON.stringify({
      keywords: [],
      totalCount: 0
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/banned', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let key = req.query.key

  console.log('key', key)

  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')

    let where = ''

    let keyMap = {
      'ㄱ': ['^(ㄱ|ㄲ)', '가', '나'],
      'ㄴ': ['^ㄴ', '나', '다'],
      'ㄷ': ['^(ㄷ|ㄸ)', '다', '라'],
      'ㄹ': ['^ㄹ', '라', '마'],
      'ㅁ': ['^ㅁ', '마', '바'],
      'ㅂ': ['^(ㅂ|ㅃ)', '바', '사'],
      'ㅅ': ['^(ㅅ|ㅆ)', '사', '아'],
      'ㅇ': ['^ㅇ', '아', '자'],
      'ㅈ': ['^(ㅈ|ㅉ)', '자', '차'],
      'ㅊ': ['^ㅊ', '차', '카'],
      'ㅋ': ['^ㅋ', '카', '타'],
      'ㅌ': ['^ㅌ', '타', '파'],
      'ㅍ': ['^ㅍ', '파', '하'],
      'ㅎ': ['^ㅎ', '하', '！']
    }

    if (key != null) {

      let values = keyMap[key]

      console.log('values', values)

      if (values != undefined) {
        where = `
          where
            search_text rlike '${values[0]}' or (search_text >= '${values[1]}' and search_text < '${values[2]}')
        `
      } else {
        where = `
          where
            lower(search_text) like '${key.toLowerCase()}%'
        `
      }
    }

    
    query = `
      select sql_calc_found_rows
        search_text, reg_dt
      from 
        oi_banned_search_text
      ${where}
      order by 
        search_text asc
    ` 
    result = await mysql.execute(query)
    let banneds = camelcase(result, {deep: true})

    query = `
      select found_rows() rows;
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      banneds: banneds,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/banned', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let keyword = req.body.keyword

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      insert ignore into oi_banned_search_text (
        banned_id, search_text, reg_user_id
      ) values (
        md5('${keyword}'), '${keyword}', ${userId}
      );
    ` 
    result = await mysql.execute(query)
    let affectedRows = result.affectedRows

    res.send(JSON.stringify({added: affectedRows == 1}))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/banned', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let keywords = req.body.keywords

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')

    if (keywords.length > 0) {

      let ids = keywords.map(keyword => {
        return `md5('${keyword}')`
      })
      query = `
        delete from
          oi_banned_search_text
        where
          banned_id in (${ids.join(',')})
      `  
    }
    
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/tag/:tag', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.params)

  let userId = req.headers.userId
  let tag = req.params.tag

  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select
        user.count user_count, feed.count feed_count, chat.count chat_count, tag_user.count tag_user_count
      from
        (select 
          count(*) count
        from 
          oi_user_tag ut
          join oi_tag_info ti on ut.tag_id = ti.tag_id
        where 
          ti.tag_val = '${tag}') user,
        (select 
          count(*) count
        from 
          oi_feed_tag ft
          join oi_tag_info ti on ft.tag_id = ti.tag_id
        where 
          ti.tag_val = '${tag}') feed,
        (select 
          count(*) count
        from 
          oi_chat_room_tag crt
          join oi_tag_info ti on crt.tag_id = ti.tag_id
        where 
          ti.tag_val = '${tag}') chat,
        (select
          count(*) count
        from
          (select 
            ut.user_id
          from 
            oi_user_tag ut
            join oi_tag_info ti on ut.tag_id = ti.tag_id
          where 
            ti.tag_val = '${tag}'
          group by
            ut.user_id
          union
              select 
            f.reg_user_id user_id
          from 
            oi_feed_tag ft
            join oi_tag_info ti on ft.tag_id = ti.tag_id
            join oi_feed_info f on ft.feed_id = f.feed_id
          where 
            ti.tag_val = '${tag}'
          group by
            f.reg_user_id
          union
              select 
            cui.user_id
          from 
            oi_chat_room_tag crt
            join oi_tag_info ti on crt.tag_id = ti.tag_id
            join oi_chat_user_info cui on crt.room_id = cui.room_id and cui.master_yn = 'Y'
          where 
            ti.tag_val = '${tag}'
          group by
            cui.user_id
          ) t) tag_user;
    `

    result = await mysql.execute(query)

    let tagInfo = camelcase(result[0], {deep: true})

    res.send(JSON.stringify(tagInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

module.exports = router