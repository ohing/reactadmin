import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  CardText,
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class PartnerPage extends React.Component {

  koreanKeys = [
    'ㄱ', 'ㄴ', 'ㄷ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅅ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'
  ]
  alphbetKeys = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
  ]

  state = {
    
    key: null,
    banneds: null,

    selectedKeywords: new Set(),

    addModal: false,
    addKeyword: '',

    deleteModal: false
  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 508) / 64 + 1) 
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let parameters = {}
      if (this.state.key != null) {
        parameters.key = this.state.key
      }
      console.log('dddd', parameters)

      let result = await api.keyword().call({apiName: 'getBannedKeywords', method: 'get', parameters: parameters})

      let totalCount = result.totalCount
      let banneds = result.banneds

      this.setState({
        totalCount: totalCount,
        banneds: banneds,
        selectedKeywords: new Set()
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handleAddButton = () => {
    this.toggleAddModal()() 
  }

  toggleAddModal = modalType => () => {
    if (!modalType) {
      this.setState({
        addModal: !this.state.addModal,
      })
    }
  }

  handleAddKeyword = event => {
    this.setState({
      addKeyword: event.target.value
    })
  }

  handleAddKeyDown = event => {
    if (event.key === 'Enter') {
      this.handleAdd()
    }
  }

  handleAdd = async () => {

    let keyword = this.state.addKeyword.trim()

    console.log(keyword)

    if (keyword.length == 0) {
      this.props.addNotification('추가할 금칙어를 입력해 주세요.')
      return
    }

    try {

      let result = await api.keyword().call({apiName: 'createBannedKeyword', method: 'put', parameters: {
        keyword: keyword
      }})

      if (result.added) {
        this.props.addNotification('금칙어가 추가되었습니다.')
        this.setState({
          addKeyword: '',
          addModal: false
        })
        this.getData()
      } else {
        this.props.addNotification('이미 추가된 금칙어입니다.')
      }

    } catch (e) {
      console.error(e)
    } 
  }
  
  handleDeleteButton = () => {
    if (this.state.selectedKeywords.size > 0) {
      this.toggleDeleteModal()()
    } else {
      this.props.addNotification('삭제할 금칙어를 선택해 주세요.')
    }
  }

  toggleDeleteModal = modalType => () => {
    if (!modalType) {
      this.setState({
        deleteModal: !this.state.deleteModal,
      })
    }
  }

  handleDelete = async () => {

    try {

      let keywords = Array.from(this.state.selectedKeywords)

      let result = await api.keyword().call({apiName: 'deleteBannedKeywords', method: 'delete', parameters: {
        keywords: keywords
      }})

      this.props.addNotification('삭제되었습니다.')
      this.setState({
        deleteModal: false
      })

      this.getData()

    } catch (e) {
      console.error(e)
    } 
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        
        <Row key='A'>
          <Col>
            {
              this.state.banneds == null ? '' : 
              <Card className="mb-3">
                <CardHeader>등록된 금칙어 총 <span className="text-danger">{this.state.totalCount}</span>개</CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Card body className="mb-3">
                        <Row style={{height:'40px', marginTop: '-15px'}}>
                          <CardBody>
                            <ButtonGroup>
                              <Button
                                color="dark"
                                outline
                                onClick={() => {
                                  this.setState({ key: null }, () => { this.getData() })
                                }}
                                active={this.state.key === null}
                              >
                              전체
                              </Button>
                              {
                                this.koreanKeys.map( (key, index) => {

                                  return (
                                    <Button
                                      color="dark"
                                      outline
                                      onClick={() => {
                                        this.setState({ key: key }, () => { this.getData() })
                                      }}
                                      active={this.state.key === key}
                                    >
                                    {key}
                                    </Button>
                                  )
                                })
                              }
                            </ButtonGroup>
                          </CardBody>
                        </Row>
                        <Row className="mb-3" style={{height:'40px'}}>
                          <CardBody>
                            <ButtonGroup>
                            {
                              this.alphbetKeys.map( (key, index) => {

                                return (
                                  <Button
                                    color="dark"
                                    outline
                                    onClick={() => {
                                      this.setState({ key: key }, () => { this.getData() })
                                    }}
                                    active={this.state.key === key}
                                  >
                                  {key}
                                  </Button>
                                )
                              })
                            }
                            </ButtonGroup>
                            
                          </CardBody>
                        </Row>

                      </Card>
                      <Card className="mb-3">
                        <Row className="form-check-inline ml-3 my-3">
                          {
                            this.state.banneds.length == 0 ? 
                            <CardText>
                              등록된 금칙어가 없습니다.
                            </CardText> 
                            :
                            this.state.banneds.map( keyword => {
                              return (
                                <Button
                                  className="mx-2 my-1"
                                  outline
                                  onClick={() => {
                                    let selected = this.state.selectedKeywords
                                    if (selected.has(keyword.searchText)) {
                                      selected.delete(keyword.searchText)
                                    } else {
                                      selected.add(keyword.searchText)
                                    }
                                    this.setState({
                                      selectedKeywords: selected
                                    })
                                  }}
                                  active={this.state.selectedKeywords.has(keyword.searchText)}
                                >
                                {keyword.searchText}
                                </Button>
                              )
                            })
                          }
                        </Row>
                      </Card>
                      <Button className="mx-1" color="secondary" onClick={this.handleAddButton}>추가</Button>
                      <Button className="mx-1" color="primary" onClick={this.handleDeleteButton}>삭제</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            }
          </Col>
        </Row>

        <Modal
          isOpen={this.state.addModal}
          toggle={this.toggleAddModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleAddModal()}>{'금칙어 추가'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">금칙어를 추가합니다.</Label>
            <Form>
              <Input type="text" value={this.state.addKeyword} onChange={this.handleAddKeyword} onKeyDown={this.handleAddKeyDown}/>
              <Input type="text" hidden={true}/>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleAddModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleAdd}>
              추가
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>{'금칙어 삭제'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택된 금칙어를 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDelete}>
              삭제
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default PartnerPage;
