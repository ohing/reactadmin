import React from 'react';
import Avatar from 'components/Avatar';

import defaultProfileImage from 'assets/img/users/default_200.png';
import defaultBackgroundImage from 'assets/img/users/default_profile_bg.png';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  CardImg,
  Col, 
  Row, 
  // Form,
  FormGroup,
  // FormFeedback,
  Label,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class UserPage extends React.Component {

  state = {
    accountId: '',
    type: '',
    status: '',
    isBlackList: false,
    openType: null,
    createdAt: null,
    lastDevice: null,
    profileImageUrl: null,
    backgroundImageUrl: null,
    name: null,
    nickname: null,
    sex: null,
    email: null,
    phoneNumber: null,
    relation: null,
    reported: null,
    tags: [],
    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
    memoText: '',
    blocks: [],
    memos: [],
    modal: false,
    blockReason: '',
    blockDays: 1,
    blackListModal: false,
    blackListReason: '',
    imageModal: false,
    imageModalType: '',
    imageBlockModal: false,
    imageBlockReason: '',
    sendSMSModal: false,
    sendSMS: '',
    sendEmailModal: false,
    sendEmail: '',
  };

  targetUserId = this.props.match.params.userId
  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {
    console.log('DidMount')
    console.log(this.props)

    this.getData()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevProps.match.params.userId != this.props.match.params.userId) {
      this.targetUserId = this.props.match.params.userId
      window.scrollTo(0, 0);
      this.getData()
    }
  }

  userTypeString = (type) => {
    
    switch (type) {
      case 'SUPER':
        return '슈퍼관리자'
      case 'OHING':
        return '일반관리자'
      case 'TESTER':
        return '테스터'
      default:
        return '유저'
    }
  }

  userStatusString = (status) => {
    switch (status) {
      case 'ACTIVE':
        return '정상'
      case 'BLOCK':
        return '차단됨'
      default:
        return '이상'
    }
  }

  openTypeString = (openType) => {
    switch (openType) {
      case 'OPEN':
        return '전체공개'
      case 'FRIEND':
        return '팔로워공개'
      case 'CLOSE':
        return '비공개'
      default:
        return '이상'
    }
  }

  createdAtString = (createdAt) => {
    createdAt = createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')
    return createdAt
  }

  sexString = (sex) => {
    return sex == 'M' ? '남자' : '여자'
  }

  birthdayString = (userInfo) => {
    let year = userInfo.birthY
    let md = userInfo.birthMd || ''
    let month = md.substring(0, 2)
    let day = md.substring(2, 4)
    return `${year}.${month}.${day}`
  }

  phoneNumberString = (userInfo) => {
    if (userInfo.userHpNo == null || userInfo.userHpNo == 'null') {
      return null
    }
    let phoneNumber = userInfo.userHpNo
    phoneNumber = phoneNumber.length < 11 ? phoneNumber.replace(/(\d{3})(\d{3})(\d+)/, '$1-$2-$3') : phoneNumber.replace(/(\d{3})(\d{4})(\d+)/, '$1-$2-$3')
    return phoneNumber
  }

  relationString = (userInfo) => {
    return (
      <div>
        팔로워: <Label className="text-danger">{userInfo.followerCount}</Label>,
        팔로잉: <Label className="text-danger">{userInfo.followingCount}</Label>,
        찜: <Label className="text-danger">{userInfo.jjimCount}</Label>, 
        찜 당함: <Label className="text-danger">{userInfo.jjimedCount}</Label>
      </div>
    )
  }

  reportedString = (userInfo) => {
    return (
      userInfo.reportedCount > 0 
      ? <a href="#" onClick={this.handleReported}>{`${userInfo.uncompletedReportCount}/${userInfo.reportedCount}`}</a>
      : '0/0'
    )
  }

  getData = async () => {
    
    let result = await api.user().call({apiName: 'getUser', pathParameter: this.targetUserId, method: 'get'})

    let profileImageUrl = result.profileFileName == null ? null : `${this.mediaCloudfrontUrl}/img/${result.profileFileName.split('.')[0]}/${result.profileFileName}`
    let backgroundImageUrl = result.backgroundFileName == null ? null : `${this.mediaCloudfrontUrl}/img/${result.backgroundFileName.split('.')[0]}/${result.backgroundFileName}`
    let type = this.userTypeString(result.type)
    let status = this.userStatusString(result.status)
    let openType = this.openTypeString(result.openType)
    let createdAt = this.createdAtString(result.createdAt)
    let sex = this.sexString(result.sex)
    let birthday = this.birthdayString(result)
    let phoneNumber = this.phoneNumberString(result)
    let relation = this.relationString(result)
    let reported = this.reportedString(result)
    

    this.setState({
      accountId: `${result.accountId} (${result.userId})`,
      profileImageUrl: profileImageUrl,
      backgroundImageUrl: backgroundImageUrl,
      type: type,
      status: status,
      isBlackList: result.isBlackList,
      openType: openType,
      createdAt: createdAt,
      lastDevice: result.osTypeCd,
      name: result.name,
      nickname: result.nickname,
      sex: sex,
      birthday: birthday,
      email: result.userEmail,
      phoneNumber: phoneNumber,
      relation: relation,
      reported: reported,
      tags: result.tags,
      blocks: result.blocks,
      memos: result.memos,
      modal: false,
      blocksModal: false,
    })
  }

  handleBackgroundImage = () => {

    if ((this.state.backgroundImageUrl || '').trim().length == 0) { 
      return
    }

    this.setState({
      imageModalType: 'Background'
    })
    this.toggleImageModal()()
  }

  handleProfileImage = () => {

    console.log(this.state.profileImageUrl)

    if ((this.state.profileImageUrl || '').trim().length == 0) { 
      return
    }

    this.setState({
      imageModalType: 'Profile'
    })
    this.toggleImageModal()()
  }

  handleEmail = event => {
    event.preventDefault()
    let email = event.target.getAttribute('value')
    console.log(email)
  }

  handlePhoneNumber = event => {
    event.preventDefault()
    this.toggleSendSMSModal()()
  }

  handleTag = event => {
    event.preventDefault()
    let tag = event.target.getAttribute('value')
    this.setState({
      currentTag: tag
    }, () => {
      this.toggleDeleteTagModal()()
    })
  }

  handleMemo = event => {
    this.setState({
      memoText: event.target.value
    })
  }

  handleMemoRegister = async () => {
    let memo = this.state.memoText.trim()

    if (memo.length == 0) {
      this.props.addNotification('메모를 입력해 주세요.')
      return
    }

    try {
      let parameters = {
        targetType: 'USER',
        targetId: this.targetUserId,
        memo: memo
      }
      let result = await api.memo().call({apiName: 'registerMemo', method: 'put', parameters: parameters})

      let memos = [result, ...this.state.memos]

      this.setState({
        memos: memos,
        memoText: ''
      })

    } catch (e) {

      console.log(e)
    }
  }

  handleMemoUser = event => {
    
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))

    console.log(targetUserId)

    this.props.history.push(`/user/${targetUserId}`) 
  }

  toggleBlackListModal = modalType => () => {

    if (!modalType) {
      this.setState({
        blackListModal: !this.state.blackListModal,
      })
    }
  }

  handleBlackListReason = event => {
    this.setState({
      blackListReason: event.target.value
    })
  }

  handleBlackList = async () => {

    try {

      let apiName = this.state.isBlackList ? 'removeBlackList' : 'setBlackList'
      let method = this.state.isBlackList ? 'delete' : 'put'

      let nextStatus = !this.state.isBlackList
      let message = this.state.isBlackList ? '블랙리스트 해제되었습니다.' : '블랙리스트 등록되었습니다.'

      let result = await api.user().call({apiName: apiName, method: method, pathParameter: this.targetUserId, parameters: {reason: this.state.blackListReason}})
      let memos = [result, ...this.state.memos]

      let nextState = {
        blackListModal: false,
        memos: memos,
        blackListReason: '',
        isBlackList: nextStatus,
      }

      if (!this.state.isBlackList) {
        nextState.status = '차단됨'
      }

      this.setState(nextState)
       
      this.props.addNotification(message)

    } catch (e) {
      console.error(e)
    }
  }

  toggleBlock = () => {

    if (this.state.isBlackList) {
      this.props.addNotification('블랙리스트 상태의 유저는 차단상태를 변경할 수 없습니다.')
      return
    }

    this.toggleBlockModal()()
  }

  toggleBlockModal = modalType => () => {

    if (!modalType) {
      this.setState({
        modal: !this.state.modal,
      })
    }
  }

  handleBlockReason = event => {
    this.setState({
      blockReason: event.target.value
    })
  }

  handleBlockDays = event => {
    let targetValue = event.target.value
    let value
    if (targetValue.length > 0) {
      value = parseInt(targetValue) || 1
    } else {
      value = targetValue
    }
    this.setState({
      blockDays: value
    })
  }

  handleBlock = async () => {

    try {

      let apiName = this.state.status === '차단됨' ? 'unblockUser' : 'blockUser'
      let method = this.state.status === '차단됨' ? 'delete' : 'put'

      let nextStatus = this.state.status === '차단됨' ? '정상' : '차단됨'
      let message = this.state.status === '차단됨' ? '차단 해제되었습니다.' : '차단되었습니다.'

      let result = await api.user().call({apiName: apiName, method: method, pathParameter: this.targetUserId, parameters: {reason: this.state.blockReason, days: this.state.blockDays}})
      let blocks = result.blockInfo != null ? [...this.state.blocks, result.blockInfo] : this.state.blocks
      let memos = [result.memoInfo, ...this.state.memos]

      this.setState({
        modal: false,
        memos: memos,
        blocks: blocks,
        blockReason: '',
        blockDays: 1,
        status: nextStatus,
      })
       
      this.props.addNotification(message)

    } catch (e) {
      console.error(e)
    }
  }

  toggleBlocksModal = modalType => () => {

    if (!modalType) {
      this.setState({
        blocksModal: !this.state.blocksModal,
      })
    }
  }

  handleBlocks = async () => {
    this.toggleBlocksModal()()
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  toggleImageBlockModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageBlockModal: !this.state.imageBlockModal,
      })
    }
  }

  handleImageBlockReason = event => {
    this.setState({
      imageBlockReason: event.target.value
    })
  }

  handleImageBlock = async () => {

    try {

      let url = this.state.imageModalType === 'Profile' ? this.state.profileImageUrl : this.state.backgroundImageUrl
      let result = await api.user().call({apiName: 'imageBlock', method: 'put', pathParameter: this.targetUserId, parameters: {type: this.state.imageModalType, url: url, reason: this.state.imageBlockReason}})
      let memos = [result, ...this.state.memos]

      let updates = {
        imageModal: false,
        imageBlockModal: false,
        memos: memos,
        imageBlockReason: '',
        imageModalType: '',
      }

      if (this.state.imageModalType === 'Profile') {
        updates.profileImageUrl = null
      } else {
        updates.backgroundImageUrl = null
      }

      this.setState(updates)

      this.props.addNotification('이미지가 차단되었습니다.')
       
    } catch (e) {
      console.error(e)
    }
  }

  toggleSendSMSModal = modalType => () => {
    if (!modalType) {

      this.setState({
        sendSMSModal: !this.state.sendSMSModal,
      })
    }
  }

  handleSMSMessage = event => {
    this.setState({
      sendSMS: event.target.value
    })
  }

  handleSendSMS = async () => {

    let smsMessage = this.state.sendSMS.trim()
    if (smsMessage.length == 0) {
      return
    }

    let parameters = {
      content: smsMessage,
      recipients: [
        {
          to: this.state.phoneNumber.replace(/-/gi, '')
        }
      ]
    }

    try {
      let result = await api.util().call({apiName: 'sendSMS', method: 'post', parameters: parameters})    
      console.log(result)
      
      this.toggleSendSMSModal()()
      this.props.addNotification('전송되었습니다.')

    } catch (e) {
      console.error(e)
      this.props.addNotification('SMS 전송이 실패하였습니다. 서버 관리자에게 문의해 주세요.')
    }
  }

  toggleDeleteTagModal = modalType => () => {
    if (!modalType) {

      this.setState({
        deleteTagModal: !this.state.deleteTagModal,
      })
    }
  }

  handleDeleteTag = async () => {

    try {

      let parameters = {
        tag: this.state.currentTag
      }
      let result = await api.user().call({apiName: 'deleteTag', method: 'delete', pathParameter: this.targetUserId, parameters: parameters})    
      let memoInfo = result.memoInfo
      let tags = result.tags
      console.log(result)

      let memos = [memoInfo, ...this.state.memos]

      this.setState({
        memos: memos,
        tags: tags,
        currentTag: null,
        deleteTagModal: false,
      }, () => {
        this.props.addNotification('삭제되었습니다.')
      })
      
    } catch (e) {
      console.error(e)
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="UserPage"
      >
        <Row key='A'>
          <Col>
            {
              this.state.status === '' ? '' :
              <Card className="mb-3">
                <CardHeader>유저 정보</CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Row>
                        <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                          아이디
                        </Label>
                        <Label for="ID" className="h-1" sm={7}>
                          {this.state.accountId}
                        </Label>
                        <Button className="mx-3" color={this.state.isBlackList ? 'warning' : 'dark'} onClick={this.toggleBlackListModal()}>{this.state.isBlackList ? '블랙리스트 해제' : '블랙리스트 등록'}</Button>
                      </Row>
                      <Row>
                        <Label for="TypeHeader" className="font-weight-bold h-1" sm={3}>
                          타입
                        </Label>
                        <Label for="Type" className="h-1" sm={7}>
                          {this.state.type}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="StatusHeader" className="font-weight-bold h-1" sm={3}>
                          계정 상태
                        </Label>
                        {
                          this.state.blocks.length == 0 ? 
                          <Label for="Status" className="h-1" sm={7}>
                            {this.state.status}
                          </Label> :
                          <Label for="Status" className="h-1" sm={7}>
                            <a href="#" onClick={this.handleBlocks} value={this.state.status}>{this.state.status}</a>
                          </Label>
                        }
                        <Button className="mx-3" color={this.state.status === '차단됨' ? 'warning' : 'dark'} onClick={this.toggleBlock}>{this.state.status === '차단됨' ? '차단 해제' : '차단'}</Button>
                      </Row>
                      <Row>
                        <Label for="OpenTypeHeader" className="font-weight-bold h-1" sm={3}>
                          프로필 공개상태
                        </Label>
                        <Label for="OpenType" className="h-1" sm={7}>
                          {this.state.openType}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="CreatedAtHeader" className="font-weight-bold h-1" sm={3}>
                          가입일시
                        </Label>
                        <Label for="CreatedAt" className="h-1" sm={7}>
                          {this.state.createdAt}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="LastDeviceHeader" className="font-weight-bold h-1" sm={3}>
                          마지막 접속 OS
                        </Label>
                        <Label for="LastDevice" className="h-1" sm={7}>
                          {this.state.lastDevice}
                        </Label>
                      </Row>
                      <br/>
                      <Row>
                        <Label for="NameHeader" className="font-weight-bold h-1" sm={2}>
                          이름
                        </Label>
                        <Label for="Name" className="h-1" sm={4}>
                          {this.state.name}
                        </Label>
                        <Label for="NicknameHeader" className="font-weight-bold h-1" sm={2}>
                          닉네임
                        </Label>
                        <Label for="Nickname" className="h-1" sm={4}>
                          {this.state.nickname}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="SexHeader" className="font-weight-bold h-1" sm={2}>
                          성별
                        </Label>
                        <Label for="Sex" className="h-1" sm={4}>
                          {this.state.sex}
                        </Label>
                        <Label for="BirthdayHeader" className="font-weight-bold h-1" sm={2}>
                          생년월일
                        </Label>
                        <Label for="Birthday" className="h-1" sm={4}>
                          {this.state.birthday}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="EmailHeader" className="font-weight-bold h-1" sm={2}>
                          이메일
                        </Label>
                        <Label for="Email" className="h-1" sm={4}>
                          <a href="#" onClick={this.handleEmail} value={this.state.email}>{this.state.email}</a>
                        </Label>
                        <Label for="PhoneNumberHeader" className="font-weight-bold h-1" sm={2}>
                          휴대전화번호
                        </Label>
                        <Label for="PhoneNumber" className="h-1" sm={4}>
                          {
                            this.state.phoneNumber == null ? '' : 
                            (<a href="#" onClick={this.handlePhoneNumber} value={(this.state.phoneNumber || '').replace(/-/gi, '')}>{this.state.phoneNumber}</a>)
                          }
                        </Label>
                      </Row>
                      <Row>
                        <Label for="RelationHeader" className="font-weight-bold h-1" sm={2}>
                          관계
                        </Label>
                        <Label for="Relation" className="h-1" sm={4}>
                          {this.state.relation}
                        </Label>
                        <Label for="RelationHeader" className="font-weight-bold h-1" sm={2}>
                          신고당함
                        </Label>
                        <Label for="Relation" className="h-1" sm={4}>
                          {this.state.reported}
                        </Label>
                      </Row>
                      <Row>
                        <Label for="TagHeader" className="font-weight-bold h-1" sm={2}>
                          관심사 태그
                        </Label>

                        <Label for="Tag" className="h-1" sm={7}>
                        {
                          this.state.tags.map( (tag, i) => {
                            return (
                              <a key={tag} className="mr-1" href="#" value={tag} onClick={this.handleTag}>#{tag}</a>
                            )
                          })
                        } 
                        </Label>
                      </Row>

                    </Col>
                    <Col sm={3}>
                    
                      <CardImg
                        className="card-img ml-2 mt-2 can-click"
                        src={this.state.backgroundImageUrl != null ? this.state.backgroundImageUrl : defaultBackgroundImage}
                        style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                        onClick={this.handleBackgroundImage}
                      />
                      <Avatar
                        src={this.state.profileImageUrl != null ? this.state.profileImageUrl : defaultProfileImage}
                        onClick={this.toggleUserCardPopover}
                        size={100}
                        className="can-click ml-2 mt-2"
                        onClick={this.handleProfileImage}
                      />

                    </Col>
                  </Row>

                </CardBody>

                <Modal
                  isOpen={this.state.blackListModal}
                  toggle={this.toggleBlackListModal()}
                  className={this.props.className}>
                  <ModalHeader toggle={this.toggleBlackListModal()}>{this.state.isBlackList ? '블랙리스트 해제' : '블랙리스트 등록'}</ModalHeader>
                  <ModalBody>
                    <Label>* 블랙리스트의 경우 본인 명의의 모든 계정이 차단되며, 새로 가입도 불가능합니다.</Label>
                    <br/>
                    <Label>{this.state.isBlackList ? '이 유저를 블랙리스트 해제하시겠습니까?' : '이 유저를 블랙리스트 등록하시겠습니까?'}</Label>
                    <Input type="text" className="my-3" id="blackListReason" placeholder={this.state.isBlackList ? '해제 사유를 입력해주세요.' : '등록 사유를 입력해주세요.'} onChange={this.handleBlackListReason} value={this.state.blackListReason}/>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={this.toggleBlackListModal()}>
                      취소
                    </Button>
                    <Button color={this.state.isBlackList ? 'warning' : 'dark'} onClick={this.handleBlackList}>
                      {this.state.isBlackList ? '해제' : '등록'}
                    </Button>{' '}
                    
                  </ModalFooter>
                </Modal>

                <Modal
                  isOpen={this.state.modal}
                  toggle={this.toggleBlockModal()}
                  className={this.props.className}>
                  <ModalHeader toggle={this.toggleBlockModal()}>{this.state.status === '차단됨' ? '유저 차단 해제' : '유저 차단'}</ModalHeader>
                  <ModalBody>
                    <Label>* 차단의 경우 본인의 다른 계정은 계속 사용할 수 있습니다.</Label>
                    <br/>
                    <Label>{this.state.status === '차단됨' ? '이 유저를 차단 해제하시겠습니까?' : '이 유저를 차단하시겠습니까?'}</Label>
                    {
                      this.state.status === '차단됨' ? 
                      <></> :
                      <>
                        <Input type="number" className="mt-3" id="blockDays" placeholder="차단 일수를 입력해 주세요." onChange={this.handleBlockDays} value={this.state.blockDays}/>
                        <Label>{'차단일수 경과 후, 다음 날 새벽 1시에 차단이 해제됩니다.'}</Label>
                      </>
                    }
                    <Input type="text" className="my-3" id="blockReason" placeholder={this.state.status === '차단됨' ? '차단 해제 사유를 입력해주세요.' : '차단 사유를 입력해주세요.'} onChange={this.handleBlockReason} value={this.state.blockReason}/>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={this.toggleBlockModal()}>
                      취소
                    </Button>
                    <Button color={this.state.status === '차단됨' ? 'warning' : 'dark'} onClick={this.handleBlock}>
                      {this.state.status === '차단됨' ? '차단 해제' : '차단'}
                    </Button>{' '}
                    
                  </ModalFooter>
                </Modal>

                <Modal
                  isOpen={this.state.blocksModal}
                  toggle={this.toggleBlocksModal()}
                  className={this.props.className}>
                  <ModalHeader toggle={this.toggleBlocksModal()}>{'차단 내역'}</ModalHeader>
                  <ModalBody>
                    <Table {...{ ['hover']: false }}>
                      <thead>
                        <tr>
                          <th>차단일수</th>
                          <th>사유</th>
                          <th>등록일</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.state.blocks.map( (block, i) => {
                            return (
                              <tr key={block.blockSeq}>
                                <td>{block.blockDays}</td>
                                <td>{block.reason}</td>
                                <td>{(block.regDt || '').replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </Table> 
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={this.toggleBlocksModal()}>
                      확인
                    </Button>
                    
                  </ModalFooter>
                </Modal>

              </Card>
            }

            <Card className="mb-3">
            <CardHeader>메모 등록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label>* 메모는 한 번 등록하면 삭제할 수 없습니다.</Label> <Button className="float-right mx-1 mb-3" color="secondary" onClick={this.handleMemoRegister}>등록</Button>
                      <Input type="textarea" name="text" value={this.state.memoText} onChange={this.handleMemo} placeholder="메모를 입력해 주세요."/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card className="mb-3">
            <CardHeader>메모</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    {
                      this.state.memos.map( (memo, i) => {

                        return (
                          <div key={memo.memoId}>
                            <Label><a href="#" onClick={this.handleMemoUser} value={memo.userId}>{memo.userLoginId}</a></Label>
                            <Label>&nbsp;&nbsp;{memo.regDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</Label>
                            <br/>
                            <Label dangerouslySetInnerHTML={{__html: memo.memo}}></Label>
                            <br/>
                            <br/>
                          </div>
                        )
                      })
                    }
                  </Col>
                </Row>
              </CardBody>
            </Card>

          </Col>
        </Row>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>{this.state.imageModalType === 'Profile' ? '프로필 이미지' : '배경 이미지'}</ModalHeader>
          <ModalBody>
            <CardImg
              className="card-img ml-2 mt-2 can-click"
              src={this.state.imageModalType === 'Profile' ? this.state.profileImageUrl : this.state.backgroundImageUrl}
              style={{ width: 450, height: 'auto'}}
            />
            <div className="mt-3">
              <Button className="ml-2" color="dark" onClick={this.toggleImageBlockModal()}>
                차단
              </Button>
              <Label className="ml-2">위 이미지를 차단합니다.</Label>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.imageBlockModal}
          toggle={this.toggleImageBlockModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageBlockModal()}>{this.state.imageModalType === 'Profile' ? '프로필 이미지 차단' : '배경 이미지 차단'}</ModalHeader>
          <ModalBody>
            <Label className="ml-2">이미지를 차단할 경우 해당 유저에게 즉시 반영되며, 복구할 수 없습니다.</Label>
            <Input type="text" className="my-3" id="imageBlockReason" placeholder="차단 사유를 입력해주세요." onChange={this.handleImageBlockReason} value={this.state.imageBlockReason}/>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleImageBlockModal()}>
              취소
            </Button>
            <Button color="dark" onClick={this.handleImageBlock}>
              {'차단'}
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.sendSMSModal}
          toggle={this.toggleSendSMSModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleSendSMSModal()}>{'SMS 발송'}</ModalHeader>
          <ModalBody>
            <Label className="ml-2">해당 유저에게 SMS를 발송합니다.(이모지 불가)</Label>
            <Input type="textarea" className="my-3" id="smsMessage" placeholder="" onChange={this.handleSMSMessage} value={this.state.sendSMS}/>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSendSMSModal()}>
              취소
            </Button>
            <Button color="dark" onClick={this.handleSendSMS}>
              {'전송'}
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.deleteTagModal}
          toggle={this.toggleDeleteTagModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteTagModal()}>{'태그 삭제'}</ModalHeader>
          <ModalBody>
            <Label className="ml-2">{`태그 [${this.state.currentTag}]를 삭제합니다.`}</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteTagModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDeleteTag}>
              {'삭제'}
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

      </Page>
    ); 
  }
};


export default UserPage;
