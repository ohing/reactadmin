import logo300Image from 'assets/img/logo/logo_300.png';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';

import API from 'utils/api'
const api = new API()

class AuthForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }
  
  // get isLogin() {
  //   return this.props.authState === STATE_LOGIN;
  // }

  // get isSignup() {
  //   return this.props.authState === STATE_SIGNUP;
  // }

  // changeAuthState = authState => event => {
  //   event.preventDefault();

  //   this.props.onChangeAuthState(authState);
  // };

  handleUsernameChange = event => {
    this.setState({ inputId: event.target.value })
  }

  handlePasswordChange = event => {
    this.setState({ inputPassword: event.target.value })
  }

  handleKeyUp = event => {
    if (event.key === 'Enter') {
      this.checkFields()
    }
  }

  handleSubmit = event => {
    event.preventDefault();
    this.checkFields()
  }

  checkFields = () => {

    let id = (this.state.inputId || '').trim()
    let password = (this.state.inputPassword || '').trim()

    if (id.length === 0 || password.length === 0) {
      alert('아이디와 비밀번호를 모두 입력해 주세요.')
    }

    this.doLogin({id: id, password: password})
  }
  
  doLogin = async (parameters: Object) => {

    try {

      let result = await api.account().call({apiName: 'signIn', method: 'post', parameters: parameters})
      let token = result.token
      let userInfo = result.userInfo

      if (token != null && userInfo != null) {

        window.localStorage.setItem('token', token)
        window.localStorage.setItem('userInfo', JSON.stringify(userInfo))
        window.location.href = '/'
        
      } else {

        alert('관리자 정보를 찾을 수 없습니다.')
      }

    } catch (e) {
      console.error(e)
      alert('오류')
    }
  }

  renderButtonText() {
    const { buttonText } = this.props;

    // if (!buttonText && this.isLogin) {
    //   return 'Login';
    // }

    if (!buttonText) {
      return '로그인';
    }

    // if (!buttonText && this.isSignup) {
    //   return 'Signup';
    // }

    return buttonText;
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      // confirmPasswordLabel,
      // confirmPasswordInputProps,
      children,
      onLogoClick,
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        {showLogo && (
          <div className="text-center pb-4">
            <img
              src={logo300Image}
              className="rounded"
              style={{ width: 100, height: 40, cursor: 'pointer' }}
              alt="logo"
              onClick={onLogoClick}
            />
          </div>
        )}
        <FormGroup>
          <Label for={usernameLabel}>{usernameLabel}</Label>
          <Input {...usernameInputProps} onChange={this.handleUsernameChange} onKeyUp={this.handleKeyUp} />
        </FormGroup>
        <FormGroup>
          <Label for={passwordLabel}>{passwordLabel}</Label>
          <Input {...passwordInputProps} onChange={this.handlePasswordChange} onKeyUp={this.handleKeyUp}/>
        </FormGroup>
        {/* {this.isSignup && (
          <FormGroup>
            <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
            <Input {...confirmPasswordInputProps} />
          </FormGroup>
        )}
        <FormGroup check>
          <Label check>
            <Input type="checkbox" />{' '}
            {this.isSignup ? 'Agree the terms and policy' : 'Remember me'}
          </Label>
        </FormGroup> */}
        <hr />
        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={this.handleSubmit}>
          {this.renderButtonText()}
        </Button>

        {/* <div className="text-center pt-1">
          <h6>or</h6>
          <h6>
            {this.isSignup ? (
              <a href="#login" onClick={this.changeAuthState(STATE_LOGIN)}>
                Login
              </a>
            ) : (
              <a href="#signup" onClick={this.changeAuthState(STATE_SIGNUP)}>
                Signup
              </a>
            )}
          </h6>
        </div> */}

        {children}
      </Form>
    );
  }
}

export const STATE_LOGIN = 'LOGIN';
// export const STATE_SIGNUP = 'SIGNUP';

AuthForm.propTypes = {
  // authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  authState: PropTypes.oneOf([STATE_LOGIN]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  // confirmPasswordLabel: PropTypes.string,
  // confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'LOGIN',
  showLogo: true,
  usernameLabel: '아이디',
  usernameInputProps: {
    type: 'text',
    placeholder: '',
  },
  passwordLabel: '비밀번호',
  passwordInputProps: {
    type: 'password',
    placeholder: '',
  },
  // confirmPasswordLabel: 'Confirm Password',
  // confirmPasswordInputProps: {
  //   type: 'password',
  //   placeholder: 'confirm your password',
  // },
  onLogoClick: () => {},
};

export default AuthForm;
