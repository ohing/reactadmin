import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class ReportListPage extends React.Component {

  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  currentReport = null

  state = {
    requestType: '0',
    isCompleted: '0',

    currentPage: 0,
    reports: [],

    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    
    contentModal: false,
    imageModal: false,
  };

  componentDidMount() {
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  } 
  
  getData = async (props) => {

    try {

      let result = await api.report().call({apiName: 'getReports', method: 'get', parameters: {
        requestType: this.state.requestType,
        isCompleted: this.state.isCompleted,
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let reports = result.reports

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        reports: reports,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleOption = event => {
    this.setState({
      requestType: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleCompleted = event => {
    this.setState({
      isCompleted: event.target.value
    }, () => {
      this.getData()
    })
  }

  toggleContentModal = modalType => () => {
    if (!modalType) {
      this.setState({
        contentModal: !this.state.contentModal,
      })
    }
  }

  handleMedia = event => {

    let feedId = parseInt(event.target.getAttribute('value'))
    let mediaUrl = event.target.getAttribute('src') 
    let mediaType = event.target.getAttribute('type') 
    let mediaSeq = parseInt(event.target.getAttribute('seq'))

    let url = new URL(mediaUrl)
    let paths = url.pathname.split('/')
    let fileName = paths[paths.length-1]
    let pureName = fileName.split('.')[0]

    mediaUrl = `${this.mediaCloudfrontUrl}/report/${fileName}`

    this.setState({
      currentMediaType: mediaType,
      currentMediaUrl: mediaUrl
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {
    if (!modalType) {
      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  handleContent = event => {
    event.preventDefault()
    let value = event.target.getAttribute('value')

    let reportId = parseInt(value.split(':')[1])
    let report = this.report(reportId)
    this.currentReport = report

    this.toggleContentModal()()
  }

  handleTarget = event => {
    event.preventDefault()
    let value = event.target.getAttribute('value')
    let type = value.split(':')[0]
    let id = parseInt(value.split(':')[1])
    console.log(type, id)

    switch (type) {
      case 'user': {
        this.props.history.push(`/user/${id}`)
        break 
      }
      case 'feed': {
        this.props.history.push(`/feed/${id}`)
        break 
      }
      case 'comment': {
        break 
      }
      case 'chatroom': {
        break 
      }
      case 'chatnotice': {
        break 
      }
      case 'chatcomment': {
        break 
      }
    }
  }

  report = (reportId) => {
    return this.state.reports.filter(report => {
      return report.reportId == reportId
    })[0]
  }

  handleComplete = event => {
    let reportId = parseInt(event.target.id.split(':')[1])
    let report = this.report(reportId)

    this.complete(reportId, report.statusCd == 'NEW')
  }
  
  complete = async (reportId, complete) => {

    try {

      await api.report().call({apiName: complete ? 'completeReport' : 'cancelCompleteReport', method: 'post', pathParameter: reportId})

      this.getData()
      this.props.addNotification(complete ? '처리가 완료되었습니다' : '처리가 취소되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  render() {

    console.log('render')

    return (
      <Page
        className="ReportListPage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>신고 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>

                          <Input className="ml-3" type="select" value={this.state.isUsing} onChange={this.handleOption}>
                            <option value="0">신고유형 전체</option>
                            <option value="RT011">타인 사칭, 10대 아님</option>
                            <option value="RT012">음란물, 폭력물 게재</option>
                            <option value="RT013">괴롭힘, 혐오발언</option>
                            <option value="RT014">지적 재산권 침해</option>
                            <option value="RT015">스팸, 허가되지 않은 판매</option>
                            <option value="RT999">기타</option>
                          </Input>

                          <Input className="ml-3" type="select" value={this.state.isOnGoing} onChange={this.handleCompleted}>
                            <option value="0">처리상태 전체</option>
                            <option value="1">처리전</option>
                            <option value="2">처리완료</option>
                          </Input>
                          
                        </div>

                        {/* <div>
                          <Input
                            type="text"
                            name="text"
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                            onChange={this.handleSearchText}
                          />
                          <Button outline color="secondary" value={this.state.searchText} onClick={this.handleSearch}>검색</Button>
                        </div> */}
                      </Form>

                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="80">신고일자</th>
                            <th width="100">처리상태</th>
                            <th width="100">신고유형</th>
                            <th width="180">신고내용</th>
                            <th width="100">신고대상</th>
                            <th width="80"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.reports.map( (report, index) => {

                              return (
                                <>
                                  <tr>
                                    <td className="align-middle">{report.reportDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                    <td className="align-middle">{report.statusCd == 'NEW' ? '처리전' : '처리완료'}</td>
                                    <td className="align-middle">{report.types.join(', ')}</td>
                                    <td className="text-truncate" style={{maxWidth:'340px'}}><a href="#" value={`report:${report.reportId}`} onClick={this.handleContent}>{report.reportContent}</a></td>
                                    <td className="align-middle">
                                      {
                                        report.targetId == null 
                                        ? ''
                                          : report.targetTypeCd == 'USER' 
                                          ? <a href="#" value={`user:${report.targetId}`} onClick={this.handleTarget}>유저: {report.targetId}</a> 
                                          : report.targetTypeCd == 'FEED'
                                          ? <a href="#" value={`feed:${report.targetId}`} onClick={this.handleTarget}>피드: {report.targetId}</a> 
                                          : report.targetTypeCd == 'COMT'
                                          ? <a href="#" value={`comment:${report.targetId}`} onClick={this.handleTarget}>댓글: {report.targetId}</a> 
                                          : report.targetTypeCd == 'CHAT' || report.targetTypeCd == 'CHATROOM'
                                          ? <a href="#" value={`chatroom:${report.targetId}`} onClick={this.handleTarget}>채팅방: {report.targetId}</a> 
                                          : report.targetTypeCd == 'CHATNOTI'
                                          ? <a href="#" value={`chatnotice:${report.targetId}`} onClick={this.handleTarget}>채팅공지: {report.targetId}</a> 
                                          : report.targetTypeCd == 'CHATCOMT'
                                          ? <a href="#" value={`chatcomment:${report.targetId}`} onClick={this.handleTarget}>채팅댓글: {report.targetId}</a>
                                          : ''
                                      } 
                                    </td>
                                    <td className="align-middle">
                                      <Button outline className="mx-1" id={`report:${report.reportId}`} color={report.statusCd == 'NEW' ? 'secondary' : 'primary' } onClick={this.handleComplete} style={{height: '32px'}}>{report.statusCd == 'NEW' ? '처리완료' : '처리취소'}</Button>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colSpan="6">
                                      {
                                        report.medias.map( (media, index) => {

                                          let fileName = media.fileName
                                          let pureName = fileName.split('.')[0]

                                          let mediaUrl = `${this.mediaCloudfrontUrl}/report/200x200/${fileName}`

                                          let icon = media.type === 'MOVIE'
                                          ? <MdVideocam className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,-280%)'}}/>
                                          : <MdPhoto className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,-280%)'}}/>

                                          return (
                                          <>
                                            <CardImg
                                              className="card-img ml-1 can-click"
                                              src={mediaUrl}
                                              type={media.type}
                                              key={media.mediaSeq}
                                              style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                                              onClick={this.handleMedia}/>
                                            {icon}
                                          </>
                                        )
                                        })
                                      }
                                    </td>
                                  </tr>
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.contentModal}
          toggle={this.toggleContentModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleContentModal()}>{'신고 내용'}</ModalHeader>
          <ModalBody>            
            <Label>{this.currentReport == null ? '' : this.currentReport.reportContent}</Label>
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>{'신고 이미지'}</ModalHeader>
          <ModalBody>            
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
        </Modal>

      </Page>
    ); 
  }
};

export default ReportListPage;
