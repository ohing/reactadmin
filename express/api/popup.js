const fs = require('fs')

var express = require('express');

const multer = require('multer');
const upload = multer({
  dest: 'uploads/'
})

var router = express.Router();
var axios = require('axios');
var parser = require('node-html-parser')

const camelcase = require('camelcase-keys')
const { v4: uuidv4 } = require('uuid');

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3()
const cloudfront = new AWS.CloudFront();
const lambda = new AWS.Lambda()

appDataInvalidation = async (mysql) => {

  let s3Result = await s3.getObject({
    Bucket: `ohing-public-${process.env.stage}`,
    Key: 'appData.json'
  }).promise()

  let body = s3Result.Body
  let string = body.toString()
  let appData = JSON.parse(string)

  let newVersion = appData.initVersion + 1

  let query = `
    update 
      oi_system_config 
    set
      config_ver = ${newVersion};
  `
  await mysql.execute(query)

  appData.initVersion = newVersion
  string = JSON.stringify(appData)
  body = Buffer.from(string)

  await s3.putObject({
    Bucket: `ohing-public-${process.env.stage}`,
    ContentType: 'application/json',
    ACL: 'public-read',
    Body: body,
    Key: 'appData.json'
  }).promise()

  let date = new Date()
  let dateString = `${date.getFullYear()}${date.getMonth()+1}${date.getDate()}${date.getHours()}${date.getMinutes()}${date.getSeconds()}`

  await cloudfront.createInvalidation({
    DistributionId: process.env.stage == 'dev' ? 'E2MKHYC1WFIEFZ' : 'E16EONPOZJ0HT8',
    InvalidationBatch: {
      CallerReference: dateString,
      Paths: {
        Quantity: 1,
        Items: [
          '/appData.json'
        ]
      }
    }
  }).promise()
}

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  try {

    let query
    let result

    let requestType = (req.query.requestType || '').trim()

    let isUsing = req.query.isUsing || '0'
    let isUsingCondition = ''
    switch (isUsing) {
      case '1': {
        isUsingCondition = `
          and bi.open_yn = 'Y'
        `
        break
      }
      case '2': {
        isUsingCondition = `
          and bi.open_yn = 'N'
        `
        break
      }
    }
    let isOnGoing = req.query.isOnGoing || '0'
    let isOnGoingCondition = ''
    switch (isOnGoing) {
      case '1': {
        isOnGoingCondition = `
          and bi.banner_begin_dt < now() and bi.banner_end_dt > now()
        `
        break
      }
      case '2': {
        isOnGoingCondition = `
          and bi.banner_begin_dt > now() or bi.banner_end_dt < now()
        `
        break
      }
    }

    let mysql = req.app.get('mysqlR')
    
    query = `
      select sql_calc_found_rows
        bi.banner_id, bi.landing_target target_type, bi.banner_title, bi.image_url, bi.ext_url target_url, bi.banner_type_cd, bi.target_id, 
        bi.banner_sort_no, bi.open_yn, bi.del_yn, bi.reg_dt, bi.banner_begin_dt, bi.banner_end_dt,
        bi.popup_hidden_day
      from 
        oi_banner_info bi
      where
        bi.service_type_cd = 'POPUP'
        and bi.del_yn = 'N'
        ${isUsingCondition}
        ${isOnGoingCondition}
      order by 
        bi.banner_id desc
      limit
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)
    let popups = camelcase(result, {deep: true})
    console.log(popups)

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      popups: popups,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


router.put('/', upload.single('image'), async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId

  try {

    let file = req.file
    let path = file.path

    let fileBuffer = fs.readFileSync(path)

    let names = file.originalname.split('.')
    let extension = names[names.length-1]
    let fileName = uuidv4()
    let key = `popup/${fileName}/${fileName}.${extension}`

    await s3.putObject({
      Bucket: `ohing-${process.env.stage == 'dev' ? 'dev' : 'opr'}-media`,
      ContentType: 'application/json',
      ContentLanguage: 'utf-8',
      ContentDisposition: 'attachment',
      Body: fileBuffer,
      Key: key
    }).promise()

    fs.access(path, fs.constants.F_OK, (error) => {
      console.log(error)
      fs.unlink(path, (error) => {
        console.log(error)
      }) 
    })    

    let imageUrl = `https://${process.env.stage == 'dev' ? 'd25s42vo6uiaxs' : 'd1msh2x81vktjt'}.cloudfront.net/${key}`

    let query
    let result

    let mysql = req.app.get('mysqlW')

    let title = req.body.title
    let partnerId = req.body.partnerId
    let targetType = req.body.targetType
    let targetId = req.body.targetId
    let targetUrl = req.body.targetUrl
    let sortOrder = req.body.sortOrder

    let beginTime = req.body.beginTime
    let endTime = req.body.endTime

    let beginDate = beginTime.split(' ')[0].replace(/-/gi, '')
    let endDate = endTime.split(' ')[0].replace(/-/gi, '')

    let hiddenDays = parseInt(req.body.hiddenDays) || 0

    if (sortOrder == null) {
      query = `
        select 
          max(banner_sort_no) max_sort_number
        from 
          oi_banner_info
        where
          service_type_cd = 'POPUP';
      `
      result = await mysql.execute(query)
      sortOrder = result[0].max_sort_number + 1
    }
    

    let landingTarget = targetType != null ? targetType : 'null'
    targetId = targetId != null ? targetId : 'null'
    targetUrl = targetUrl != null ? `'${targetUrl}'` : 'null'
    partnerId = partnerId != null && partnerId != '0' ? partnerId : 'null'

    query = `
      insert into oi_banner_info (
        landing_target, banner_title, image_url, banner_type_cd, banner_start_ymd, banner_end_ymd, banner_begin_dt, banner_end_dt, 
        service_type_cd, target_id, ext_url, banner_sort_no, open_yn, del_yn, producer_id, reg_dt, popup_hidden_title, popup_hidden_day
      ) values (
        ${landingTarget}, '${title}', '${imageUrl}', null, '${beginDate}', '${endDate}', timestamp('${beginTime}'), timestamp('${endTime}'),
        'POPUP', ${targetId}, ${targetUrl}, ${sortOrder}, 'Y', 'N', ${partnerId}, now(), '일동안 보지 않기', ${hiddenDays}
      );
    `

    await mysql.execute(query)

    appDataInvalidation(mysql)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:bannerId', upload.single('image'), async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId
  let bannerId = req.params.bannerId

  try {

    let imageUrl

    if (req.file != null) {

      let file = req.file
      let path = file.path

      let fileBuffer = fs.readFileSync(path)

      let names = file.originalname.split('.')
      let extension = names[names.length-1]
      let fileName = uuidv4()
      let key = `banner/${fileName}/${fileName}.${extension}`

      await s3.putObject({
        Bucket: `ohing-${process.env.stage == 'dev' ? 'dev' : 'opr'}-media`,
        ContentType: 'application/json',
        ContentLanguage: 'utf-8',
        ContentDisposition: 'attachment',
        Body: fileBuffer,
        Key: key
      }).promise()

      fs.access(path, fs.constants.F_OK, (error) => {
        console.log(error)
        fs.unlink(path, (error) => {
          console.log(error)
        }) 
      })

      imageUrl = `https://${process.env.stage == 'dev' ? 'd25s42vo6uiaxs' : 'd1msh2x81vktjt'}.cloudfront.net/${key}`

    } else {

      imageUrl = req.body.imageUrl
    }

    let query
    let result

    let mysql = req.app.get('mysqlW')

    let title = req.body.title
    let partnerId = req.body.partnerId
    let targetType = req.body.targetType
    let targetId = req.body.targetId
    let targetUrl = req.body.targetUrl
    let sortOrder = req.body.sortOrder

    let beginTime = req.body.beginTime
    let endTime = req.body.endTime

    let beginDate = beginTime.split(' ')[0].replace(/-/gi, '')
    let endDate = endTime.split(' ')[0].replace(/-/gi, '')

    let openImmediately = req.body.openImmediately == '1'

    if (sortOrder == null) {
      query = `
        select 
          max(banner_sort_no) max_sort_number
        from 
          oi_banner_info
        where
          service_type_cd = 'POPUP';
      `
      result = await mysql.execute(query)
      sortOrder = result[0].max_sort_number + 1
    }
    

    let landingTarget = targetType != null ? targetType : 'null'
    targetId = targetId != null ? targetId : 'null'
    targetUrl = targetUrl != null ? `'${targetUrl}'` : 'null'
    partnerId = partnerId != null && partnerId != '0' ? partnerId : 'null'

    query = `
      update
        oi_banner_info
      set
        landing_target = ${landingTarget}, banner_title = '${title}', image_url = '${imageUrl}', banner_start_ymd = '${beginDate}', banner_end_ymd = '${endDate}',
        banner_begin_dt = timestamp('${beginTime}'), banner_end_dt = timestamp('${endTime}'), service_type_cd = 'POPUP', target_id = ${targetId}, ext_url = ${targetUrl},
        banner_sort_no = ${sortOrder}, producer_id = ${partnerId}, upd_dt = now(), upd_user_id = ${userId}
      where
        banner_id = ${bannerId};
    `

    await mysql.execute(query)

    appDataInvalidation(mysql)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:bannerId/open', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId
  let bannerId = req.params.bannerId

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_banner_info
      set
        open_yn = 'Y'
      where
        banner_id = ${bannerId};
    `
    await mysql.execute(query)

    appDataInvalidation(mysql)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:bannerId/close', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId
  let bannerId = req.params.bannerId

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_banner_info
      set
        open_yn = 'N'
      where
        banner_id = ${bannerId};
    `
    await mysql.execute(query)

    appDataInvalidation(mysql)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId

  try {

    let bannerIds = req.body.bannerIds

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      delete from
        oi_banner_info
      where
        banner_id in (${bannerIds.join(',')})
    `
    await mysql.execute(query)

    appDataInvalidation(mysql)
    
    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})



module.exports = router