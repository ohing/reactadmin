import logo200Image from 'assets/img/logo/logo_200.png';
import sidebarBgImage from 'assets/img/sidebar/sidebar-4.jpg';
import SourceLink from 'components/SourceLink';
import React from 'react';
// import { FaGithub } from 'react-icons/fa';
import {
  MdAccountCircle,
  MdArrowDropDownCircle,
  MdBorderAll,
  MdBrush,
  MdChromeReaderMode,
  MdDashboard,
  MdExtension,
  MdGroupWork,
  MdInsertChart,
  MdKeyboardArrowDown,
  MdNotificationsActive,
  MdPages,
  MdRadioButtonChecked,
  MdSend,
  MdStar,
  MdTextFields,
  MdViewCarousel,
  MdViewDay,
  MdViewList,
  MdWeb,
  MdWidgets,
  MdWork,
  MdChatBubble,
  MdComment,
  MdPerson,
  MdDescription,
  MdFavorite,
  MdClose,
  MdImage,
  MdShortText,
  MdReport,
  MdBrandingWatermark,
  MdAssignmentTurnedIn,
  MdQuestionAnswer,
  MdList,
  MdHelp,
  MdSentimentSatisfied,
  MdPeople
} from 'react-icons/md';
import {
  FaUserCheck,
  FaTelegramPlane,
  FaAdversal
} from 'react-icons/fa'
import {
  IoMdMegaphone,
  IoMdNotifications,
  IoIosPricetag,
} from 'react-icons/io'
import {
  GiArtificialHive
} from 'react-icons/gi'

import { NavLink } from 'react-router-dom';
import {
  // UncontrolledTooltip,
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import bn from 'utils/bemnames';

const sidebarBackground = {
  // backgroundImage: `url("${sidebarBgImage}")`,
  // backgroundSize: 'cover',
  // backgroundRepeat: 'no-repeat',
};

// const navComponents = [
//   { to: '/buttons', name: 'buttons', exact: false, Icon: MdRadioButtonChecked },
//   {
//     to: '/button-groups',
//     name: 'button groups',
//     exact: false,
//     Icon: MdGroupWork,
//   },
//   { to: '/forms', name: 'forms', exact: false, Icon: MdChromeReaderMode },
//   { to: '/input-groups', name: 'input groups', exact: false, Icon: MdViewList },
//   {
//     to: '/dropdowns',
//     name: 'dropdowns',
//     exact: false,
//     Icon: MdArrowDropDownCircle,
//   },
//   { to: '/badges', name: 'badges', exact: false, Icon: MdStar },
//   
//   { to: '/progress', name: 'progress', exact: false, Icon: MdBrush },
//   { to: '/modals', name: 'modals', exact: false, Icon: MdViewDay },
// ];

// const navContents = [
//   { to: '/typography', name: 'typography', exact: false, Icon: MdTextFields },
//   { to: '/tables', name: 'tables', exact: false, Icon: MdBorderAll },
// ];

// const navItems = [
//   { to: '/', name: '대시보드', exact: true, Icon: MdDashboard },
//   { to: '/cards', name: 'cards', exact: false, Icon: MdWeb },
//   { to: '/charts', name: 'charts', exact: false, Icon: MdInsertChart },
//   { to: '/widgets', name: 'widgets', exact: false, Icon: MdWidgets },
// ];

const navItems = [
  { to: '/', name: '대시보드', exact: true, Icon: MdDashboard },
  { to: '/admin', name: '관리자', exact: false, Icon: MdWork },
  { to: '/user', name: '회원', exact: false, Icon: MdPerson },
  { to: '/feed', name: '게시물', exact: false, Icon: MdDescription }, 
  { to: '/comment', name: '댓글', exact: false, Icon: MdComment }, 
  { to: '/chat/room', name: '오픈채팅', exact: false, Icon: MdChatBubble }, 
];

const pushContents = [
  { to: '/push/tester', name: '테스트 계정', exact: false, Icon: FaUserCheck }, 
  { to: '/push/message', name: '메시지', exact: false, Icon: FaTelegramPlane }, 
];

const navItems2 = [
  { to: '/notice', name: '공지사항', exact: true, Icon: IoMdMegaphone },  
];

const keywordContents = [
  { to: '/keyword/recommend', name: '프로필 관심사 추천', exact: false, Icon: MdFavorite }, 
  { to: '/keyword/popular', name: '오픈채팅 인기 관심사', exact: false, Icon: MdStar }, 
  { to: '/keyword/banned', name: '금칙어', exact: false, Icon: MdClose }, 
];

const bannerContents = [
  { to: '/ad/partner', name: '파트너', exact: false, Icon: MdPeople }, 
  { to: '/ad/banner', name: '배너', exact: true, Icon: MdBrandingWatermark },
];

const navItems3 = [
  { to: '/popup', name: '메인팝업', exact: true, Icon: MdAssignmentTurnedIn },
  { to: '/ohing-on', name: 'Ohing On', exact: true, Icon: MdSentimentSatisfied },
  { to: '/report', name: '신고', exact: true, Icon: MdReport },  
  { to: '/inquirement', name: '문의', exact: true, Icon: MdQuestionAnswer },  
];

const categoryContents = [
  { to: '/category/help', name: '도와줘', exact: false, Icon: MdHelp }, 
]

const aiContents = [
  { to: '/ai/bad-image', name: '이미지', exact: false, Icon: MdImage }, 
  { to: '/ai/bad-text', name: '텍스트', exact: false, Icon: MdShortText }, 
];

const navSamples = [
  // { to: '/tables', name: 'Table', exact: false, Icon: MdBorderAll },
  // { to: '/forms', name: 'Form', exact: false, Icon: MdChromeReaderMode },
  { to: '/buttons', name: 'Button', exact: false, Icon: MdRadioButtonChecked },
  { to: '/badges', name: 'Badges', exact: false, Icon: MdStar },
  { to: '/widgets', name: 'Widgets', exact: false, Icon: MdWidgets },
  // { to: '/cards', name: 'Cards', exact: false, Icon: MdWeb },
  // { to: '/alerts', name: 'alerts', exact: false, Icon: MdNotificationsActive },
  // { to: '/modals', name: 'modals', exact: false, Icon: MdViewDay },
]

const bem = bn.create('sidebar');

class Sidebar extends React.Component {
  state = {
    isOpenPush: false,
    isOpenKeyword: false,
    isOpenAd: false,
    isOpenCategory: false,
    isOpenAI: false,
  };

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  handleLink = event => {
    window.localStorage.removeItem('lastUsersState')
    window.localStorage.removeItem('lastFeedsState')
    window.localStorage.removeItem('lastPushTesterState')
    window.localStorage.removeItem('lastPushMessageState')
  }

  render() {
    return (
      <aside className={bem.b()} data-image={sidebarBgImage}>
        <div className={bem.e('background')} style={sidebarBackground} />
        <div className={bem.e('content')}>
          <Navbar>
            <SourceLink className="navbar-brand d-flex">
              <img
                src={logo200Image}
                width="40"
                height="30"
                className="pr-2"
                alt=""
              />
              <span className="text-white">
                ing
              </span>
            </SourceLink>
          </Navbar>
          <Nav vertical>
            {navItems.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                  onClick={this.handleLink}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Push')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <IoMdNotifications className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">PUSH</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenPush
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>

            <Collapse isOpen={this.state.isOpenPush}>
              {pushContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase, ml-3"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                    onClick={this.handleLink}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            {navItems2.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                  onClick={this.handleLink}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Keyword')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <IoIosPricetag className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">키워드</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenKeyword
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>

            <Collapse isOpen={this.state.isOpenKeyword}>
              {keywordContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase, ml-3"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                    onClick={this.handleLink}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Ad')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <FaAdversal className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">광고</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenAd
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>

            <Collapse isOpen={this.state.isOpenAd}>
              {bannerContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase, ml-3"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                    onClick={this.handleLink}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            {navItems3.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                  onClick={this.handleLink}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Category')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdList className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">카테고리</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenCategory
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>

            <Collapse isOpen={this.state.isOpenCategory}>
              {categoryContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase, ml-3"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                    onClick={this.handleLink}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('AI')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <GiArtificialHive className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">AI</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenAI
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>

            <Collapse isOpen={this.state.isOpenAI}>
              {aiContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase, ml-3"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                    onClick={this.handleLink}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>


            {/* {navSamples.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                  onClick={this.handleLink}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))} */}

            
            {/* <Collapse isOpen={this.state.isOpenComponents}>
              {navComponents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Contents')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdSend className={bem.e('nav-item-icon')} />
                  <span className="">Contents</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenContents
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>
            <Collapse isOpen={this.state.isOpenContents}>
              {navContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Pages')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdPages className={bem.e('nav-item-icon')} />
                  <span className="">Pages</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenPages
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>
            <Collapse isOpen={this.state.isOpenPages}>
              {pageContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse> */}
          </Nav>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
