var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')

router.get('/', async (req, res) => {

  console.log(req.body)

  let userId = req.headers.userId

  let mysql = req.app.get('mysqlR')
  let redis3 = req.app.get('redis3')
  let redis7 = req.app.get('redis7')

  let query
  let result

  query = `
  select 
	android_today.count android_today_join_count, android_yesterday.count android_yesterday_join_count,
    android_today_withdraw.count android_today_withdraw_count,
    android_yesterday_withdraw.count android_yesterday_withdraw_count,
    android_active.count android_active_count, android_withdraw.count android_withdraw_count,
    ios_today.count ios_today_join_count, ios_yesterday.count ios_yesterday_join_count, 
    ios_today_withdraw.count ios_today_withdraw_count,
    ios_yesterday_withdraw.count ios_yesterday_withdraw_count,
    ios_active.count ios_active_count, ios_withdraw.count ios_withdraw_count,
    (total_active.count - android_active.count - ios_active.count) unknown_active_count,
    (total_withdraw.count - android_withdraw.count - ios_withdraw.count) unknown_withdraw_count 
from (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > (curdate()+0)
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'ANDROID'
	) android_today,
    (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > (curdate()+0)
        and ui.user_type_cd = 'USER'
        and ui.user_status_cd = 'WITHDRAW'
		and udi.os_type_cd = 'ANDROID'
	) android_today_withdraw,
    (
    select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > date_sub((curdate()+0), interval 1 day)
        and ui.reg_dt < (curdate()+0)
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'ANDROID'
	) android_yesterday,
    (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > date_sub((curdate()+0), interval 1 day)
        and ui.reg_dt < (curdate()+0)
        and ui.user_type_cd = 'USER'
        and ui.user_status_cd = 'WITHDRAW'
		and udi.os_type_cd = 'ANDROID'
	) android_yesterday_withdraw,
    (
    select 
		count(*) count
	from 
		oi_user_info ui
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.user_status_cd = 'ACTIVE'
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'ANDROID'
	) android_active,
    (
    select 
		count(*) count
	from 
		oi_user_info ui
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.user_status_cd = 'WITHDRAW'
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'ANDROID'
	) android_withdraw,
    (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > (curdate()+0)
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'IOS'
	) ios_today,
    (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
        ui.reg_dt > (curdate()+0)
        and ui.user_type_cd = 'USER'
        and ui.user_status_cd = 'WITHDRAW'
		and udi.os_type_cd = 'IOS'
	) ios_today_withdraw,
    (
    select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > date_sub((curdate()+0), interval 1 day)
    and ui.reg_dt < (curdate()+0)
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'IOS'
	) ios_yesterday,
    (
	select 
		count(*) count
	from 
		oi_user_info ui 
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.reg_dt > date_sub((curdate()+0), interval 1 day)
        and ui.reg_dt < (curdate()+0)
        and ui.user_type_cd = 'USER'
        and ui.user_status_cd = 'WITHDRAW'
		and udi.os_type_cd = 'IOS'
	) ios_yesterday_withdraw,
    (
    select 
		count(*) count
	from 
		oi_user_info ui
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.user_status_cd = 'ACTIVE'
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'IOS'
	) ios_active,
    (
    select 
		count(*) count
	from 
		oi_user_info ui
		join oi_user_device_info udi on ui.user_id = udi.user_id
	where
		ui.user_status_cd = 'WITHDRAW'
        and ui.user_type_cd = 'USER'
		and udi.os_type_cd = 'IOS'
	) ios_withdraw,
  (
    select 
		count(*) count
	from 
		oi_user_info ui
	where
		ui.user_status_cd = 'ACTIVE'
    and ui.user_type_cd = 'USER'
	) total_active,
  (
    select 
		count(*) count
	from 
		oi_user_info ui
	where
		ui.user_status_cd = 'WITHDRAW'
    and ui.user_type_cd = 'USER'
	) total_withdraw;
  `
  result = await mysql.execute(query)
  let counts = result[0]

  counts.onlineCount = parseInt(await redis3.eval(`return #redis.pcall('keys', 'alive:*')`, 0))

  query = `
    select
      user_pwd
    from
      oi_user_info
    where 
      user_id = ${userId};
  `
  result = await mysql.execute(query)
  let currentPassword = result[0].user_pwd

  let needsChangePassword = currentPassword == '831c237928e6212bedaa4451a514ace3174562f6761f6a157a2fe5082b36e2fb'

  let date = new Date()
  let ymd = `${date.getUTCFullYear()}-${date.getUTCMonth()+1}-${date.getUTCDate()}`
  let dau = await redis7.scard(`dau:${ymd}`)

  query = `
    select 
    sum(count) / (select count(*) from oi_user_info where user_status_cd = 'ACTIVE' and user_type_cd = 'USER') count
  from (
    select 
      count(ur.user_id) count 
    from 
      oi_user_relation ur 
      join oi_user_info ui on ur.user_id = ui.user_id
    where
      ui.user_status_cd = 'ACTIVE'
      and ui.user_type_cd = 'USER'
    group by 
      ur.user_id
    ) a;
  `
  result = await mysql.execute(query)
  let followerCount = result[0].count

  query = `
    select 
      sum(count) / (select count(*) from oi_user_info where user_status_cd = 'ACTIVE' and user_type_cd = 'USER') count
    from (
      select 
        count(ur.relation_user_id) count 
      from 
        oi_user_relation ur 
            join oi_user_info ui on ur.relation_user_id = ui.user_id
      where
        ui.user_status_cd = 'ACTIVE'
        and ui.user_type_cd = 'USER'
      group by 
        ur.relation_user_id
      ) a;
  `
  result = await mysql.execute(query)
  let followingCount = result[0].count

  let activities = {dau: dau, followerCount: followerCount, followingCount: followingCount}

  let response = camelcase({
    dashboardData: {counts: counts, activities: activities},
    needsChangePassword: needsChangePassword
  }, {deep: true})

  console.log(response)

  res.send(JSON.stringify(response))
})

module.exports = router