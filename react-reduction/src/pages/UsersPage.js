import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label
} from 'reactstrap';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class UsersPage extends React.Component {

  state = {
    currentPage: 0,
    users: [],
    rowCount: 10,
    status: 0,
    openType: 0,
    reportedType: 0,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    searchText: '',
    deleteModal: false
  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 
    console.log('DidMount')
    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {
    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let result = await api.user().call({apiName: 'getUsers', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        status: this.state.status,
        openType: this.state.openType, 
        reportedType: this.state.reportedType,
        rowCount: this.state.rowCount, 
        searchText: this.state.searchText
      }})

      let totalCount = result.totalCount
      let users = result.users

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        users: users,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })

    } catch (e) {

      console.log(e)
    }
  }

  handleStatus = event => {

    let type = parseInt(event.target.value)
    console.log('type', type)

    this.state.status = type
    this.state.currentPage = 0
    this.getData()
  }

  handleOpenType = event => {

    let type = parseInt(event.target.value)
    console.log('type', type)

    this.state.openType = type
    this.state.currentPage = 0
    this.getData()
  }

  handleReportedType = event => {

    let type = parseInt(event.target.value)
    console.log('type', type)

    this.state.reportedType = type
    this.state.currentPage = 0
    this.getData()
  }

  handleSearchText = event => {
    let searchText = event.target.value
    this.setState({
      searchText: searchText
    })
  }

  handleKeyDown = event => {
    if (event.key == 'Enter') {
      event.preventDefault();
      this.handleSearch(event)
    }
  }

  handleSearch = event => {
    this.state.currentPage = 0
    this.getData()
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleUser = event => {
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/user/${targetUserId}`) 
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>회원 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>
                          <Input type="select" value={this.state.status} onChange={this.handleStatus}>
                            <option value="0">계정 상태</option>
                            <option value="1">정상</option>
                            <option value="2">차단</option>
                          </Input>
                          <Input className="ml-3" type="select" value={this.state.openType} onChange={this.handleOpenType}>
                            <option value="0">프로필 공개상태</option>
                            <option value="1">전체공개</option>
                            <option value="2">친구공개</option>
                            <option value="3">비공개</option>
                          </Input>
                          <Input className="ml-3" type="select" value={this.state.reportedType} onChange={this.handleReportedType}>
                            <option value="0">신고처리상태</option>
                            <option value="1">미처리 있음</option>
                          </Input>
                        </div>

                        <div>
                          <Input
                            type="text"
                            name="text"
                            value={this.state.searchText}
                            onChange={this.handleSearchText}
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                          />
                          <Button outline color="secondary" onClick={this.handleSearch}>검색</Button>
                        </div>
                      </Form>
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="180">아이디</th>
                            <th width="80">이름</th>
                            <th width="120">프로필 공개</th>
                            <th width="80">신고</th>
                            <th width="100">OS</th>
                            <th width="130">가입일</th>
                            <th width="130">마지막 로그인</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.users.map( (user, index) => {
                              return (
                                <tr key={user.userId}>
                                  <td><a href="#" onClick={this.handleUser} value={user.userId}>{user.accountId}</a></td>
                                  <td>{user.name}</td>
                                  <td>{user.openType}</td>
                                  <td>{user.reportedCount > 0 ? <a href="#" value={user.userId} onClick={this.handleReported}>{user.uncompletedReportCount}/{user.reportedCount}</a> : '0/0'}</td>
                                  <td>{user.osTypeCd}</td>
                                  <td>{user.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                  <td>{(user.updatedAt || '').replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Page>
    ); 
  }
};

export default UsersPage;
