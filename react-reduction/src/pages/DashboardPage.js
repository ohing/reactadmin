import { AnnouncementCard, TodosCard } from 'components/Card';
import HorizontalAvatarList from 'components/HorizontalAvatarList';
import MapWithBubbles from 'components/MapWithBubbles';
import PageSpinner from 'components/PageSpinner';
import Page from 'components/Page';
import ProductMedia from 'components/ProductMedia';
import SupportTicket from 'components/SupportTicket';
import UserProgressTable from 'components/UserProgressTable';
import { IconWidget, TextDetailWidget, NumberWidget, DualNumberWidget } from 'components/Widget';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import {
  avatarsData,
  chartjs,
  productsData,
  supportTicketsData,
  todosData,
  userProgressTableData,
} from 'demos/dashboardPage';
import React from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  MdBubbleChart,
  MdInsertChart,
  MdPersonPin,
  MdPieChart,
  MdRateReview,
  MdShare,
  MdShowChart,
  MdThumbUp,
} from 'react-icons/md';
import InfiniteCalendar from 'react-infinite-calendar';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardDeck,
  CardGroup,
  CardHeader,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Form,
  Input,
} from 'reactstrap';

import { getColor } from 'utils/colors';

import API from 'utils/api'
const api = new API()

const today = new Date();
const lastWeek = new Date(
  today.getFullYear(),
  today.getMonth(),
  today.getDate() - 7,
);

class DashboardPage extends React.Component {

  state = {
    passwordModal: false,
    userPassword: '',
    userPasswordRepeat: ''
  }
  componentDidMount() {
    // this is needed, because InfiniteCalendar forces window scroll
    window.scrollTo(0, 0);

    this.getData()
  }

  getData = async () => {
    
    let result = await api.dashboard().call({apiName: 'dashboardData', method: 'get'})

    let passwordModal = false
    
    if (result.needsChangePassword) {
       passwordModal = true
    }

    this.setState({
      passwordModal: passwordModal,
      dashboard: result.dashboardData
    })
    
  }

  togglePasswordModal = modalType => () => {

    if (!modalType) {

      this.setState({
        passwordModal: !this.state.passwordModal,
      })
    }
  }

  handlePassword = event => {
    event.preventDefault()

    this.setState({
      userPassword: event.target.value
    })
  }

  handlePasswordRepeat = event => {
    event.preventDefault()

    this.setState({
      userPasswordRepeat: event.target.value
    })
  }

  handlePasswordKeyDown = event => {
    if (event.key === 'Enter') {
      this.changePassword()
    }
  }

  changePassword = async () => {

    let password = this.state.userPassword.trim()
    let repeat = this.state.userPasswordRepeat.trim()

    if (password != repeat) {
      this.props.addNotification('비밀번호가 일치하지 않습니다.')
      return
    }

    if (password.length == 0) {
      this.props.addNotification('비밀번호를 입력해 주세요.')
      return
    }

    if (repeat.length == 0) {
      this.props.addNotification('비밀번호를 다시 한 번 입력해 주세요.')
      return
    }

    try {
      let parameters = {
        password: password,
      }
      let result = await api.account().call({apiName: 'changePassword', method: 'post', parameters: parameters})

      this.setState({
        passwordModal: false
      })

      this.props.addNotification('비밀번호가 변경되었습니다.')

    } catch (e) {

      console.log(e)
    }
  }

  render() {
    const primaryColor = getColor('primary');
    const secondaryColor = getColor('secondary');

    let dashboard = this.state.dashboard || {}

    let counts = dashboard.counts || {}
    let maxTodayJoinCount = Math.max(counts.androidTodayJoinCount, counts.iosTodayJoinCount)
    let maxYesterdayJoinCount = Math.max(counts.androidYesterdayJoinCount, counts.iosYesterdayJoinCount)
    let maxActiveCount = Math.max(counts.androidActiveCount, counts.iosActiveCount)

    let maxTodayWithdrawCount = Math.max(counts.androidTodayWithdrawCount, counts.iosTodayWithdrawCount)
    let maxYesterdayWithdrawCount = Math.max(counts.androidYesterdayWithdrawCount, counts.iosYesterdayWithdrawCount)
    let maxWithdrawCount = Math.max(counts.androidWithdrawCount, counts.iosWithdrawCount)

    let activities = dashboard.activities || {}

    return this.state.dashboard == null ? (
      <PageSpinner />
    ) : (
      <Page
        className="DashboardPage"
        title="대시보드"
      >
        <Row>
          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="오늘 가입자"
              number={`${counts.androidTodayJoinCount}/${counts.iosTodayJoinCount}`}
              color="secondary"
              progress={{
                value1: counts.androidTodayJoinCount, maxValue1: maxTodayJoinCount, label1: `Android:${counts.androidTodayJoinCount}`,
                value2: counts.iosTodayJoinCount, maxValue2: maxTodayJoinCount, label2: `iOS:${counts.iosTodayJoinCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="어제 가입자"
              number={`${counts.androidYesterdayJoinCount}/${counts.iosYesterdayJoinCount}`}
              color="secondary"
              progress={{
                value1: counts.androidYesterdayJoinCount, maxValue1: maxYesterdayJoinCount, label1: `Android:${counts.androidYesterdayJoinCount}`,
                value2: counts.iosYesterdayJoinCount, maxValue2: maxYesterdayJoinCount, label2: `iOS:${counts.iosYesterdayJoinCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="활성 회원 현황"
              number={`${counts.androidActiveCount}/${counts.iosActiveCount}/${counts.unknownActiveCount}`}
              color="secondary"
              progress={{
                value1: counts.androidActiveCount, maxValue1: maxActiveCount, label1: `Android:${counts.androidActiveCount}`,
                value2: counts.iosActiveCount, maxValue2: maxActiveCount, label2: `iOS:${counts.iosActiveCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <NumberWidget
              title="온라인 회원"
              number={counts.onlineCount}
              color="secondary"
              progress={{
                value: counts.onlineCount, maxValue: (counts.androidActiveCount+counts.iosActiveCount), label: ' ',
              }}
            />
          </Col>
        </Row>

        <Row>
          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="오늘 탈퇴자"
              number={`${counts.androidTodayWithdrawCount}/${counts.iosTodayWithdrawCount}`}
              color="secondary"
              progress={{
                value1: counts.androidTodayWithdrawCount, maxValue1: maxTodayWithdrawCount, label1: `Android:${counts.androidTodayWithdrawCount}`,
                value2: counts.iosTodayWithdrawCount, maxValue2: maxTodayWithdrawCount, label2: `iOS:${counts.iosTodayWithdrawCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="어제 탈퇴자"
              number={`${counts.androidYesterdayWithdrawCount}/${counts.iosYesterdayWithdrawCount}`}
              color="secondary"
              progress={{
                value1: counts.androidYesterdayWithdrawCount, maxValue1: maxYesterdayWithdrawCount, label1: `Android:${counts.androidYesterdayWithdrawCount}`,
                value2: counts.iosYesterdayWithdrawCount, maxValue2: maxYesterdayJoinCount, label2: `iOS:${counts.iosYesterdayWithdrawCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <DualNumberWidget
              title="탈퇴 회원 현황"
              number={`${counts.androidWithdrawCount}/${counts.iosWithdrawCount}/${counts.unknownWithdrawCount}`}
              color="secondary"
              progress={{
                value1: counts.androidWithdrawCount, maxValue1: maxWithdrawCount, label1: `Android:${counts.androidWithdrawCount}`,
                value2: counts.iosWithdrawCount, maxValue2: maxWithdrawCount, label2: `iOS:${counts.iosWithdrawCount}`
              }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
          
          </Col>
        </Row>

        <Row>
          <Col lg="12" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                회원 활동 현황{' '}
              </CardHeader>
              <CardBody>
                <Row>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="DAU"
                      subtitle="1,919"
                    />
                  </Col>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="WAU"
                      subtitle="1,919"
                    />
                  </Col>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="MAU"
                      subtitle="1,919"
                    />
                  </Col>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="팔로워 보유 회원"
                      subtitle="1,919"
                    />
                  </Col>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="1인당 평균 팔로워"
                      subtitle={activities.followerCount}
                    />
                  </Col>
                  <Col lg={4} md={4} sm={6} xs={12}>
                    <TextDetailWidget
                      title="1인당 평균 팔로잉"
                      subtitle={activities.followingCount}
                    />
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col lg="8" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                Total Revenue{' '}
                <small className="text-muted text-capitalize">This year</small>
              </CardHeader>
              <CardBody>
                <Line data={chartjs.line.data} options={chartjs.line.options} />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>Total Expense</CardHeader>
              <CardBody>
                <Bar data={chartjs.bar.data} options={chartjs.bar.options} />
              </CardBody>
              <ListGroup flush>
                <ListGroupItem>
                  <MdInsertChart size={25} color={primaryColor} /> Cost of sales{' '}
                  <Badge color="secondary">$3000</Badge>
                </ListGroupItem>
                <ListGroupItem>
                  <MdBubbleChart size={25} color={primaryColor} /> Management
                  costs <Badge color="secondary">$1200</Badge>
                </ListGroupItem>
                <ListGroupItem>
                  <MdShowChart size={25} color={primaryColor} /> Financial costs{' '}
                  <Badge color="secondary">$800</Badge>
                </ListGroupItem>
                <ListGroupItem>
                  <MdPieChart size={25} color={primaryColor} /> Other operating
                  costs <Badge color="secondary">$2400</Badge>
                </ListGroupItem>
              </ListGroup>
            </Card>
          </Col>
        </Row>

        <CardGroup style={{ marginBottom: '1rem' }}>
          <IconWidget
            bgColor="white"
            inverse={false}
            icon={MdThumbUp}
            title="50+ Likes"
            subtitle="People you like"
          />
          <IconWidget
            bgColor="white"
            inverse={false}
            icon={MdRateReview}
            title="10+ Reviews"
            subtitle="New Reviews"
          />
          <IconWidget
            bgColor="white"
            inverse={false}
            icon={MdShare}
            title="30+ Shares"
            subtitle="New Shares"
          />
        </CardGroup>

        <Row>
          <Col md="6" sm="12" xs="12">
            <Card>
              <CardHeader>New Products</CardHeader>
              <CardBody>
                {productsData.map(
                  ({ id, image, title, description, right }) => (
                    <ProductMedia
                      key={id}
                      image={image}
                      title={title}
                      description={description}
                      right={right}
                    />
                  ),
                )}
              </CardBody>
            </Card>
          </Col>

          <Col md="6" sm="12" xs="12">
            <Card>
              <CardHeader>New Users</CardHeader>
              <CardBody>
                <UserProgressTable
                  headers={[
                    <MdPersonPin size={25} />,
                    'name',
                    'date',
                    'participation',
                    '%',
                  ]}
                  usersData={userProgressTableData}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col lg={4} md={4} sm={12} xs={12}>
            <Card>
              <Line
                data={getStackLineChart({
                  labels: [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                  ],
                  data: [0, 13000, 5000, 24000, 16000, 25000, 10000],
                })}
                options={stackLineChartOptions}
              />
              <CardBody
                className="text-primary"
                style={{ position: 'absolute' }}
              >
                <CardTitle>
                  <MdInsertChart /> Sales
                </CardTitle>
              </CardBody>
            </Card>
          </Col>

          <Col lg={4} md={4} sm={12} xs={12}>
            <Card>
              <Line
                data={getStackLineChart({
                  labels: [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                  ],
                  data: [10000, 15000, 5000, 10000, 5000, 10000, 10000],
                })}
                options={stackLineChartOptions}
              />
              <CardBody
                className="text-primary"
                style={{ position: 'absolute' }}
              >
                <CardTitle>
                  <MdInsertChart /> Revenue
                </CardTitle>
              </CardBody>
            </Card>
          </Col>
          <Col lg={4} md={4} sm={12} xs={12}>
            <Card>
              <Line
                data={getStackLineChart({
                  labels: [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                  ],
                  data: [0, 13000, 5000, 24000, 16000, 25000, 10000].reverse(),
                })}
                options={stackLineChartOptions}
              />
              <CardBody
                className="text-primary"
                style={{ position: 'absolute', right: 0 }}
              >
                <CardTitle>
                  <MdInsertChart /> Profit
                </CardTitle>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          {/* <Col lg="4" md="12" sm="12" xs="12">
            <InfiniteCalendar
              selected={today}
              minDate={lastWeek}
              width="100%"
              theme={{
                accentColor: primaryColor,
                floatingNav: {
                  background: secondaryColor,
                  chevron: primaryColor,
                  color: '#FFF',
                },
                headerColor: primaryColor,
                selectionColor: secondaryColor,
                textColor: {
                  active: '#FFF',
                  default: '#333',
                },
                todayColor: secondaryColor,
                weekdayColor: primaryColor,
              }}
            />
          </Col> */}

          <Col lg="8" md="12" sm="12" xs="12">
            <Card inverse className="bg-gradient-primary">
              <CardHeader className="bg-gradient-primary">
                Map with bubbles
              </CardHeader>
              <CardBody>
                <MapWithBubbles />
              </CardBody>
            </Card>
          </Col>
        </Row>

        <CardDeck style={{ marginBottom: '1rem' }}>
          <Card body style={{ overflowX: 'auto','paddingBottom':'15px','height': 'fit-content','paddingTop': 'inherit'}}>
            <HorizontalAvatarList
              avatars={avatarsData}
              avatarProps={{ size: 50 }}
            />
          </Card>

          <Card body style={{ overflowX: 'auto','paddingBottom':'15px','height': 'fit-content','paddingTop': 'inherit'}}>
            <HorizontalAvatarList
              avatars={avatarsData}
              avatarProps={{ size: 50 }}
              reversed
            />
          </Card>
        </CardDeck>

        <Row>
          <Col lg="4" md="12" sm="12" xs="12">
            <AnnouncementCard
              color="gradient-secondary"
              header="Announcement"
              avatarSize={60}
              name="Jamy"
              date="1 hour ago"
              text="Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy euismod tinciduntut laoreet doloremagna"
              buttonProps={{
                children: 'show',
              }}
              style={{ height: 500 }}
            />
          </Col>

          <Col lg="4" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                  <span>Support Tickets</span>
                  <Button>
                    <small>View All</small>
                  </Button>
                </div>
              </CardHeader>
              <CardBody>
                {supportTicketsData.map(supportTicket => (
                  <SupportTicket key={supportTicket.id} {...supportTicket} />
                ))}
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" md="12" sm="12" xs="12">
            <TodosCard todos={todosData} />
          </Col>
        </Row>

        <Modal
          isOpen={this.state.passwordModal}
          toggle={this.togglePasswordModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.togglePasswordModal()}>초기 비밀번호 변경</ModalHeader>
          <ModalBody>
            <Form>
              <Label className="ml-2">변경할 비밀번호를 입력합니다.</Label>
              <Input type="password" value={this.state.userPassword} onChange={this.handlePassword} onKeyDown={this.handlePasswordKeyDown}/>
              <Label className="ml-2 mt-3">다시 한 번 입력합니다.</Label>
              <Input type="password" value={this.state.userPasswordRepeat} onChange={this.handlePasswordRepeat} onKeyDown={this.handlePasswordKeyDown}/>
            </Form>
            
          </ModalBody>
          <ModalFooter>

            <Button color="primary" onClick={this.togglePasswordModal()}>
              다음에
            </Button>
            
            <Button color="secondary" onClick={this.changePassword}>
              변경
            </Button>
            
          </ModalFooter>
        </Modal>

      </Page>
    );
  }
}
export default DashboardPage;
