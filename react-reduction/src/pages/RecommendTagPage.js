import React, {
  useRef
} from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon,
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

import API from 'utils/api'
const api = new API()

class RecommendTagPage extends React.Component {  

  state = {
    tags: [],
    recommendTags: null,
    inputText: '',

    currentPage: 0,
    populars: [],
    rowCount: 10,

    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,

    saveType: '',
    saveModal: false

  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 600) / 64 + 1) 
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    await this.getRecommendData()
    await this.getPopularData()
  }

  getRecommendData = async (props) => {

    try {

      let result = await api.keyword().call({apiName: 'getRecommendTags', method: 'get'})

      this.setState({
        recommendTags: result
      })

    } catch (e) {

      console.error(e)
    }
  }

  getPopularData = async (props) => {

    try {

      let result = await api.keyword().call({apiName: 'getPopularTags', method: 'get', parameters: {
        requestType: this.state.requestType,
        isUsing: this.state.isUsing,
        searchText: this.state.searchText,
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let populars = result.populars

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        populars: populars,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleiOSTag = () => {
    this.setState({
      saveType: 'iOS'
    }, () => {
      this.toggleSaveModal()()
    })
  }

  handleAndroidTag = () => {
    this.setState({
      saveType: 'Android'
    }, () => {
      this.toggleSaveModal()()
    })
  }

  toggleSaveModal = modalType => () => {
    if (!modalType) {
      this.setState({
        saveModal: !this.state.saveModal,
      })
    }
  }

  handleSave = () => {
    this.saveRecommend()
    this.setState({
      deleteModal: false
    })
  }

  saveRecommend = async () => {
    let parameters = {
      osType: this.state.saveType,
      tags: this.state.recommendTags.Android
    }
    try {
      let result = await api.keyword().call({apiName: 'setRecommendTags', method: 'put', parameters: parameters})
      this.props.addNotification('저장되었습니다.')

      this.setState({
        saveType: '',
        saveModal: false
      })
    } catch (e) {
      console.error(e)
    }
  }

  handleTag = event => {
    event.preventDefault()
    let tagText = event.target.innerText
    let tag = tagText.substring(1)
    this.props.history.push(`/keyword/tag/${tag}`)
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        
        <Row key='A'>
          <Col>
            {
              this.state.recommendTags == null ? '' :
              <Card className="mb-3">
              <CardHeader>프로필 관심사 추천</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <div className="d-flex justify-content-between align-items-center mb-2">
                      <Label>iOS</Label> <Button outline className="mx-1" onClick={this.handleiOSTag} style={{height: '32px'}}>저장</Button>
                    </div>
                    <ReactTagInput
                      className="ios_tag_input"
                      tags={this.state.recommendTags.iOS.map(tag => `#${tag}`)}
                      removeOnBackspace={true}
                      editable={true}
                      readOnly={false}
                      onChange={(newTags) => {
                        let recommendTags = this.state.recommendTags
                        recommendTags.iOS = newTags.map(tag => tag.replace('#', ''))
                        this.setState({
                          recommendTags: recommendTags
                        })
                      }}
                      validator={(value) => {

                        let replaced = value

                        if (replaced.includes(' ')) {
                          replaced = replaced.replace(/\s/gi, '')
                        }
                        
                        if (replaced.includes('#')) {
                          replaced = replaced.replace(/#/gi, '')
                        }

                        if (replaced.trim().length == 0) {
                          return false
                        }

                        if (this.state.recommendTags.Android.includes(replaced)) {
                          this.props.addNotification('이미 추가된 태그입니다.')
                          return false
                        }

                        if (replaced != value) {
                          let recommendTags = this.state.recommendTags
                          recommendTags.iOS.push(replaced)
                          this.setState({
                            recommendTags: recommendTags
                          })
                          setTimeout(() => {
                            document.querySelectorAll('.react-tag-input__input').forEach(input => {
                              input.value = ''
                            })
                          }, 50);
                          return false
                        }

                        return true
                      }}
                    />
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <div className="d-flex justify-content-between align-items-center mb-2">
                      <Label>Android</Label> <Button outline className="mx-1" onClick={this.handleAndroidTag} style={{height: '32px'}}>저장</Button>
                    </div>
                    <ReactTagInput
                      tags={this.state.recommendTags.Android.map(tag => `#${tag}`)}
                      removeOnBackspace={true}
                      editable={true}
                      readOnly={false}
                      onChange={(newTags) => {
                        let recommendTags = this.state.recommendTags
                        recommendTags.Android = newTags.map(tag => tag.replace('#', ''))
                        this.setState({
                          recommendTags: recommendTags
                        })
                      }}
                      validator={(value) => {

                        let replaced = value

                        if (replaced.includes(' ')) {
                          replaced = replaced.replace(/\s/gi, '')
                        }
                        
                        if (replaced.includes('#')) {
                          replaced = replaced.replace(/#/gi, '')
                        }

                        if (replaced.trim().length == 0) {
                          return false
                        }

                        if (this.state.recommendTags.Android.includes(replaced)) {
                          this.props.addNotification('이미 추가된 태그입니다.')
                          return false
                        }

                        if (replaced != value) {
                          let recommendTags = this.state.recommendTags
                          recommendTags.Android.push(replaced)
                          this.setState({
                            recommendTags: recommendTags
                          })
                          setTimeout(() => {
                            document.querySelectorAll('.react-tag-input__input').forEach(input => {
                              input.value = ''
                            })
                          }, 50);
                          return false
                        }

                        return true
                      }}
                    />
                  </Col>
                </Row>
              </CardBody>
            </Card>
            }
            <Card className="mb-3">
              <CardHeader>인기 태그 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">

                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th>태그</th>
                            <th width="120">사용횟수</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.populars.map( (popular, index) => {

                              return (
                                <>
                                  <tr>
                                    <td className="align-middle"><a href="#" onClick={this.handleTag}>#{popular.tag}</a></td>
                                    <td className="align-middle">{popular.count}</td>
                                  </tr>
                                  
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.saveModal}
          toggle={this.toggleSaveModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleSaveModal()}>{'추천 관심사 저장'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">현재 입력되어있는 관심사 태그를 저장합니다. 저장 즉시 {this.state.saveType} 앱에 반영되며, iOS, Android 각각 다르게 적용되니 참고하세요.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSaveModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleSave}>
              저장
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default RecommendTagPage;
