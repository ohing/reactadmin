var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')

router.get('/open-rooms', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10
  let status = req.query.status || 0
  let type = req.query.type || 0
  let anonymous = req.query.anonymous || 0
  let reportedType = req.query.reportedType || 0
  let searchText = (req.query.searchText || '').trim()

  let query
  let result
  let valueParameters

  let statusCondition = status == 1 ? `
    and cri.except_yn = 'N'
  ` : status == 2 ? `
    and cri.except_yn = 'Y'
  ` : ''

  let searchCondition = searchText.length == 0 ? '' : `
    and (
      cri.title like '%${searchText}%'
      or (
        case
          when cpi.chat_profile_id is not null then cpi.nick_nm like '%${searchText}%'
          else ui.user_login_id like '%${searchText}%'
        end
      )
      or (
        (select
          group_concat(concat('"', ti.tag_val, '"') separator ',')
        from 
          oi_chat_room_info cri3
          join oi_chat_room_tag crt on cri3.room_id = crt.room_id
          join oi_tag_info ti on crt.tag_id = ti.tag_id
        where 
          cri3.room_id = cri.room_id) like '%${searchText}%'
      )
    )
  `

  valueParameters = [searchText, searchText, searchText, searchText]
  console.log(valueParameters)

  let mysql = req.app.get('mysqlR')

  try {    

    query = `
      select sql_calc_found_rows
        cri.room_id, cri.title, cri.reg_dt created_at, count(cri.room_id) report_count, 
        ui.user_id master_user_id,
        case
          when cpi.chat_profile_id is not null then concat(cpi.nick_nm, ' (', ui.user_login_id, ')')
          else ui.user_login_id
        end master_nickname,
        concat(
          '[',
          (select
            group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
            oi_chat_room_info cri2
            join oi_chat_room_tag crt on cri2.room_id = crt.room_id
            join oi_tag_info ti on crt.tag_id = ti.tag_id
          where 
            cri2.room_id = cri.room_id
          order by
            crt.tag_seq asc
          ),
          ']'
        ) tags,
        substring_index(fi.org_file_nm, '/', -1) cover_file_name
      from 
        oi_chat_room_info cri
        left join oi_file_info fi on cri.room_image_id = fi.file_id
        left join oi_report r on r.target_type_cd = 'CHAT' and cri.room_id = r.target_id
        left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        left join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cri.room_type_cd = 'OPEN'
        and cri.room_status_cd = 'ACTIVE'
        ${statusCondition}
        ${searchCondition}
      group by
        cri.room_id
      order by
        count(cri.room_id) desc,
        cri.reg_dt desc
      limit 
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    query = `
      select found_rows() count
    `
    let countResult = await mysql.execute(query)
    let count = countResult[0].count

    if (result.length == 0) {
      res.send(JSON.stringify({
        rooms: [],
        totalCount: count
      }))
      return
    }

    for (let row of result) {
      row.tags = JSON.parse(row.tags)
    }
    
    let rooms = camelcase(result, {deep:true})

    res.send(JSON.stringify({
      rooms: rooms,
      totalCount: count
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/rooms/:roomId', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetRoomId = req.params.roomId
    
    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select
        cri.room_id, cri.title, cri.reg_dt created_at, count(cri.room_id) report_count, 
        cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count, cri.except_yn excepted,
        ui.user_id master_user_id,
        case
          when cpi.chat_profile_id is not null then concat(cpi.nick_nm, ' (', ui.user_login_id, ')')
          else ui.user_login_id
        end master_nickname,
        concat(
          '[',
          (select
          group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
          oi_chat_room_info cri2
          join oi_chat_room_tag crt on cri2.room_id = crt.room_id
          join oi_tag_info ti on crt.tag_id = ti.tag_id
          where 
          cri2.room_id = cri.room_id
          order by
          crt.tag_seq asc
          ),
          ']'
        ) tags,
        substring_index(fi.org_file_nm, '/', -1) cover_file_name
      from 
        oi_chat_room_info cri
        left join oi_file_info fi on cri.room_image_id = fi.file_id
        left join oi_report r on r.target_type_cd = 'CHAT' and cri.room_id = r.target_id
        left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        left join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cri.room_id = ${targetRoomId}
    `
    result = await mysql.execute(query)

    let room = result[0]
    room.tags = JSON.parse(room.tags)
    room.excepted = room.excepted == 'Y'
    room = camelcase(room, {deep:true})

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        am.target_type_cd = 'CHATROOM'
        and am.target_id = ${targetRoomId}
      order by memo_id desc
    `
    let memos = await mysql.execute(query)

    res.send(JSON.stringify(camelcase({room: room, memos: memos}, {deep: true})))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('rooms/:roomId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetRoomId = req.params.roomId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      update 
        oi_chat_room_info
      set
        except_yn = 'Y',
        upd_dt = now()
      where
        room_id = ${targetRoomId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'CHATROOM', ${targetRoomId}, '(시스템) 차단하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('rooms/:roomId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetRoomId = req.params.roomId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      update 
        oi_chat_room_info
      set
        except_yn = 'N',
        upd_dt = now()
      where
        room_id = ${targetRoomId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'CHATROOM', ${targetRoomId}, '(시스템) 차단 해제하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

module.exports = router