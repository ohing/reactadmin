export { default as NumberWidget } from './NumberWidget';
export { default as DualNumberWidget } from './DualNumberWidget';
export { default as IconWidget } from './IconWidget';
export { default as TextDetailWidget } from './TextDetailWidget';
