import React, {
  useRef
} from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon,
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

import API from 'utils/api'
const api = new API()

class TagPage extends React.Component {  

  tag = this.props.match.params.tag

  state = {
    tagInfo: null
  };

  componentDidMount() {
    console.log('DidMount')

    this.getData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  getData = async (props) => {

    try {

      let result = await api.keyword().call({apiName: 'getTagInfo', pathParameter: this.tag, method: 'get'})

      console.log('result', result)

      this.setState({
        tagInfo: result
      })

    } catch (e) {

      console.error(e)
    }
  }

  render() {

    console.log('render')

    return (
      <Page
        className="TagPage"
      >
        
        <Row key='A'>
          <Col>
            {
              this.state.tagInfo == null ? '' :
              <Card className="mb-3">
                <CardHeader>태그 정보: <b>{this.tag}</b></CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                        유저 관심사
                      </Label>
                      <Label for="ID" className="h-1" sm={7}>
                        <a href="#">{this.state.tagInfo.userCount}</a>
                      </Label>
                      <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                        게시물 태그
                      </Label>
                      <Label for="ID" className="h-1" sm={7}>
                        <a href="#">{this.state.tagInfo.feedCount}</a>
                      </Label>
                      <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                        오픈채팅방 태그
                      </Label>
                      <Label for="ID" className="h-1" sm={7}>
                        <a href="#">{this.state.tagInfo.chatCount}</a>
                      </Label>
                      <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                        사용한 유저 수
                      </Label>
                      <Label for="ID" className="h-1" sm={7}>
                        <a href="#">{this.state.tagInfo.tagUserCount}</a>
                      </Label>
                    </Col>
                  </Row>

                  
                </CardBody>
              </Card>
            } 
          </Col>
        </Row>

        {/* <Modal
          isOpen={this.state.saveModal}
          toggle={this.toggleSaveModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleSaveModal()}>{'추천 관심사 저장'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">현재 입력되어있는 관심사 태그를 저장합니다. 저장 즉시 {this.state.saveType} 앱에 반영되며, iOS, Android 각각 다르게 적용되니 참고하세요.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSaveModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleSave}>
              저장
            </Button>{' '}
            
          </ModalFooter>
        </Modal> */}
        
      </Page>
    ); 
  }
};

export default TagPage;
