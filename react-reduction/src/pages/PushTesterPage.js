import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class PushTesterPage extends React.Component {

  state = {
    currentPage: 0,
    testers: [],
    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    addModal: false,
    addingTester: '',
    deleteModal: false,
    useModal: false,
    useModalType: 'USE',
    selectedIds: new Set([])
  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 
    console.log('DidMount')
    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {
    // let lastTesterState = window.localStorage.getItem('lastPushTesterState')
    // if (lastTesterState != null) {
    //   let state = JSON.parse(lastTesterState)
    //   state.selectedIds = new Set([])
    //   this.setState(state)
    // } else {
    //   this.getData()
    // }
    this.getData()
  }
  
  getData = async (props) => {

    try {

      let result = await api.push().call({apiName: 'getTesters', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount
      }})

      let totalCount = result.totalCount
      let testers = result.testers

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        testers: testers,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
        addingTester: '',
        selectedIds: new Set([])
      })

      console.log(JSON.stringify(this.state))

      // window.localStorage.setItem('lastPushTesterState', JSON.stringify(this.state))      

    } catch (e) {

      console.log(e)
    }
  }

  deleteTesters = async () => {

    try {

      await api.push().call({apiName: 'deleteTesters', method: 'delete', parameters: {
        userIds: Array.from(this.state.selectedIds)
      }})

      this.getData()
      this.props.addNotification('삭제되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  setUse = async () => {

    try {

      let userIds = Array.from(this.state.selectedIds)

      await api.push().call({apiName: 'testersToUse', method: 'post', parameters: {
        userIds: userIds
      }})

      let testers = this.state.testers

      for (let tester of testers) {
        if (userIds.includes(tester.userId)) {
          tester.useYn = 'Y'
          continue
        }
      }

      this.setState({
        testers: testers,
        selectedIds: new Set([])
      })
      this.props.addNotification('설정되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  setUnuse = async () => {

    try {

      let userIds = Array.from(this.state.selectedIds)

      await api.push().call({apiName: 'testersToUnuse', method: 'post', parameters: {
        userIds: Array.from(this.state.selectedIds)
      }})

      let testers = this.state.testers

      for (let tester of testers) {
        if (userIds.includes(tester.userId)) {
          tester.useYn = 'N'
          continue
        }
      }

      this.setState({
        testers: testers,
        selectedIds: new Set([])
      })
      this.props.addNotification('설정되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleUser = event => {
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/user/${targetUserId}`) 
  }

  handleAddButton = () => {
    this.toggleAddModal()()
  }

  toggleAddModal = modalType => () => {
    if (!modalType) {
      this.setState({
        addModal: !this.state.addModal,
      })
    }
  }

  handleAddingTester = event => {
    this.setState({
      addingTester: event.target.value
    })
  }

  handleAdd = async () => {

    if (this.state.addingTester.length == 0) {
      this.props.addNotification('아이디를 입력해 주세요.')
      return
    }

    try {
      let result = await api.push().call({apiName: 'addTester', method: 'put', parameters: {
        accountId: this.state.addingTester
      }})
      this.getData()
      this.setState({
        addModal: false
      })

      this.props.addNotification('추가되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handleChecked = event => {
    let checked = event.target.checked
    let userId = parseInt(event.target.id.split(':')[1])
    let selectedIds = this.state.selectedIds
    if (checked) {
      selectedIds.add(userId)
    } else {
      selectedIds.delete(userId)
    }
    this.setState({
      selectedIds: selectedIds
    })
  }

  handleRemove = () => {
    if (this.state.selectedIds.size > 0) {
      this.toggleDeleteModal()()
    } else {
      this.props.addNotification('삭제할 유저를 선택해 주세요.')
    }
  }

  toggleDeleteModal = modalType => () => {
    if (!modalType) {
      this.setState({
        deleteModal: !this.state.deleteModal,
      })
    }
  }

  handleDelete = () => {
    this.deleteTesters()
    this.setState({
      deleteModal: false
    })
  }

  handleUse = () => {
    if (this.state.selectedIds.size > 0) {
      this.setState({
        useModalType: 'USE'
      })
      this.toggleUseModal()()
    } else {
      this.props.addNotification('사용 상태로 설정할 유저를 선택해 주세요.')
    }
  }

  handleUnuse = () => {
    if (this.state.selectedIds.size > 0) {
      this.setState({
        useModalType: 'UNUSE'
      })
      this.toggleUseModal()()
    } else {
      this.props.addNotification('사용안함 상태로 설정할 유저를 선택해 주세요.')
    }
  }

  toggleUseModal = modalType => () => {
    if (!modalType) {
      this.setState({
        useModal: !this.state.useModal,
      })
    }
  }

  handleUseSetting = () => {
    if (this.state.useModalType == 'USE') {
      this.setUse()
    } else {
      this.setUnuse()
    }
    this.setState({
      useModal: false
    })
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="PushTesterPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>테스터 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="40"></th>
                            <th>아이디</th>
                            <th>타입</th>
                            <th>사용여부</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.testers.map( (tester, index) => {

                              return (
                                <>
                                  <tr>
                                    <td><Input type="checkbox" className="mx-auto" id={`userId:${tester.userId}`} onChange={this.handleChecked} checked={this.state.selectedIds.has(tester.userId)}/></td>
                                    <td><a href="#" onClick={this.handleUser} value={tester.userId}>{tester.accountId}</a></td>
                                    <td>{tester.type == 'SUPER' ? '슈퍼관리자' : tester.type == 'OHING' ? '일반관리자' : '테스터'}</td>
                                    <td>{tester.useYn == 'Y' ? '사용' : '사용안함'}</td>
                                  </tr>
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                    <Button className="mx-1" color="secondary" onClick={this.handleAddButton}>추가</Button>
                    <Button className="mx-1" color="primary" onClick={this.handleRemove}>삭제</Button>
                    <Button className="mx-1" color="success" onClick={this.handleUse}>사용함</Button>
                    <Button className="mx-1" color="warning" onClick={this.handleUnuse}>사용안함</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.addModal}
          toggle={this.toggleAddModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleAddModal()}>{'푸시 테스터 추가'}</ModalHeader>
          <ModalBody>            
            <Input type="text" className="my-3" id="blockReason" placeholder="추가할 유저 아이디 입력(ohingmaster)" onChange={this.handleAddingTester} value={this.state.addingTester}/>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleAddModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleAdd}>
              추가
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>{'푸시 테스터 삭제'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택한 테스터를 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDelete}>
              삭제
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.useModal}
          toggle={this.toggleUseModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleUseModal()}>{'사용여부 설정'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">{this.state.useModalType == 'USE' ? '선택한 테스터에게 테스트 푸시를 보내도록 합니다.' : '선택한 테스터에게 테스트 푸시를 보내지 않도록 합니다.'}</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleUseModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleUseSetting}>
              설정
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default PushTesterPage;
