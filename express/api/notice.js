var express = require('express');
var router = express.Router();
var axios = require('axios');
var parser = require('node-html-parser')


const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3()
const cloudfront = new AWS.CloudFront();
const lambda = new AWS.Lambda()

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  let requestType = (req.query.requestType || '').trim()
  let isUsing = (req.query.isUsing || '').trim()
  let searchText = (req.query.searchText || '').trim()

  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')

    let category = {
      '1': '이벤트',
      '2': '공지',
      '3': '업데이트'
    }[requestType]

    let onoffYn = {
      '1': 'Y',
      '2': 'N'
    }[isUsing]

    let categoryCondition = (requestType.length == 0 || requestType == '0') ? '' : `
      and category = '${category}'
    `
    let onoffCondition = (isUsing.length == 0 || isUsing == '0') ? '' : `
      and onoff_yn = '${onoffYn}'
    `

    let searchTextCondition = searchText.length == 0 ? '' : `
      and title like '%${searchText}%'
    `

    query = `
      select sql_calc_found_rows
        ni.notice_id, ni.fix_yn fixed, ni.onoff_yn is_using, ni.category, ni.title, ni.content_url, ni.reg_dt created_at, ni.reg_user_id uiser_id,
        ui.user_login_id account_id
      from 
        oi_notice_info ni
        join oi_user_info ui on ni.reg_user_id = ui.user_id
      where
        1=1
        ${categoryCondition}
        ${onoffCondition}
        ${searchTextCondition}
      order by 
        ni.fix_yn desc, ni.notice_id desc
      limit
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)
    result = result.map(row => {
      row.fixed = row.fixed == 'Y'
      row.is_using = row.is_using == 'Y'
      return row
    })

    let notices = camelcase(result, {deep:true})

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      notices: notices,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/:noticeId/html', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let noticeId = req.params.noticeId

    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select
        content_url
      from
        oi_notice_info
      where
        notice_id = ${noticeId};
    `
    result = await mysql.execute(query)

    let url = result[0].content_url

    result = await axios.get(url)

    let html = result.data
    let parsed = parser.parse(html)
    console.log(html)

    let body = parsed.querySelector('hgcgroup').innerHTML

    console.log(url, body)

    res.send(JSON.stringify({
      html: body
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let userId = req.headers.userId

    let category = req.body.category
    let title = req.body.title
    let body = req.body.body

    if (title.length == 0 || body.length == 0) {
      res.status(403)
      res.send('Forbidden')
      return
    }
    
    let query
    let result
    
    let mysql = req.app.get('mysqlW')

    query = `
      insert into oi_notice_info (
        category, title, reg_dt, upd_dt, reg_user_id
      ) values (
        '${category}', '${title}', now(), now(), ${userId}
      );
    `
    console.log(query)
    result = await mysql.execute(query)
    let noticeId = result.insertId

    console.log(body)

    let bodyBuffer = Buffer.from(body)
    let key = `notice/${noticeId}.html`

    let params = {
      Bucket: `ohing-public-${process.env.stage}`,
      ContentType: 'text/html',
      ContentLanguage: 'utf-8',
      Body: bodyBuffer,
      Key: key
    }
    console.log(params)
    await s3.putObject(params).promise()

    console.log('tttt', process.env.stage)


    let url = `https://${process.env.stage == 'dev' ? 'd2ro0vwgwjzuou' : 'd257x8iyw0ju12'}.cloudfront.net/${key}`

    query = `
      update
        oi_notice_info
      set
        content_url = '${url}'
      where
        notice_id = ${noticeId};
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let userId = req.headers.userId

    let noticeIds = req.body.noticeIds
    if (!(Array.isArray(noticeIds) && noticeIds.length > 0)) {
      res.send('{}')
      return
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      delete from
        oi_notice_info
      where
        notice_id in (${noticeIds.join(',')});
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:noticeId', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let userId = req.headers.userId
    let noticeId = req.params.noticeId

    let category = req.body.category
    let title = req.body.title
    let body = req.body.body

    if (title.length == 0 || body.length == 0) {
      res.status(403)
      res.send('Forbidden')
      return
    }

    let bodyBuffer = Buffer.from(body)
    let key = `notice/${noticeId}.html`

    let params = {
      Bucket: `ohing-public-${process.env.stage}`,
      ContentType: 'text/html',
      ContentLanguage: 'utf-8',
      Body: bodyBuffer,
      Key: key
    }
    console.log(params)
    await s3.putObject(params).promise()

    let timestamp = new Date().getTime()

    params = {
      DistributionId: process.env.stage == 'dev' ? 'E2MKHYC1WFIEFZ' : 'E16EONPOZJ0HT8',
      InvalidationBatch: {
        CallerReference: `${timestamp}`,
        Paths: {
          Quantity: 1,
          Items: [
            `/${key}`
          ]
        }
      }
    }

    cloudfront.createInvalidation(params, (error, data) => {
      console.log('캐시 삭제', error, data)
    })
    
    let query
    let result
    
    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_notice_info
      set
        category= '${category}',
        title = '${title}',
        upd_dt = now(),
        upd_user_id = ${userId}
      where
        notice_id = ${noticeId};
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:noticeId', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:noticeId/fix', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let noticeId = req.params.noticeId

    let query    
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_notice_info
      set
        fix_yn = 'Y'
      where
        notice_id = ${noticeId};
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:noticeId/fix', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let noticeId = req.params.noticeId

    let query    
    let result

    let mysql = req.app.get('mysqlW')    

    query = `
      update
        oi_notice_info
      set
        fix_yn = 'N'
      where
        notice_id = ${noticeId};
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:noticeId/use', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let noticeId = req.params.noticeId

    let query    
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_notice_info
      set
        onoff_yn = 'Y'
      where
        notice_id = ${noticeId};
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:noticeId/use', async (req, res) => {

  console.log(req.headers, req.body)

  try {

    let noticeId = req.params.noticeId

    let query    
    let result

    let mysql = req.app.get('mysqlW')    

    query = `
      update
        oi_notice_info
      set
        onoff_yn = 'N'
      where
        notice_id = ${noticeId};
    `
    console.log(query)
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

module.exports = router