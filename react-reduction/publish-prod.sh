#!/bin/sh

while true; do
  read -p "Are you sure you want to publish new version of 'prod'? (yes or no) " yn
  case $yn in 
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --region us-east-1 --domain ohingmedia --domain-owner 862667126176 --query authorizationToken --output text`

echo 'File change for Production...'
mv .env.production .env.production_dev
mv .env.production_prod .env.production
mv package.json package_dev.json
mv package_prod.json package.json
mv ../express/.env ../express/.env.dev
mv ../express/.env.production ../express/.env

echo 'Build & Minifying...'
yarn build

echo 'Commit & Pushing...'
git add .
git commit -m "Publish new version via automatically."
git push origin master

echo 'Connect and Pulling...'
ssh -i "~/Downloads/ohing-public.pem" bitnami@3.34.106.103 -t "cd projects/reactadmin; git pull; pm2 restart all"

echo 'Changed file to restore...'
mv .env.production .env.production_prod
mv .env.production_dev .env.production
mv package.json package_prod.json
mv package_dev.json package.json
mv ../express/.env ../express/.env.production
mv ../express/.env.dev ../express/.env

echo 'Completed!'