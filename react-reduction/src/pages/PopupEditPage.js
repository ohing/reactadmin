import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class PopupEditPage extends React.Component {

  state = {

    partners: [],

    bannerId: null,

    position: '0',
    title: '',
    partnerId: '0',
    targetType: '0',
    targetId: null,
    targetUrl: '',
    sortOrder: '',
    beginDate: null,
    beginTime: null,
    endDate: null,
    endTime: null,
    hideUnit: '0',
    imageUrl: '',
    filePath: null,

    editModal: false
  };

  currentFile = null

  componentDidMount() {

    let banner = this.props.location.state.banner

    if (banner != null) {

      let beginDateString = banner.bannerBeginDt
      let endDateString = banner.bannerEndDt

      let begins = beginDateString.split('T')
      let beginDate = begins[0]
      let beginTime = begins[1].substring(0, 8)

      let ends = endDateString.split('T')
      let endDate = ends[0]
      let endTime = ends[1].substring(0, 8)

      let hiddenDays = banner.popupHiddenDay || 0
      if (hiddenDays > 30) {
        hiddenDays = 30
      }

      this.setState({
        bannerId: banner.bannerId,
        title: banner.bannerTitle,
        partnerId: banner.partnerId || '0',
        targetType: `${banner.targetType}` || '0',
        targetId: banner.targetId == null ? null : `${banner.targetId}`,
        targetUrl: banner.targetUrl || '',
        sortOrder: `${banner.bannerSortNo}`,
        beginDate: beginDate,
        beginTime: beginTime,
        endDate: endDate,
        endTime: endTime,
        hideUnit: `${hiddenDays}`,
        imageUrl: banner.imageUrl,
      })
    }

    // if (notice != null) {
    //   this.setState({
    //     notice: notice,
    //     category: category,
    //     title: notice.title
    //   }, () => {
    //     this.getHTML()
    //   })
    // }

    this.getData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }
  
  getData = async (props) => {

    try {

      let result = await api.partner().call({apiName: 'getPartners', method: 'get', parameters: {
        needsAll: 1
      }})

      let partners = result.partners

      this.setState({
        partners: partners
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePosition = event => {
    this.setState({
      position: event.target.value
    })
  }

  handleTitle = event => {
    this.setState({
      title: event.target.value
    })
  }

  handleTargetType = event => {

    let value = event.target.value

    let nextState = {
      targetType: value
    }

    if (!(value == '3' || value == '4' || value == '7')) {
      nextState.targetUrl = ''
    }
    if (!(value == '1' || value == '2' || value == '8')) {
      nextState.targetId = ''
    }    

    console.log(nextState)

    this.setState(nextState)
  }

  handleTargetId = event => {
    this.setState({
      targetId: event.target.value
    })
  }

  handleTargetUrl = event => {
    this.setState({
      targetUrl: event.target.value
    })
  }

  handleSortOrder = event => {
    this.setState({
      sortOrder: event.target.value
    })
  }

  handleBeginDate = event => {
    this.setState({
      beginDate: event.target.value
    })
  }

  handleBeginTime = event => {
    this.setState({
      beginTime: event.target.value
    })
  }

  handleEndDate = event => {
    this.setState({
      endDate: event.target.value
    })
  }

  handleEndTime = event => {
    this.setState({
      endTime: event.target.value
    })
  }

  handleHideUnit = event => {
    this.setState({
      hideUnit: event.target.value
    })
  }

  handleFile = event => {
    if (event.target.files.length > 0) {
      let file = event.target.files[0]
      this.currentFile = file
      let fileReader = new FileReader()
      fileReader.onload = event => {
        this.setState({
          filePath: event.target.result
        }) 
      }
      fileReader.readAsDataURL(file)
    }
  }

  handleCreateButton = () => {

    let title = this.state.title.trim()
    if (title.length == 0) {
      this.props.addNotification('제목을 입력해 주세요.')
      return
    }

    switch (this.state.targetType) {
      case '1':
      case '2':
      case '8': {

        let targetId = parseInt(this.state.targetId)

        if (this.state.targetType == '8') {
          if (!(Number.isInteger(targetId) && targetId > -1 && targetId < 5)) {
            this.props.addNotification('타겟 아이디를 0~4 범위에서 입력해 주세요.')
            return
          }
        } else {
          if (!(Number.isInteger(targetId) && targetId > 0)) {
            this.props.addNotification('자연수 형태의 타겟 아이디를 입력해 주세요.')
            return
          }
        }
        break
      }

      case '3':
      case '4':
      case '7': {

        let targetUrl = this.state.targetUrl.trim()
        if (targetUrl.length == 0) {
          this.props.addNotification('타겟 URL을 입력해 주세요.')
          return
        }

        try {
          let url = new URL(targetUrl)
          if (!(url.protocol === 'http:' || url.protocol === 'https:')) {
            this.props.addNotification('올바른 형태의 URL을 입력해 주세요.')
          }
        } catch (e) {
          this.props.addNotification('올바른 형태의 URL을 입력해 주세요.')
          return
        }

        break
      }

      default: {
        break
      }
    }

    let sortOrder = (this.state.sortOrder || '').trim()
    if (sortOrder.length > 0 && !Number.isInteger(parseInt(sortOrder))) {
      this.props.addNotification('정수 형태의 정렬 순서를 입력해 주세요.')
      return
    }

    if (this.state.beginDate == null || this.state.beginTime == null) {
      this.props.addNotification('시작시간을 입력해 주세요.')
      return
    }

    if (this.state.endDate == null || this.state.endTime == null) {
      this.props.addNotification('종료시간을 입력해 주세요.')
      return
    }

    if (this.currentFile == null && this.state.imageUrl == null) {
      this.props.addNotification('팝업 이미지를 선택해 주세요.')
      return
    }

    this.toggleCreateModal()()
  } 

  toggleCreateModal = modalType => () => {
    if (!modalType) {
      this.setState({
        createModal: !this.state.createModal,
      })
    }
  }

  handleCreate = async event => {

    this.setState({
      createModal: false
    })

    try {

      let bannerId = this.state.bannerId

      let beginDate = this.state.beginDate
      let beginTime = this.state.beginTime

      let endDate = this.state.endDate
      let endTime = this.state.endTime

      let beginDateString = `${beginDate} ${beginTime}`
      let endDateString = `${endDate} ${endTime}`

      let parameters = {
        title: this.state.title.trim(),
        openImmediately: this.state.openImmediately,
        beginTime: beginDateString,
        endTime: endDateString,
      }

      if (this.currentFile != null) {
        parameters.image = this.currentFile
      } else {
        parameters.imageUrl = this.state.imageUrl
      }

      let targetType = this.state.targetType
      console.log(targetType)
      if (targetType != '0') {
        parameters.targetType = targetType
        switch (targetType) {
          case '1':
          case '2':
          case '8': {
            parameters.targetId = parseInt(this.state.targetId)
            break
          }

          case '3':
          case '4':
          case '7': {
            parameters.targetUrl = this.state.targetUrl.trim()
            break
          }

          default:
            break
        }
      }

      let sortOrder = this.state.sortOrder.trim()
      if (sortOrder.length > 0) {
        parameters.sortOrder = sortOrder
      }

      parameters.hiddenDays = this.state.hideUnit

      console.log(parameters)

      let result = await api.popup().call({apiName: 'modifyPopup', method: 'put', pathParameter: bannerId, parameters: parameters, isMultipart: true})

      this.props.addNotification('팝업 정보가 수정되었습니다.')
      this.props.history.replace('/popup')

    } catch(e) {
      console.error(e)
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="BannerCreatePage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>팝업 편집</CardHeader>
              <CardBody>

                <Row>

                  <Label className="mx-3 mb-1">제목* (제목은 유저에게 노출되지 않지만, Firebase에 집계됩니다.)</Label>
                  <Input className="mx-3 mb-3" type="text" placeholder="제목 입력" onChange={this.handleTitle} value={this.state.title}/>

                  <Label className="mx-3 mb-1">타겟 대상</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.targetType} onChange={this.handleTargetType}>
                    <option value="0">이동안함</option>
                    <option value="1">글 상세</option>
                    <option value="2">프로필</option>
                    <option value="3">내부 브라우저</option>
                    <option value="4">외부 브라우저</option>
                    <option value="5">글작성</option>
                    <option value="6">도와줘작성</option>
                    <option value="7">공지사항</option>
                    <option value="8">탭</option>
                  </Input>

                  <Label className="mx-3 mb-1">{this.state.targetType == '1' || this.state.targetType == '2' || this.state.targetType == '8' ? '타겟 아이디*' : '타겟 아이디'}</Label>
                  {
                    this.state.targetType == '1' || this.state.targetType == '2' || this.state.targetType == '8' ? 
                    <Input className="mx-3 mb-3" type="text" placeholder={this.state.targetType == '1' ? '피드 아이디 입력' : this.state.targetType == '2' ? '유저 아이디 입력' : '가장 왼쪽 도와줘 탭부터 0~4 입력'} onChange={this.handleTargetId} value={this.state.targetId}/> :
                    <Input className="mx-3 mb-3" disabled type="text" placeholder="아이디 입력" onChange={this.handleTargetId} value={this.state.targetId == null ? '' : this.state.targetId}/>
                  }

                  <Label className="mx-3 mb-1">{this.state.targetType == '3' || this.state.targetType == '4' || this.state.targetType == '7' ? '타겟 URL*' : '타겟 URL'}</Label>
                  {
                    this.state.targetType == '3' || this.state.targetType == '4' || this.state.targetType == '7' ? 
                    <Input className="mx-3 mb-3" type="text" placeholder="url 입력" onChange={this.handleTargetUrl} value={this.state.targetUrl}/> :
                    <Input className="mx-3 mb-3" disabled type="text" placeholder="url 입력" onChange={this.handleTargetUrl} value={this.state.targetUrl}/>
                  }

                  <Label className="mx-3 mb-1">정렬 순서(미입력 시 같은 위치의 가장 마지막으로 지정됩니다.)</Label>
                  <Input className="mx-3 mb-3" type="text" placeholder="숫자 입력" onChange={this.handleSortOrder} value={this.state.sortOrder}/>

                  <Label className="mx-3 mb-1">시작시간*</Label>
                  <Input type="date" className="mx-3" name="beginDate" value={this.state.beginDate} onChange={this.handleBeginDate}/>
                  <Input type="time" className="mx-3 mt-1 mb-3" name="beginTime" value={this.state.beginTime} onChange={this.handleBeginTime}/>

                  <Label className="mx-3 mb-1">종료시간*</Label>
                  <Input type="date" className="mx-3" name="endDate" value={this.state.endDate} onChange={this.handleEndDate}/>
                  <Input type="time" className="mx-3 mt-1 mb-3" name="endTime" value={this.state.endTime} onChange={this.handleEndTime}/>

                  <Label className="mx-3 mb-1">숨김일수(0으로 설정할 경우 '다시 보지 않기' 가 됩니다.)</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.hideUnit} onChange={this.handleHideUnit}>
                    {
                      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30].map( (seq, index) => {
                        return (
                          <option value={`${seq}`}>{`${seq}`}</option>
                        ) 
                      })
                    } 
                  </Input>

                  <Label className="mx-3 mb-1">팝업 이미지* (1:1)
                  </Label>
                  <Input className="ml-3 mb-2" type="file" accept="image/png,image/jpeg,image/jpg" onChange={this.handleFile}/>
                  <CardImg
                    className="card-img ml-3 mb-2"
                    src={this.state.filePath != null ? this.state.filePath : this.state.imageUrl}
                    // key={media.mediaSeq}
                    style={{ width: 300, height: 300, objectFit: 'cover', objectPosition: 'center'}}
                  />

                </Row>

                <Button className="mt-3" color="secondary" onClick={this.handleCreateButton}>작성완료</Button>

              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.createModal}
          toggle={this.toggleCreateModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleCreateModal()}>{'팝업 편집'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">입력된 정보로 팝업을 수정합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleCreateModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleCreate}>
              수정
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default PopupEditPage;
