const fs = require('fs')

var express = require('express');

const multer = require('multer');
const upload = multer({
  dest: 'uploads/'
})

var router = express.Router();
var axios = require('axios');
var parser = require('node-html-parser')

const camelcase = require('camelcase-keys')
const { v4: uuidv4 } = require('uuid');

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3()
const cloudfront = new AWS.CloudFront();
const lambda = new AWS.Lambda()
const ses = new AWS.SES({apiVersion: '2010-12-01'})

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  try {

    let query
    let result

    let requestType = (req.query.requestType || '').trim()
    let status = (req.query.status || '').trim()

    let typeCondition = requestType == '0' ? '' : `
      and ask_type_cd = '${requestType}'
    `

    let statusCondition = status == '0' ? '' : `
      and ask_status_cd = '${status}'
    `

    let mysql = req.app.get('mysqlR')

    query = `
      select sql_calc_found_rows
        ua.ask_id, ua.ask_type_cd, 
        case 
          when ua.ask_type_cd = 'ASK001' then '가입/탈퇴'
          when ua.ask_type_cd = 'ASK002' then '게시물작성'
          when ua.ask_type_cd = 'ASK003' then '메신저'
          when ua.ask_type_cd = 'ASK004' then '도와줘'
          else null
        end ask_type,
        ua.ask_status_cd, 
        case
          when ua.ask_status_cd = 'ASK' then '대기중'
          when ua.ask_status_cd = 'VIEW' then '처리중'
          when ua.ask_status_cd = 'ANSWER' then '처리완료'
          else null
        end ask_status,
        ua.content, ua.ask_dt
      from 
        oi_user_ask ua
      where
        1=1
        ${typeCondition}
        ${statusCondition}
      order by 
        ua.ask_id desc
      limit
        ${page * rowCount}, ${rowCount};
    `
    result = await mysql.execute(query)
    let inquirements = camelcase(result, {deep: true})

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      inquirements: inquirements,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/:inquirementId', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  let inquirementId = req.params.inquirementId

  try {

    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select sql_calc_found_rows
        ua.ask_id, ua.ask_type_cd, 
        case 
          when ua.ask_type_cd = 'ASK001' then '가입/탈퇴'
          when ua.ask_type_cd = 'ASK002' then '게시물작성'
          when ua.ask_type_cd = 'ASK003' then '메신저'
          when ua.ask_type_cd = 'ASK004' then '도와줘'
          else null
        end ask_type,
        ua.ask_status_cd, 
        case
          when ua.ask_status_cd = 'ASK' then '대기중'
          when ua.ask_status_cd = 'VIEW' then '처리중'
          when ua.ask_status_cd = 'ANSWER' then '처리완료'
          else null
        end ask_status,
        ua.content, ua.ask_dt, ua.ask_user_id,
        ua.answer_content, ua.answer_dt, ua.answer_user_id,
        ua.answer_email, 
        ui.user_id, ui.user_login_id account_id,
        ui2.user_id answer_user_id, ui2.user_login_id answer_account_id
        
      from 
        oi_user_ask ua
        left join oi_user_info ui on ua.ask_user_id = ui.user_id
        left join oi_user_info ui2 on ua.answer_user_id = ui2.user_id 
      where
        ua.ask_id = ${inquirementId}
      limit
        1;
    `
    result = await mysql.execute(query)
    let inquirement = result[0]

    query = `
      select 
        fi.media_type_cd type, substring_index(fi.org_file_nm, '/', -1) file_name
      from 
        oi_user_ask_media_info ami 
          join oi_file_info fi on ami.file_id = fi.file_id
      where
        ami.ask_id = ${inquirementId};
    `
    result = await mysql.execute(query)
    inquirement.medias = result

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        am.target_type_cd = 'ASK'
        and am.target_id = ${inquirementId}
      order by memo_id desc
    `
    inquirement.memos = await mysql.execute(query)

    inquirement = camelcase(inquirement, {deep: true})

    res.send(JSON.stringify(inquirement))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:inquirementId/check', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId

  try {

    let inquirementId = req.params.inquirementId

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_user_ask
      set
        ask_status_cd = 'VIEW'
      where
        ask_id = ${inquirementId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'ASK', ${inquirementId}, '(시스템) 확인하였습니다.', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:inquirementId/answer', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId

  try {

    let inquirementId = req.params.inquirementId
    let answer = req.body.answer

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      select
        ua.answer_email, content, ua.ask_type_cd type, ui.user_login_id account_id, ui.user_status_cd, ua.ask_dt
      from
        oi_user_ask ua
        join oi_user_info ui on ua.ask_user_id = ui.user_id
      where
        ua.ask_id = ${inquirementId};
    `
    result = await mysql.execute(query)

    let userStatus = result[0].user_status_cd

    if (userStatus != 'ACTIVE') {
      res.send({'message': '이미 탈퇴한 회원입니다.'})
      return
    }

    let email = result[0].answer_email
    let question = result[0].content
    let type = result[0].type
    let category = {"ASK001": "가입/탈퇴", "ASK002": "게시물작성", "ASK003": "메신저", "ASK004": "도와줘"}[type] || ''
    let accountId = result[0].account_id
    let askDt = result[0].ask_dt
    console.log(askDt.toString())
    let createdAt = askDt.toString().replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '').substring(0, 16)

    let subject = '[오잉]문의하신 질문에 답변이 등록되었습니다.'

    let bodyBuffer = fs.readFileSync('answerEmail.html')
    let body = bodyBuffer.toString()

    let cloudFrontHost = process.env.stage == 'dev' ? 'd2ro0vwgwjzuou' : 'd257x8iyw0ju12'

    body = body
      .replace('[stage]', cloudFrontHost)
      .replace('[stage]', cloudFrontHost)
      .replace('[answer]', answer)
      .replace('[accountId]', accountId)
      .replace('[email]', email)
      .replace('[createdAt]', createdAt)
      .replace('[category]', category)
      .replace('[question]', question)

      console.log(answer)

    let params = {
      Destination: {
        ToAddresses: [
          email
        ]
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: body
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        }
      },
      Source: 'contact@ohing.net'
    }

    await ses.sendEmail(params).promise()

    query = `
      update
        oi_user_ask
      set
        ask_status_cd = 'ANSWER'
      where
        ask_id = ${inquirementId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'ASK', ${inquirementId}, '(시스템) 답변을 등록하였습니다. (${answer})', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})



module.exports = router