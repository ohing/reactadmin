import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class PushListPage extends React.Component {

  state = {
    currentPage: 0,
    messages: [],
    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,

    sendModal: false,
    sendType: '',
    sendPushId: null,

    createModal: false,
    title: '',
    body: '',
    landingType: 0,
    landingTarget: '',

    reserveModal: false,
    reserveDate: null,
    reserveTime: null,
    
    deleteModal: false,
    historyModal: false
  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 
    console.log('DidMount')
    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {
    
    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let result = await api.push().call({apiName: 'getMessages', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let messages = result.messages

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        messages: messages,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
        selectedIds: new Set([])
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })

    } catch (e) {

      console.log(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  toggleCreateModal = modalType => () => {

    if (!modalType) {
      this.setState({
        createModal: !this.state.createModal
      })
    }
  }

  handleAddButton = () => {
    this.toggleCreateModal()()
  }

  handleTitle = event => {

    let title = event.target.value

    console.log(title)

    this.setState({
      title: title
    })
  }

  handleBody = event => {

    let body = event.target.value

    console.log(body)

    this.setState({
      body: body
    })
  }
  
  handleLandingType = event => {

    let landingType = parseInt(event.target.value)

    console.log(landingType)

    this.setState({
      landingType: landingType
    })
  }

  handleLandingTarget = event => {

    let landingTarget = event.target.value

    console.log(landingTarget)

    this.setState({
      landingTarget: landingTarget
    })
  }

  handleContent = event => {
    event.preventDefault();
    let content = event.target.getAttribute('value')
    this.setState({
      currentContent: content
    }, () => {
      this.toggleContentModal()()
    })
  }

  toggleContentModal = modalType => () => {
    if (!modalType) {
      this.setState({
        contentModal: !this.state.contentModal,
      })
    }
  }

  handleCreate = async () => {

    let title = this.state.title.trim()
    let body = this.state.body.trim()

    if (title.length == 0 && body.length == 0) {
      this.props.addNotification('제목 또는 내용중 하나라도 입력해야 합니다.')
    }

    let landingType = this.state.landingType
    let landingTarget = this.state.landingTarget.trim()

    try {

      let result = await api.push().call({apiName: 'createMessage', method: 'put', parameters: {
        title: title,
        body: body,
        landingType: landingType,
        landingTarget: landingTarget
      }})
      this.getData()
      this.setState({
        createModal: false,
        title: '',
        body: '',
        landingType: null,
        landingTarget: null
      })

      this.props.addNotification('생성되었습니다.')
      
    } catch (e) {
      console.error(e)
    }
  }

  handleFeedLink = event => {
    event.preventDefault()
    let feedId = parseInt(event.target.innerText)
    this.props.history.push(`/feed/${feedId}`)  
  }
  
  handleUserLink = event => {
    event.preventDefault()
    let userId = parseInt(event.target.innerText)
    this.props.history.push(`/user/${userId}`)  
  }

  handleTestPush = event => {

    let pushId = parseInt(event.target.id.split(':')[1])

    this.setState({
      sendType: '테스트',
      sendPushId: pushId
    })

    this.toggleSendModal()()
  }

  handleWholePush = event => {

    let pushId = parseInt(event.target.id.split(':')[1])

    this.setState({
      sendType: '전체',
      sendPushId: pushId
    })

    this.toggleSendModal()()
  }

  handleReserve = event => {

    let pushId = parseInt(event.target.id.split(':')[1])

    console.log('pushId', pushId)

    let currentDate = new Date()
    let oneHourLater = new Date(currentDate.getTime() + 3600000)

    let year = `${oneHourLater.getFullYear()}`.padStart(2, '0')
    let month = `${oneHourLater.getMonth()+1}`.padStart(2, '0')
    let day = `${oneHourLater.getDate()}`.padStart(2, '0')
    let date = `${year}-${month}-${day}`

    let hour = `${oneHourLater.getHours()}`.padStart(2, '0')
    let minute = `${oneHourLater.getMinutes()}`.padStart(2, '0')
    
    let time = `${hour}:${minute}`

    this.setState({
      sendType: '예약',
      sendPushId: pushId,
      reserveDate: date,
      reserveTime: time
    })

    this.toggleReserveModal()()
  }

  toggleSendModal = modalType => () => {

    if (!modalType) {

      this.setState({
        sendModal: !this.state.sendModal,
      })
    }
  }

  handleSend = async () => {

    try {

      let apiName = this.state.sendType == '테스트' ? 'sendTestMessage' : 'sendWholeMessage'
      let result = await api.push().call({apiName: apiName, pathParameter: this.state.sendPushId, method: 'post'})

      this.props.addNotification(`${this.state.sendType} 메시지 발송에 성공하였습니다.`)
      this.setState({
        sendModal: false,
        title: '',
        body: '',
        landingType: 0,
        landingTarget: ''
      })

      this.getData()

    } catch (e) {
      console.error(e)
    }
  }

  toggleReserveModal = modalType => () => {

    if (!modalType) {

      this.setState({
        reserveModal: !this.state.reserveModal,
      })
    }
  }

  handleReserveDate = event => {
    console.log(event.target.value)
    this.setState({ reserveDate: event.target.value })
  }

  handleReserveTime = event => {
    this.setState({ reserveTime: event.target.value })
  }

  handleCancelReserve = async () => {

    try {

      let result = await api.push().call({apiName: 'cancelReserve', pathParameter: this.state.sendPushId, method: 'delete'})

      let messages = this.state.messages

      for (let i = 0; i < messages.length; i++) {
        if (messages[i].pushId == result.pushId) {
          messages[i].fireAt = null
          messages[i].fireType = null
          break
        }
      }

      this.setState({
        reserveModal: false,
        sendPushId: null,
        messages: messages
      })

      // window.localStorage.setItem('lastPushMessageState', JSON.stringify(this.state))

      this.props.addNotification('예약 발송 설정이 취소되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handleTestReserve = async () => {

    let date = this.state.reserveDate
    let time = this.state.reserveTime
    this.reserve(date, time, 'TEST')
  }

  handleWholeReserve = async () => {
    let date = this.state.reserveDate
    let time = this.state.reserveTime
    this.reserve(date, time, 'WHOLE') 
  }

  reserve = async (date, time, type) => {

    let parameters = {
      fireType: type,
      fireDate: date,
      fireTime: time
    }

    try {

      let result = await api.push().call({apiName: 'reserveMessage', pathParameter: this.state.sendPushId, parameters: parameters, method: 'post'})

      let messages = this.state.messages

      for (let i = 0; i < messages.length; i++) {
        if (messages[i].pushId == result.pushId) {
          messages[i].fireAt = result.fireAt
          messages[i].fireType = result.fireType
          break
        }
      }

      this.setState({
        reserveModal: false,
        sendPushId: null,
        messages: messages
      })

      // window.localStorage.setItem('lastPushMessageState', JSON.stringify(this.state))

      this.props.addNotification('예약 발송 설정이 완료되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handleChecked = event => {
    let checked = event.target.checked
    let pushId = parseInt(event.target.id.split(':')[1])
    let selectedIds = this.state.selectedIds
    if (checked) {
      selectedIds.add(pushId)
    } else {
      selectedIds.delete(pushId)
    }
    this.setState({
      selectedIds: selectedIds
    })
  }

  handleRemove = () => {

    if (this.state.selectedIds.size > 0) {
      this.toggleDeleteModal()()
    }
  }

  toggleDeleteModal = modalType => () => {

    if (!modalType) {
      this.setState({
        deleteModal: !this.state.deleteModal
      })
    }
  }

  handleDelete = () => {
    this.deleteMessages()
  }

  deleteMessages = async () => {

    let pushIds = Array.from(this.state.selectedIds)

    try {

      let result = await api.push().call({apiName: 'deleteMessages', method: 'delete', parameters: {
        pushIds: pushIds 
      }})

      this.props.addNotification('삭제되었습니다.')

      this.setState({
        deleteModal: false
      })

      this.getData()

    } catch (e) {
      console.error(e)
    } 
  }

  handleCount = (event) => {

    let pushId = parseInt(event.target.getAttribute('value'))
    let message = this.state.messages.filter(message => {
      return message.pushId == pushId
    })[0]

    this.setState({
      currentMessage: message,
    }, () => {
      this.toggleHistoryModal()()
    })
  }

  toggleHistoryModal = modalType => () => {

    if (!modalType) {
      this.setState({
        historyModal: !this.state.historyModal
      })
    }
  }

  
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>메시지 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="40"></th>
                            <th width="120">제목</th>
                            <th>내용</th>
                            <th width="80">랜딩타입</th>
                            <th width="120">랜딩목적지</th>
                            <th width="80">발송내역</th>
                            <th width="180">생성일시</th>
                            <th width="240">예약일시</th>
                            <th width="260"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.messages.map( (message, index) => {

                              return (
                                <>
                                  <tr>
                                    <td><Input type="checkbox" className="mx-auto align-middle" id={`userId:${message.pushId}`} onChange={this.handleChecked} checked={this.state.selectedIds.has(message.pushId)}/></td>
                                    <td className="text-truncate align-middle" style={{maxWidth:'120px'}}><a href="#" onClick={this.handleContent} value={message.title}>{message.title}</a></td>
                                    <td className="text-truncate align-middle" style={{maxWidth:'220px'}}><a href="#" onClick={this.handleContent} value={message.body}>{message.body}</a></td>
                                    <td className="align-middle">{message.targetType}</td>
                                    <td className="align-middle">{
                                      message.targetType == null 
                                        ? '' 
                                        : message.targetType == 'USER' 
                                          ? <a href="#" onClick={this.handleUserLink}>{message.targetId}</a>
                                          : message.targetType == 'FEED'
                                            ? <a href="#" onClick={this.handleFeedLink}>{message.targetId}</a>
                                            : message.targetType == 'WEB' 
                                              ? <a href={message.targetUrl} target="_blank">{message.targetUrl}</a> 
                                              : ''
                                    }</td>
                                    <td className="align-middle">{message.lastTargetCount > 0 ? <a href="#" value={message.pushId} onClick={this.handleCount}>{message.lastTargetCount}</a> : '발송전'}</td>
                                    <td className="align-middle">{message.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                    <td className="align-middle">{message.fireAt == null ? '' : `${message.fireAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')} ${message.fireType == 'TEST' ? '(T)' : '(A)'}`}</td>
                                    <td className="align-middle">
                                      <Button outline className="mx-1" id={`push:${message.pushId}`} color="secondary" height="20" onClick={this.handleTestPush} style={{height: '32px'}}>테스트</Button>
                                      <Button outline className="mx-1" id={`push:${message.pushId}`} color="danger" onClick={this.handleWholePush} style={{height: '32px'}}>전체발송</Button>
                                      <Button outline className="mx-1" id={`push:${message.pushId}`} color="dark" onClick={this.handleReserve} style={{height: '32px'}}>예약설정</Button>
                                    </td>
                                  </tr>
                                  
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                    <Button className="mx-1" color="secondary" onClick={this.handleAddButton}>생성</Button>
                    <Button className="mx-1" color="primary" onClick={this.handleRemove}>삭제</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.contentModal}
          toggle={this.toggleContentModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleContentModal()}>텍스트 자세히 보기</ModalHeader>
          <ModalBody>
            <Label>
              {this.state.currentContent}
            </Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleContentModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.createModal}
          toggle={this.toggleCreateModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleCreateModal()}>{'푸시 메시지 생성'}</ModalHeader>
          <ModalBody>            

            <Label >제목</Label>
            <Input type="text" placeholder="제목 입력" onChange={this.handleTitle} value={this.state.title}/>
            <br/>
            <Label >내용</Label>
            <Input type="textarea" placeholder="내용 입력" onChange={this.handleBody} value={this.state.body}/>
            <br/>
            <Label>랜딩타입</Label>
            <Input type="select" value={this.state.landingType} onChange={this.handleLandingType}>
              <option value="0">없음</option>
              <option value="1">게시물</option>
              <option value="2">유저</option>
              <option value="3">웹페이지</option>
            </Input>
            <br/>
            <Label >랜딩목적지</Label>
            <Input type="text" placeholder="목적지 ID 또는 URL 입력" onChange={this.handleLandingTarget} value={this.state.landingTarget}/>

          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleCreateModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleCreate}>
              생성
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.sendModal}
          toggle={this.toggleSendModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleSendModal()}>{`${this.state.sendType} 메시지 발송`}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">{this.state.sendType == '테스트' ? '테스트 계정으로 등록된 사용자의 기기에만 메시지를 전송합니다.' : '모든 유저의 기기에 메시지를 전송합니다.'}</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSendModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleSend}>
              발송
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.reserveModal}
          toggle={this.toggleReserveModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleReserveModal()}>메시지 발송 예약</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">* 현재 시간과 같거나 과거의 시간으로 예약할 경우 발송되지 않습니다.</Label>
            <Label className="ml-2">* 한 메시지 당 하나의 예약만 가능합니다.</Label>
            <Label className="ml-2">* 예약된 상태에서 다시 예약 시 기존 예약이 취소됩니다.</Label>
            <Form>
              <Input type="date" name="reserveDate" value={this.state.reserveDate} onChange={this.handleReserveDate}/>
              <Input type="time" name="reserveTime" value={this.state.reserveTime} onChange={this.handleReserveTime}/>
            </Form>
          </ModalBody>
          <ModalFooter> 
            <Button color="primary" onClick={this.handleCancelReserve}>
              예약 취소
            </Button>
            <Button color="secondary" onClick={this.handleTestReserve}>
              테스트 예약
            </Button>
            <Button color="danger" onClick={this.handleWholeReserve}>
              전체 발송 예약
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>메시지 삭제</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택된 메시지들을 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter> 
            <Button color="primary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="secondary" onClick={this.handleDelete}>
              삭제
            </Button>
            {' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.historyModal}
          toggle={this.toggleHistoryModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleHistoryModal()}>발송 내역</ModalHeader>
          <ModalBody>            
            <Table>
              <thead>
                <tr>
                  <th>대상</th>
                  <th>발송시간</th>
                  <th>대상기기수</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.currentMessage == null ? '' : this.state.currentMessage.history.map( (message, index) => {
                    return (
                      <>
                        <tr>
                          <td className="align-middle">{message.fireType == 'TEST' ? '테스터에게만' : '전체 회원'}</td>
                          <td className="align-middle">{message.fireDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                          <td className="align-middle">{message.targetCount}</td>
                        </tr>
                      </>
                    )
                  })
                }
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter> 
            <Button color="primary" onClick={this.toggleHistoryModal()}>
              확인
            </Button>
            {' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default PushListPage;
