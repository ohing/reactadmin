import React from 'react';
import Avatar from 'components/Avatar';

import defaultProfileImage from 'assets/img/users/default_200.png';
import defaultBackgroundImage from 'assets/img/users/default_profile_bg.png';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  CardImg,
  Col, 
  Row, 
  // Form,
  FormGroup,
  // FormFeedback,
  Label,
  Input,
  Button,
  // Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class ChatRoomPage extends React.Component {

  state = {

    room: null,

    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
    memoText: '',
    memos: [],
    modal: false,
    blockReason: '',
  };

  targetRoomId = this.props.match.params.roomId
  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {
    console.log('DidMount')
    console.log(this.props)

    this.getData()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevProps.match.params.userId != this.props.match.params.userId) {
      this.targetFeedId = this.props.match.params.userId
      window.scrollTo(0, 0);
      this.getData()
    }
  }

  getData = async () => {
    
    let result = await api.chat().call({apiName: 'getRoomInfo', pathParameter: this.targetRoomId, method: 'get'})

    this.setState({
      room: result.room,
      status: result.status,
      memos: result.memos,
      modal: false,
    })
  }

  handleMaster = event => {
    event.preventDefault()
    let userId = event.target.getAttribute('value')
    this.props.history.push(`/user/${userId}`) 
  }

  handleTag = event => {
    event.preventDefault()
    let tag = event.target.getAttribute('value')
    this.props.history.push(`/tag/${tag}`) 
  }

  handleMemoUser = event => {
    event.preventDefault()
    let userId = event.target.getAttribute('value')
    this.props.history.push(`/user/${userId}`) 
  }

  handleMemo = event => {
    this.setState({
      memoText: event.target.value
    })
  }

  handleMemoRegister = async () => {
    let memo = this.state.memoText.trim()

    if (memo.length == 0) {
      this.props.addNotification('메모를 입력해 주세요.')
      return
    }

    try {
      let parameters = {
        targetType: 'CHATROOM',
        targetId: this.targetRoomId,
        memo: memo
      }
      let result = await api.memo().call({apiName: 'registerMemo', method: 'put', parameters: parameters})

      let memos = [result, ...this.state.memos]

      this.setState({
        memos: memos,
        memoText: ''
      })

    } catch (e) {

      console.log(e)
    }
  }

  toggleBlockModal = modalType => () => {

    if (!modalType) {
      this.setState({
        modal: !this.state.modal,
      })
    }
  }

  handleBlockReason = event => {
    this.setState({
      blockReason: event.target.value
    })
  }

  handleBlock = async () => {

    try {

      let apiName = this.state.room.excepted ? 'unblockRoom' : 'blockRoom'
      let method = this.state.room.excepted ? 'delete' : 'put'

      let message = this.state.room.excepted ? '차단 해제되었습니다.' : '차단되었습니다.'

      let result = await api.chat().call({apiName: apiName, method: method, pathParameter: this.targetRoomId, parameters: {reason: this.state.blockReason}})
      let memos = [result, ...this.state.memos]

      this.state.room.excepted = !this.state.room.excepted

      this.setState({
        modal: false,
        room: this.state.room,
        memos: memos,
        blockReason: '',
      })
       
      this.props.addNotification(message)

    } catch (e) {
      console.error(e)
    }
  }
  
  handleMedia = event => {

    let mediaUrl = event.target.getAttribute('src') 

    let url = new URL(mediaUrl)
    let paths = url.pathname.split('/')
    let fileName = paths[paths.length-1]
    let pureName = fileName.split('.')[0]

    mediaUrl = `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`

    this.setState({
      currentMediaUrl: mediaUrl,
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="ChatRoomPage"
      >
        {
          this.state.room == null ? '' : 
          <>
            <Row key='A'>
              <Col>
                <Card className="mb-3">
                  <CardHeader>채팅방 정보</CardHeader>
                  <CardBody>
                    <Row>
                      <Col sm={5}>
                        <Row>
                          <Label for="IDHeader" className="font-weight-bold h-1" sm={4}>
                            아이디
                          </Label>
                          <Label for="ID" className="h-1" sm={6}>
                            {this.state.room.roomId}
                          </Label>
                        </Row>
                        <Row>
                          <Label for="StatusHeader" className="font-weight-bold h-1" sm={4}>
                            상태
                          </Label>
                          <Label for="Status" className="h-1" sm={6}>
                            {this.state.room.excepted ? '숨겨짐' : '정상'}
                          </Label>
                        </Row>
                        <Row>
                          <Label for="RoomMasterHeader" className="font-weight-bold h-1" sm={4}>
                            방장
                          </Label>
                          <Label for="RoomMasterNickname" className="h-1" sm={6}>
                            <a value={this.state.room.masterUserId} href="#" onClick={this.handleMaster}>{this.state.room.masterNickname}</a>
                          </Label>
                        </Row>
                        <Row>
                          <Label for="CreatedAtHeader" className="font-weight-bold h-1" sm={4}>
                            생성일시
                          </Label>
                          <Label for="CreatedAt" className="h-1" sm={6}>
                            {this.state.room.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}
                          </Label>
                        </Row>
                        <Row>
                          <Label for="TagHeader" className="font-weight-bold h-1" sm={3}>
                            태그
                          </Label>
                          <Label for="Tag" className="h-1" sm={7}>
                          {
                            this.state.room.tags.map( (tag, i) => {
                              return (
                                <a key={tag} className="mr-1" href="#" value={tag} onClick={this.handleTag}>#{tag}</a>
                              )
                            })
                          } 
                          </Label>
                        </Row>

                      </Col>

                      <Col sm={7}>

                        <Row>
                          <Label for="CoverHeader" className="font-weight-bold h-1" sm={3}>
                            커버이미지
                          </Label>
                          {
                            this.state.room.coverFileName == null ? '' :
                            (
                              
                              <>
                                <CardImg
                                  className="card-img ml-1 can-click"
                                  src={`${this.mediaCloudfrontUrl}/img/${this.state.room.coverFileName.split('.')[0]}/${this.state.room.coverFileName}`}
                                  value={this.state.room.roomId}
                                  style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                                  onClick={this.handleMedia}/>
                              </>
                            )
                          }
                        </Row>

                        <Row>
                          <br/>
                        </Row>
                        
                        <Row>
                          <Label for="ReportedHeader" className="font-weight-bold h-1" sm={3}>
                            신고당함
                          </Label>
                          <Label for="Reported" className="h-1" sm={7}>
                            <a className="mr-1" href="#" value={this.state.room.roomId} onClick={this.handleReported}>{this.state.room.reportCount}</a>
                          </Label>
                        </Row>

                      </Col>

                    </Row>
                    {/* <Row>
                      {
                        this.state.medias.map( (media, index) => {

                          let fileName = media.fileName
                          let pureName = fileName.split('.')[0]

                          let mediaUrl = media.type === 'MOVIE' 
                          ? `${this.mediaCloudfrontUrl}/mov/${pureName}/jpg/${pureName}.0000000.jpg`
                          : `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`

                          console.log(mediaUrl)

                          let icon = media.type === 'MOVIE'
                          ? <MdVideocam className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,20%)'}}/>
                          : <MdPhoto className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,20%)'}}/>
                          
                          return (
                            <>
                              <CardImg
                                className="card-img ml-1 mb-2 can-click"
                                src={mediaUrl}
                                value={media.feedId}
                                key={media.mediaSeq}
                                seq={media.mediaSeq}
                                type={media.type}
                                style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                                onClick={this.handleMedia}/>
                              {icon}
                            </>
                          )
                        })
                      }
                    </Row> */}

                  </CardBody>
                  {
                    this.state.status === '' ? '' :
                    <>
                    <Button className="mx-3 mb-3" color={this.state.room.excepted ? 'warning' : 'dark'} onClick={this.toggleBlockModal()}>{this.state.room.excepted ? '차단 해제' : '차단'}</Button>
                    <Modal
                      isOpen={this.state.modal}
                      toggle={this.toggleBlockModal()}
                      className={this.props.className}>
                      <ModalHeader toggle={this.toggleBlockModal()}>{this.state.room.excepted ? '게시물 차단 해제' : '게시물 차단'}</ModalHeader>
                      <ModalBody>
                        <Label>{this.state.status === '차단됨' ? '이 게시물을 차단 해제하시겠습니까?' : '이 게시물을 차단하시겠습니까?'}</Label>
                        <Input type="text" className="my-3" id="blockReason" placeholder={this.state.room.excepted ? '차단 해제 사유를 입력해주세요.' : '차단 사유를 입력해주세요.'} onChange={this.handleBlockReason} value={this.state.blockReason}/>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={this.toggleBlockModal()}>
                          취소
                        </Button>
                        <Button color={this.state.status === '차단됨' ? 'warning' : 'dark'} onClick={this.handleBlock}>
                          {this.state.room.excepted ? '차단 해제' : '차단'}
                        </Button>{' '}
                        
                      </ModalFooter>
                    </Modal>
                    </>
                  }

                </Card>

                <Card className="mb-3">
                <CardHeader>메모 등록</CardHeader>
                  <CardBody>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label>* 메모는 한 번 등록하면 삭제할 수 없습니다.</Label> <Button className="float-right mx-1 mb-3" color="secondary" onClick={this.handleMemoRegister}>등록</Button>
                          <Input type="textarea" name="text" value={this.state.memoText} onChange={this.handleMemo} placeholder="메모를 입력해 주세요."/>
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>

                <Card className="mb-3">
                <CardHeader>메모</CardHeader>
                  <CardBody>
                    <Row>
                      <Col>
                        {
                          this.state.memos.map( (memo, i) => {

                            return (
                              <div key={memo.memoId}>
                                <Label><a href="#" onClick={this.handleMemoUser} value={memo.userId}>{memo.userLoginId}</a></Label>
                                <Label>&nbsp;&nbsp;{memo.regDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</Label>
                                <br/>
                                <Label dangerouslySetInnerHTML={{__html: memo.memo}}></Label>
                                <br/>
                                <br/>
                              </div>
                            )
                          })
                        }
                      </Col>
                    </Row>
                  </CardBody>
                </Card>

              </Col>
            </Row>
          </>
        }

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>미디어 자세히 보기</ModalHeader>
          <ModalBody>
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
          <ModalFooter>

            {/* <Button color="dark" onClick={this.downloadImage}>
              다운로드
            </Button> */}
            
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

      </Page>
    ); 
  }
};


export default ChatRoomPage;
