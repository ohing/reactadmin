import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class PopularKeywordPage extends React.Component {

  state = {
    requestType: '0',
    isUsing: '0',
    searchText: '',
    currentPage: 0,
    keywords: [],
    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    
  };

  componentDidMount() {
    this.state.rowCount = parseInt((window.innerHeight - 508) / 64 + 1) 
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let result = await api.keyword().call({apiName: 'get', method: 'getSearchKeywords', parameters: {
        requestType: this.state.requestType,
        isUsing: this.state.isUsing,
        searchText: this.state.searchText,
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let keywords = result.keywords

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }
      }

      console.log('keywords', result)

      this.setState({
        keywords: keywords,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
        selectedIds: new Set([])
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>검색어 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">

                      {/* <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="40"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.keywords.map( (keyword, index) => {

                              return (
                                <>
                                  <tr>
                                    <td><Input type="checkbox" className="mx-auto align-middle" id={`noticeId:${notice.noticeId}`} onChange={this.handleChecked} checked={this.state.selectedIds.has(notice.noticeId)}/></td>
                                    <td className="text-truncate align-middle" style={{maxWidth:'120'}}><a href={notice.contentUrl}>{notice.title}</a></td>
                                    <td className="align-middle">{notice.fixed ? '고정됨' : ''}</td>
                                    <td className="align-middle">{notice.isUsing ? '사용함' : '사용안함'}</td>
                                    <td className="align-middle">{notice.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                    <td className="align-middle">
                                      <Button outline className="mx-1" id={`notice:${notice.noticeId}`} color="dark" onClick={this.handleEdit} style={{height: '32px'}}>편집</Button>
                                      <Button outline className="mx-1" id={`notice:${notice.noticeId}`} color={notice.fixed ? 'primary' : 'secondary'} onClick={this.handleFix} style={{height: '32px'}}>{notice.fixed ? '고정해제' : '상단고정'}</Button>
                                      <Button outline className="mx-1" id={`notice:${notice.noticeId}`} color={notice.isUsing ? 'primary' : 'secondary'} onClick={this.handleUse} style={{height: '32px'}}>{notice.isUsing ? '사용안함' : '사용'}</Button>
                                    </td>
                                  </tr>
                                  
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table> */}
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                    <Button className="mx-1" color="secondary" onClick={this.handleAddButton}>작성</Button>
                    <Button className="mx-1" color="primary" onClick={this.handleRemove}>삭제</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>{'공지사항 삭제'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택된 공지사항을 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDelete}>
              삭제
            </Button>{' '}
            
          </ModalFooter>
        </Modal> */}
        
      </Page>
    ); 
  }
};

export default PopularKeywordPage;
