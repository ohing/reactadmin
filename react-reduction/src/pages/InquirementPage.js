import React from 'react';
import Avatar from 'components/Avatar';

import defaultProfileImage from 'assets/img/users/default_200.png';
import defaultBackgroundImage from 'assets/img/users/default_profile_bg.png';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  CardImg,
  Col, 
  Row, 
  // Form,
  FormGroup,
  // FormFeedback,
  Label,
  Input,
  Button,
  // Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class InquirementPage extends React.Component {

  state = {

    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
    memoText: '',
    memos: [],
    inquirement: null,
    answerText: '',

    checkModal: false,
    answerModal: false,
  };

  inquirementId = this.props.match.params.inquirementId
  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {
    console.log('DidMount')
    console.log(this.props)

    this.getData()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevProps.match.params.inquirementId != this.props.match.params.inquirementId) {
      this.inquirementId = this.props.match.params.inquirementId
      window.scrollTo(0, 0);
      this.getData()
    }
  }

  getData = async () => {
    
    let result = await api.inquirement().call({apiName: 'getInquirement', pathParameter: this.inquirementId, method: 'get'})

    let inquirement = result
    let memos = inquirement.memos
    delete inquirement.memos

    this.setState({
      inquirement: inquirement,
      answerText: inquirement.answerContent,
      memos: memos,
    })
  }

  handleUser = event => {
    event.preventDefault()
    this.props.history.push(`user/${this.state.inquirement.userId}`)
  }

  handleCheck = async event => {
    event.preventDefault()

    this.toggleCheckModal()()
  }

  handleCheckAction = async () => {

    try {

      let result = await api.inquirement().call({apiName: 'check', pathParameter: this.inquirementId, method: 'post'})

      this.props.addNotification('상태가 변경되었습니다.')

      let memos = [result, ...this.state.memos]
      let inquirement = this.state.inquirement
      inquirement.askStatusCd = 'VIEW'
      inquirement.askStatus = ''
      
      this.setState({
        checkModal: false,
        memos: memos,
        inquirement: inquirement
      })

    } catch (e) {
      console.error(e)
    }
  }

  handleAnswer = event => {
    this.setState({
      answerText: event.target.value
    })
  }

  handleAnswerRegister = () => {

    let answer = this.state.answerText.trim()

    if (answer.length == 0) {
      this.props.addNotification('답변을 입력해 주세요.')
      return
    } else if (answer == this.state.inquirement.answerContent) {
      this.props.addNotification('최근 답변과 동일합니다.')
      return
    }

    this.toggleAnswerModal()()
  }

  handleAnswerAction = async () => {
    
    try {

      let answer = this.state.answerText.trim()

      let result = await api.inquirement().call({apiName: 'answer', pathParameter: this.inquirementId, method: 'post', parameters: {
        answer: answer
      }})

      if (result.message != null) {

        this.props.addNotification(result.message)

        this.setState({
          answerModal: false
        })

      } else {

        this.props.addNotification('답변이 전송되었습니다.')

        let memos = [result, ...this.state.memos]
        let inquirement = this.state.inquirement
        inquirement.askStatusCd = 'ANSWER'
        inquirement.askStatus = '처리완료'

        this.setState({
          answerModal: false,
          memos: memos,
          inquirement: inquirement
        })
      }

    } catch (e) {
      console.error(e)
    }
  }

  handleMemo = event => {
    this.setState({
      memoText: event.target.value
    })
  }

  handleMemoRegister = async () => {
    let memo = this.state.memoText.trim()

    if (memo.length == 0) {
      this.props.addNotification('메모를 입력해 주세요.')
      return
    }

    try {
      let parameters = {
        targetType: 'ASK',
        targetId: this.inquirementId,
        memo: memo
      }
      let result = await api.memo().call({apiName: 'registerMemo', method: 'put', parameters: parameters})

      let memos = [result, ...this.state.memos]

      this.setState({
        memos: memos,
        memoText: ''
      })

    } catch (e) {
      console.error(e)
    }
  }

  handleMemoUser = event => {
    
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))

    console.log(targetUserId)

    this.props.history.push(`/user/${targetUserId}`) 
  }

  toggleCheckModal = modalType => () => {

    if (!modalType) {

      this.setState({
        checkModal: !this.state.checkModal,
      })
    }
  }

  toggleAnswerModal = modalType => () => {

    if (!modalType) {

      this.setState({
        answerModal: !this.state.answerModal,
      })
    }
  }

  handleMedia = event => {

    let mediaUrl = event.target.getAttribute('src') 
    let mediaType = event.target.getAttribute('type') 

    if (mediaType === 'MOVIE') {
      let url = new URL(mediaUrl)
      let paths = url.pathname.split('/')
      let fileName = paths[paths.length-1]
      let pureName = fileName.split('.')[0]
      mediaUrl = `${this.mediaCloudfrontUrl}/ask/${pureName}.mp4`
    }

    this.setState({
      currentMediaType: mediaType,
      currentMediaUrl: mediaUrl
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="InquirementPage"
      >
        <Row key='A'>
          <Col>
            {
              this.state.inquirement === null ? '' :
              <Card className="mb-3">
                <CardHeader>문의 상세 정보</CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Row>
                        <Label for="IDHeader" className="font-weight-bold h-1" sm={3}>
                          아이디
                        </Label>
                        <Label for="ID" className="h-1" sm={7}>
                          {this.state.inquirement.askId}
                        </Label>
                      </Row>

                      <Row>
                        <Label for="DateHeader" className="font-weight-bold h-1" sm={3}>
                          문의유저
                        </Label>
                        <Label for="Date" className="h-1" sm={7}>
                          {<a href="#" onClick={this.handleUser}>{this.state.inquirement.accountId} ({this.state.inquirement.userId})</a>}
                        </Label>
                      </Row>

                      <Row>
                        <Label for="TypeHeader" className="font-weight-bold h-1" sm={3}>
                          문의유형
                        </Label>
                        <Label for="Type" className="h-1" sm={7}>
                          {this.state.inquirement.askType}
                        </Label>
                      </Row>

                      <Row>
                        <Label for="ContentHeader" className="font-weight-bold h-1" sm={3}>
                          문의내용
                        </Label>
                        <Label for="Content" className="h-1" sm={7}>
                          {this.state.inquirement.content}
                        </Label>
                      </Row>

                      <Row>
                        {
                          this.state.inquirement.medias.map( (media, index) => {

                            let fileName = media.fileName
                            let pureName = fileName.split('.')[0]

                            let mediaUrl = `${this.mediaCloudfrontUrl}/ask/200x200/${fileName}`

                            let icon = media.type === 'MOVIE'
                            ? <MdVideocam className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,10%)'}}/>
                            : <MdPhoto className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,10%)'}}/>

                            return (
                            <>
                              <CardImg
                                className="card-img ml-1 can-click"
                                src={mediaUrl}
                                type={media.type}
                                key={media.mediaSeq}
                                style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                                onClick={this.handleMedia}/>
                              {icon}
                            </>
                          )
                          })
                        }
                      </Row>

                      <Row>
                        <Label for="EmailHeader" className="font-weight-bold h-1" sm={3}>
                          답변 요청 이메일
                        </Label>
                        <Label for="Email" className="h-1" sm={7}>
                          {this.state.inquirement.answerEmail}
                        </Label>
                      </Row>

                      <Row>
                        <Label for="DateHeader" className="font-weight-bold h-1" sm={3}>
                          문의일시
                        </Label>
                        <Label for="Date" className="h-1" sm={7}>
                          {this.state.inquirement.askDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}
                        </Label>
                      </Row>
                      
                      <Row>
                        <Label for="StatusHeader" className="font-weight-bold h-1" sm={3}>
                          처리상태
                        </Label>
                        <Label for="Status" className="h-1" sm={7}>
                          {this.state.inquirement.askStatus}
                        </Label>  
                      </Row>
                      {
                        this.state.inquirement.askStatusCd === 'ASK' ? <Button color="secondary" onClick={this.handleCheck}>확인</Button> : ''
                      }
                      

                    </Col>
                    <Col sm={3}>
                    
                      
                    </Col>
                  </Row>

                </CardBody>

              </Card>
            }

            <Card className="mb-3">
            <CardHeader>답변 등록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label>* 답변 작성/변경 시 요청된 이메일로 해당 답변이 전송됩니다.</Label> <Button className="float-right mx-1 mb-3" color="secondary" onClick={this.handleAnswerRegister}>등록</Button>
                      <Input type="textarea" name="text" value={this.state.answerContent} onChange={this.handleAnswer} placeholder="답변을 입력해 주세요." value={this.state.answerText}/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card className="mb-3">
            <CardHeader>메모 등록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label>* 메모는 한 번 등록하면 삭제할 수 없습니다.</Label> <Button className="float-right mx-1 mb-3" color="secondary" onClick={this.handleMemoRegister}>등록</Button>
                      <Input type="textarea" name="text" value={this.state.memoText} onChange={this.handleMemo} placeholder="메모를 입력해 주세요."/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card className="mb-3">
            <CardHeader>메모</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    {
                      this.state.memos.map( (memo, i) => {

                        return (
                          <div key={memo.memoId}>
                            <Label><a href="#" onClick={this.handleMemoUser} value={memo.userId}>{memo.userLoginId}</a></Label>
                            <Label>&nbsp;&nbsp;{memo.regDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</Label>
                            <br/>
                            <Label dangerouslySetInnerHTML={{__html: memo.memo}}></Label>
                            <br/>
                            <br/>
                          </div>
                        )
                      })
                    }
                  </Col>
                </Row>
              </CardBody>
            </Card>

          </Col>
        </Row>

        <Modal
          isOpen={this.state.checkModal}
          toggle={this.toggleCheckModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleCheckModal()}>{'문의 확인'}</ModalHeader>
          <ModalBody>            
            <Label>확인 처리 시 요청된 이메일로 문의 내용을 확인했다는 내용이 전송됩니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleCheckModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleCheckAction}>
              확인
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.answerModal}
          toggle={this.toggleAnswerModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleAnswerModal()}>{'답변 등록'}</ModalHeader>
          <ModalBody>            
            <Label>입력한 내용으로 답변을 등록합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleAnswerModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleAnswerAction}>
              등록
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>미디어 자세히 보기</ModalHeader>
          <ModalBody>
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

      </Page>
    ); 
  }
};


export default InquirementPage;
