// import { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import { STATE_LOGIN } from 'components/AuthForm';
// import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from 'pages/AuthPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';
import {
  MdDone,
} from 'react-icons/md';
import NotificationSystem from 'react-notification-system';
import { NOTIFICATION_SYSTEM_STYLE } from 'utils/constants';

// const AuthModalPage = React.lazy(() => import('pages/AuthModalPage'));
// const ButtonGroupPage = React.lazy(() => import('pages/ButtonGroupPage'));
// const ChartPage = React.lazy(() => import('pages/ChartPage'));
// const DropdownPage = React.lazy(() => import('pages/DropdownPage'));
// const ModalPage = React.lazy(() => import('pages/ModalPage'));
// const ProgressPage = React.lazy(() => import('pages/ProgressPage'));
// const TypographyPage = React.lazy(() => import('pages/TypographyPage'));

const DashboardPage = React.lazy(() => import('pages/DashboardPage'));
const AdminPage = React.lazy(() => import('pages/AdminPage'));
const AddAdminPage = React.lazy(() => import('pages/AddAdminPage'));
const UsersPage = React.lazy(() => import('pages/UsersPage'));
const UserPage = React.lazy(() => import('pages/UserPage'));
const FeedsPage = React.lazy(() => import('pages/FeedsPage'));
const FeedPage = React.lazy(() => import('pages/FeedPage'));
const CommentsPage = React.lazy(() => import('pages/CommentsPage'));
const ChatRoomsPage = React.lazy(() => import('pages/ChatRoomsPage'));
const ChatRoomPage = React.lazy(() => import('pages/ChatRoomPage'));
const PushTesterPage = React.lazy(() => import('pages/PushTesterPage'));
const PushListPage = React.lazy(() => import('pages/PushListPage'));
const NoticeListPage = React.lazy(() => import('pages/NoticeListPage'));
const NoticeCreatePage = React.lazy(() => import('pages/NoticeCreatePage'));
const NoticeEditPage = React.lazy(() => import('pages/NoticeEditPage'));
const PopularKeywordPage = React.lazy(() => import('pages/PopularKeywordPage'));
const BannedKeywordPage = React.lazy(() => import('pages/BannedKeywordPage'));
const RecommendTagPage = React.lazy(() => import('pages/RecommendTagPage'));
const TagPage = React.lazy(() => import('pages/TagPage'));
const PartnerListPage = React.lazy(() => import('pages/PartnerListPage'));
const PartnerPage = React.lazy(() => import('pages/PartnerPage'));
const BannerListPage = React.lazy(() => import('pages/BannerListPage'));
const BannerCreatePage = React.lazy(() => import('pages/BannerCreatePage'));
const BannerEditPage = React.lazy(() => import('pages/BannerEditPage'));

const PopupListPage = React.lazy(() => import('pages/PopupListPage'));
const PopupCreatePage = React.lazy(() => import('pages/PopupCreatePage'));
const PopupEditPage = React.lazy(() => import('pages/PopupEditPage'));

const OhingOnListPage = React.lazy(() => import('pages/OhingOnListPage'));
const OhingOnCreatePage = React.lazy(() => import('pages/OhingOnCreatePage'));
const OhingOnEditPage = React.lazy(() => import('pages/OhingOnEditPage'));

const ReportListPage = React.lazy(() => import('pages/ReportListPage'));

const InquirementListPage = React.lazy(() => import('pages/InquirementListPage'));
const InquirementPage = React.lazy(() => import('pages/InquirementPage'));

const TablePage = React.lazy(() => import('pages/TablePage'));
const FormPage = React.lazy(() => import('pages/FormPage'));
const ButtonPage = React.lazy(() => import('pages/ButtonPage'));
const InputGroupPage = React.lazy(() => import('pages/InputGroupPage'));
const WidgetPage = React.lazy(() => import('pages/WidgetPage'));
const BadgePage = React.lazy(() => import('pages/BadgePage'));
const CardPage = React.lazy(() => import('pages/CardPage'));
const AlertPage = React.lazy(() => import('pages/AlertPage'));
const ModalPage = React.lazy(() => import('pages/ModalPage'));

const getBasename = () => {
  return `/`;
};

class App extends React.Component {

  addNotification = message => {
    this.notificationSystem.addNotification({
      title: <MdDone />,
      message: message,
      level: 'info',
    });
  }

  render() {
    return (
      <>
      <BrowserRouter basename={getBasename()}>
        <Switch>
          <LayoutRoute
            exact
            path="/login"
            layout={EmptyLayout}
            component={props => (
              <AuthPage {...props} authState={STATE_LOGIN} />
            )}
          />
          {/* <LayoutRoute
            exact
            path="/signup"
            layout={EmptyLayout}
            component={props => (
              <AuthPage {...props} authState={STATE_SIGNUP} />
            )}
          /> */}
          
          <MainLayout breakpoint={this.props.breakpoint}>
            <React.Suspense fallback={<PageSpinner />}>
              <Route exact path="/" render={ (props) => (
                <DashboardPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/admin" render={ (props) => (
                <AdminPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/admin/add" render={ (props) => (
                <AddAdminPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/user" render={ (props) => (
                <UsersPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/user/:userId" render={ (props) => (
                <UserPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/feed" render={ (props) => (
                <FeedsPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/feed/:feedId" render={ (props) => (
                <FeedPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/comment" render={ (props) => (
                <CommentsPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/chat/room" render={ (props) => (
                <ChatRoomsPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/chat/room/:roomId" render={ (props) => (
                <ChatRoomPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/push/tester" render={ (props) => (
                <PushTesterPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/push/message" render={ (props) => (
                <PushListPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/notice" render={ (props) => (
                <NoticeListPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/notice-create" render={ (props) => (
                <NoticeCreatePage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/notice/:noticeId" render={ (props) => (
                <NoticeEditPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/keyword/recommend" render={ (props) => (
                <RecommendTagPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/keyword/popular" render={ (props) => (
                <PopularKeywordPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/keyword/banned" render={ (props) => (
                <BannedKeywordPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/keyword/tag/:tag" render={ (props) => (
                <TagPage addNotification={this.addNotification} {...props} />
              )} /> 
              <Route exact path="/ad/partner" render={ (props) => (
                <PartnerListPage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ad/partner/:partnerId" render={ (props) => (
                <PartnerPage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ad/banner" render={ (props) => (
                <BannerListPage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ad/banner-create" render={ (props) => (
                <BannerCreatePage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ad/banner/:bannerId" render={ (props) => (
                <BannerEditPage addNotification={this.addNotification} {...props} />
              )} />

              <Route exact path="/popup" render={ (props) => (
                <PopupListPage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/popup-create" render={ (props) => (
                <PopupCreatePage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/popup/:bannerId" render={ (props) => (
                <PopupEditPage addNotification={this.addNotification} {...props} />
              )} />


              <Route exact path="/ohing-on" render={ (props) => (
                <OhingOnListPage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ohing-on-create" render={ (props) => (
                <OhingOnCreatePage addNotification={this.addNotification} {...props} />
              )} />
              <Route exact path="/ohing-on/:bannerId" render={ (props) => (
                <OhingOnEditPage addNotification={this.addNotification} {...props} />
              )} />

              <Route exact path="/report" render={ (props) => (
                <ReportListPage addNotification={this.addNotification} {...props} />
              )} />

              <Route exact path="/inquirement" render={ (props) => (
                <InquirementListPage addNotification={this.addNotification} {...props} />
              )} />

              <Route exact path="/inquirement/:inquirementId" render={ (props) => (
                <InquirementPage addNotification={this.addNotification} {...props} />
              )} />

              
              {/* <Route exact path="/tables" component={TablePage} />
              <Route exact path="/forms" component={FormPage} />
              <Route exact path="/buttons" component={ButtonPage} />
              <Route exact path="/input-groups" component={InputGroupPage} />
              <Route exact path="/widgets" component={WidgetPage} />
              <Route exact path="/badges" component={BadgePage} />
              <Route exact path="/cards" component={CardPage} />
              <Route exact path="/alerts" component={AlertPage} />
              <Route exact path="/modals" component={ModalPage} /> */}

              {/* <Route exact path="/login-modal" component={AuthModalPage} />
              <Route exact path="/buttons" component={ButtonPage} />
              
              
              <Route exact path="/typography" component={TypographyPage} />
              
              <Route
                exact
                path="/button-groups"
                component={ButtonGroupPage}
              />
              <Route exact path="/dropdowns" component={DropdownPage} />
              <Route exact path="/progress" component={ProgressPage} />
              
              <Route exact path="/input-groups" component={InputGroupPage} />
              <Route exact path="/charts" component={ChartPage} /> */}
            </React.Suspense>
          </MainLayout>
          <Redirect to="/" />

        </Switch>
      </BrowserRouter>

      <NotificationSystem
        dismissible={false}
        ref={notificationSystem =>
          (this.notificationSystem = notificationSystem)
        }
        style={NOTIFICATION_SYSTEM_STYLE}
      />

      </>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
