var express = require('express');
var axios = require('axios');
var router = express.Router();

const camelcase = require('camelcase-keys')
const moment = require('moment-timezone')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()
const eventBridge = new AWS.EventBridge();

const fcmServerKey = process.env.stage == 'dev' 
  ? 'AAAAl4zZDWQ:APA91bFM-ImE3Er_pov80-XmmeVnX9oXe6YS4WjUsd7npagM6LTMqL7Z6uRCud3kGsBcQVuVxQtLMV9R4tLAlm9D511gCtwHTQkeYN878_zVkqYKzG7so4XkXCbIKAQUa_Nd9zw4AUE_'
  : 'AAAASDIjXcY:APA91bHXSJRQvPEL8d-crG9ALJs-33vXWUTm5vZkrs9VP7vNwq07AJT7Z5WnJWl_X3E0JllvlyP1YCGHqW4LDKhLDmQW9MhDZdHTPtlxYmoaGINAKKpnw83QD9Y_0NILSGpkZBvZ6hQU'
const sendMessageEndpoint = 'https://fcm.googleapis.com/fcm/send'

const fcmAxiosOptions = {
  headers: {
    'Authorization': `key=${fcmServerKey}`
  }
}

let androidOptions = {
  priority: 'high'
}

let iosOptions = {
  headers: {
    'apns-priority': 10,
    'content-available': true,
    'mutable-content': true,
  }
}

let fcmTokensPayload = (tokens, notification, data = null) => {

  console.log(tokens, notification, data)
  return {
    registration_ids: tokens,
    data: data,
    notification: notification,
    android: androidOptions,
    apns: iosOptions
  }
}

let fcmTopicPayload = (topic, notification, data = null) => {

  console.log(topic, notification, data)
  return {
    to: `/topics/${topic}`,
    data: data,
    notification: notification,
    android: androidOptions,
    apns: iosOptions
  }
}

let sendFCM = async (payload) => {

  try {
    let url = `${sendMessageEndpoint}`
    let result = await axios.post(url, payload, fcmAxiosOptions)
    console.log(result.data)
  } catch (e) {
    console.error(e)
  }
}

let makeReserve = async (pushId, fireAt) => {

  try {

    let date = moment.tz(fireAt, 'Asia/Seoul')
    let utcDate = date.utc()
    
    let cron = `${utcDate.minutes()} ${utcDate.hours()} ${utcDate.date()} ${utcDate.month()+1} ? ${utcDate.year()}`
    let ruleName = `Push-Process-${pushId}`

    params = {
      Name: ruleName,
      ScheduleExpression: `cron(${cron})`,
      RoleArn: 'arn:aws:iam::862667126176:role/Serverless'
    }

    result = await eventBridge.putRule(params).promise()
    let ruleArn = result.RuleArn

    params = {
      Rule: ruleName,
      Targets: [
        {
          Id: ruleName,
          Arn: `arn:aws:lambda:ap-northeast-2:862667126176:function:api-new-batch-${process.env.stage}-processReservedPush`,
          Input: JSON.stringify({
            body: JSON.stringify({
              pushId: pushId
            })
          })
        }
      ]
    }
    
    await eventBridge.putTargets(params).promise()

    params = {
      Action: 'lambda:InvokeFunction', 
      FunctionName: `api-new-batch-${process.env.stage}-processReservedPush`,
      Principal: 'events.amazonaws.com',
      SourceArn: ruleArn, 
      StatementId: ruleName
    }

    await lambda.addPermission(params).promise()

  } catch (e) {
    console.error(e)
    throw(e)
  }
}

let deleteReserve = async (pushId) => {

  try {

    const ruleName = `Push-Process-${pushId}`

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-processReservedPush`,
      StatementId: ruleName
    }

    await lambda.removePermission(params).promise()

    params = {
      Rule: ruleName,
      Ids: [
        ruleName
      ]
    }

    await eventBridge.removeTargets(params).promise()

    params = {
      Name: ruleName
    }

    console.log(params)

    await eventBridge.deleteRule(params).promise()

  } catch (e) {
    console.error(e)
    throw(e)
  }
}

router.get('/tester', async (req, res) => {

  try {

    let mysql = req.app.get('mysqlR')

    let query
    let result

    let page = req.query.currentPage || 0
    let rowCount = req.query.rowCount || 10

    query = `
      select sql_calc_found_rows
        ui.user_id, ui.user_login_id account_id, ui.user_type_cd type, pt.use_yn
      from 
        oi_user_info ui
        join oi_admin_push_tester pt on ui.user_id = pt.user_id
      order by
        ui.user_login_id asc
      limit 
        ${page * rowCount}, ${rowCount};
    `
    result = await mysql.execute(query)
    let testers = camelcase(result, {deep: true})

    query = `
      select found_rows() count
    `
    let countResult = await mysql.execute(query)
    let count = countResult[0].count

    res.send(JSON.stringify({
      testers: testers,
      totalCount: count
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/tester', async (req, res) => {

  try {

    console.log('userId', req.headers.userId)

    let userId = req.headers.userId
    let accountId = req.body.accountId

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      select
        user_id
      from 
        oi_user_info
      where
        user_login_id = '${accountId}'
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      res.send(JSON.stringify({message:'사용자를 찾을 수 없습니다.'}))
      return
    }

    let targetUserId = result[0].user_id

    query = `
      insert ignore into oi_admin_push_tester (
        user_id, admin_id
      ) values (
        ${targetUserId}, ${userId}
      )
    `
    result = await mysql.execute(query)

    console.log(result)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/tester', async (req, res) => {

  try {

    let userId = req.headers.userId
    let targetUserIds = req.body.userIds

    if (targetUserIds.length == 0) {
      res.send('{}')
      return
    }

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      delete from
        oi_admin_push_tester
      where
        user_id in (${targetUserIds.join(',')})
    `
    await mysql.execute(query)
    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/tester/use', async (req, res) => {

  try {

    let userId = req.headers.userId
    let targetUserIds = req.body.userIds

    if (targetUserIds.length == 0) {
      res.send('{}')
      return
    }

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      update
        oi_admin_push_tester
      set 
        use_yn = 'Y'
      where
        user_id in (${targetUserIds.join(',')})
    `
    await mysql.execute(query)
    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/tester/unuse', async (req, res) => {

  try {

    let userId = req.headers.userId
    let targetUserIds = req.body.userIds

    if (targetUserIds.length == 0) {
      res.send('{}')
      return
    }

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      update
        oi_admin_push_tester
      set 
        use_yn = 'N'
      where
        user_id in (${targetUserIds.join(',')})
    `
    await mysql.execute(query)
    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/message', async (req, res) => {

  try {

    let mysql = req.app.get('mysqlR')

    let query
    let result

    let page = req.query.currentPage || 0
    let rowCount = req.query.rowCount || 10

    query = `
      select sql_calc_found_rows
        push_id, title, body, target_type_cd target_type, target_id, target_url, reg_dt created_at, fire_dt fire_at, fire_type
      from 
        oi_admin_push
      order by
        push_id desc
      limit 
        ${page * rowCount}, ${rowCount};
    `
    result = await mysql.execute(query)

    let messageIds = []
    let messageIdMap = {}

    result.forEach(row => {
      messageIds.push(row.push_id)
      row.history = []
      messageIdMap[row.push_id] = row
      return row
    })

    query = `
      select found_rows() count
    `
    let countResult = await mysql.execute(query)
    let count = countResult[0].count

    query = `
      select
        *
      from
        oi_admin_push_send
      where
        push_id in (${messageIds.join(',')})
      order by
        fire_dt desc;
    `
    result = await mysql.execute(query)

    for (let row of result) {
      let pushId = row.push_id
      if (messageIdMap[pushId] != null) {
        messageIdMap[pushId].history.push(row)
      }
    }

    let messages = Object.values(messageIdMap)

    messages = messages.sort( (lhs, rhs) => {
      return lhs.push_id < rhs.push_id ? 1 : lhs.push_id > rhs.push_id ? -1 : 0
    })

    for (let message of messages) {
      message.last_target_count = message.history.length > 0 ? message.history[0].target_count : 0;
    }

    messages = camelcase(messages, {deep: true})

    res.send(JSON.stringify({
      messages: messages,
      totalCount: count
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/message', async (req, res) => {

  try {

    let userId = req.headers.userId

    let mysql = req.app.get('mysqlW')

    let query
    let result

    let title = req.body.title
    let body = req.body.body
    let landingType = req.body.landingType
    let landingTarget = req.body.landingTarget
    let targetType
    let targetId
    let targetUrl
    switch (landingType) {
      case 1:
        targetType = `'FEED'`
        targetId = parseInt(landingTarget)
        targetUrl = 'null'
        break;
      case 2:
        targetType = `'USER'`
        targetId = parseInt(landingTarget)
        targetUrl = 'null'
        break
      case 3:
        targetType = `'WEB'`
        targetId = 'null'
        targetUrl = landingTarget 
        break
    
      default:
        targetType = 'null'
        targetId = 'null'
        targetUrl = 'null'
        break;
    }

    query = `
      insert into oi_admin_push (
        title, body, target_type_cd, target_id, target_url, admin_user_id
      ) values (
        '${title}', '${body}', ${targetType}, ${targetId}, '${targetUrl}', ${userId}
      )
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/message', async (req, res) => {

  try {

    let userId = req.headers.userId

    let pushIds = req.body.pushIds

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      delete from
        oi_admin_push
      where
        push_id in (${pushIds.join(',')});
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/message/:pushId/to-tester', async (req, res) => {

  console.log(process.env.stage)

  try {

    let userId = req.headers.userId
    let pushId = req.params.pushId

    console.log(userId, pushId)

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      select 
        * 
      from 
        oi_admin_push
      where 
        push_id = ${pushId}
    `
    result = await mysql.execute(query)

    let pushInfo = result[0]

    query = `
      select 
        push_auth_key
      from 
        oi_admin_push_tester pt
        join oi_user_device_info udi on pt.user_id = udi.user_id
      where
        udi.push_auth_key is not null
        and udi.login_yn = 'Y';
    `
    result = await mysql.execute(query)

    let tokens = result.map(row => {
      return row.push_auth_key
    })

    let notification = {
      title: pushInfo.title,
      body: pushInfo.body
    }

    let data = {
      notificationType: 'EVENT',
      title: pushInfo.title,
      body: pushInfo.body,
      targetType: pushInfo.target_type_cd,
      targetId: pushInfo.target_id,
      targetUrl: pushInfo.target_url
    }

    await sendFCM(fcmTokensPayload(tokens, notification, data))

    query = `
      insert into oi_admin_push_send (
        push_id, fire_dt, fire_type, target_count
      ) values (
        ${pushId}, now(), 'TEST', ${tokens.length}
      );
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/message/:pushId/to-whole', async (req, res) => {

  try {

    let userId = req.headers.userId
    let pushId = req.params.pushId

    console.log(userId, pushId)

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      select 
        * 
      from 
        oi_admin_push
      where 
        push_id = ${pushId}
    `
    result = await mysql.execute(query)

    let pushInfo = result[0]

    let notification = {
      title: pushInfo.title,
      body: pushInfo.body
    }

    let data = {
      type: 'Push',
      payload: {
        type: 'Event',
        title: notification.title,
        body: notification.body,
        userId: userId,
        accountId: '',
        nickname: '',
        targetType: pushInfo.target_type_cd,
        targetId: pushInfo.target_id,
        targetUrl: pushInfo.target_url
      }
    }
    
    await sendFCM(fcmTopicPayload('notification-ios', notification, data))
    await sendFCM(fcmTopicPayload('notification-android', notification, data))

    query = `
      select 
        count(*) count
      from
        oi_user_device_info udi
        join oi_user_info ui on udi.user_id = ui.user_id
      where
        udi.login_yn = 'Y'
        and ui.user_status_cd = 'ACTIVE';
    `
    result = await mysql.execute(query)
    let count = result[0].count

    query = `
      insert into oi_admin_push_send (
        push_id, fire_dt, fire_type, target_count
      ) values (
        ${pushId}, now(), 'WHOLE', ${count}
      );
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/message/:pushId/reserve', async (req, res) => {

  console.log(req.body)

  try {

    let userId = req.headers.userId
    let pushId = req.params.pushId

    let fireType = req.body.fireType
    let fireDate = req.body.fireDate
    let fireTime = req.body.fireTime

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      select
        fire_dt
      from
        oi_admin_push
      where
        push_id = ${pushId};
    `
    result = await mysql.execute(query)
    if (result[0].fire_dt != null) {
      await deleteReserve(pushId)
    }

    query = `
      update 
        oi_admin_push
      set
        fire_dt = '${fireDate} ${fireTime}',
        fire_type = '${fireType}'
      where
        push_id = ${pushId}
    `
    await mysql.execute(query)

    query = `
      select
        push_id, fire_dt fire_at, fire_type
      from
        oi_admin_push
      where
        push_id = ${pushId};
    `
    result = await mysql.execute(query)

    let fireInfo = camelcase(result[0], {deep: true})

    await makeReserve(pushId, `${fireDate} ${fireTime}`)

    res.send(JSON.stringify(fireInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/message/:pushId/reserve', async (req, res) => {

  console.log(req.body)

  try {

    let userId = req.headers.userId
    let pushId = req.params.pushId

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      select
        fire_dt
      from
        oi_admin_push
      where
        push_id = ${pushId};
    `
    result = await mysql.execute(query)
    if (result[0].fire_dt != null) {
      
      await deleteReserve(pushId)

      query = `
        update
          oi_admin_push
        set
          fire_dt = null, fire_type = null
        where
          push_id = ${pushId}
      `
      await mysql.execute(query)
    }

    res.send(JSON.stringify({pushId: pushId}))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


module.exports = router