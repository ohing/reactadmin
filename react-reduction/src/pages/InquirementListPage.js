import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class InquirementListPage extends React.Component {

  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  currentInquirement = null

  state = {
    requestType: '0',
    status: '0',

    currentPage: 0,
    inquirements: [],

    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,

    imageModal: false,
  };

  componentDidMount() {
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  } 
  
  getData = async (props) => {

    try {

      let result = await api.inquirement().call({apiName: 'getInquirements', method: 'get', parameters: {
        requestType: this.state.requestType,
        status: this.state.status,
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let inquirements = result.inquirements

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        inquirements: inquirements,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleOption = event => {
    this.setState({
      requestType: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleStatus = event => {
    this.setState({
      status: event.target.value
    }, () => {
      this.getData()
    })
  }
  
  handleId = event => {
    event.preventDefault()

    let value = event.target.getAttribute('value')
    let inquirementId = parseInt(value.split(':')[1])
    this.props.history.push(`/inquirement/${inquirementId}`)
  }

  toggleContentModal = modalType => () => {
    if (!modalType) {
      this.setState({
        contentModal: !this.state.contentModal,
      })
    }
  }

  toggleImageModal = modalType => () => {
    if (!modalType) {
      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  handleContent = event => {
    event.preventDefault()
    let value = event.target.getAttribute('value')

    let inquirementId = parseInt(value.split(':')[1])
    let inquirement = this.inquirement(inquirementId)
    this.currentInquirement = inquirement

    this.toggleContentModal()()
  }

  handleTarget = event => {
    event.preventDefault()
    let value = event.target.getAttribute('value')
    let type = value.split(':')[0]
    let id = parseInt(value.split(':')[1])
    console.log(type, id)

    switch (type) {
      case 'user': {
        this.props.history.push(`/user/${id}`)
        break 
      }
      case 'feed': {
        this.props.history.push(`/feed/${id}`)
        break 
      }
      case 'comment': {
        break 
      }
      case 'chatroom': {
        break 
      }
      case 'chatnotice': {
        break 
      }
      case 'chatcomment': {
        break 
      }
    }
  }

  inquirement = (inquirementId) => {
    return this.state.inquirements.filter(inquirement => {
      return inquirement.askId == inquirementId
    })[0]
  }

  handleComplete = event => {
    let reportId = parseInt(event.target.id.split(':')[1])
    let report = this.report(reportId)

    this.complete(reportId, report.statusCd == 'NEW')
  }
  
  complete = async (reportId, complete) => {

    try {

      await api.report().call({apiName: complete ? 'completeReport' : 'cancelCompleteReport', method: 'post', pathParameter: reportId})

      this.getData()
      this.props.addNotification(complete ? '처리가 완료되었습니다' : '처리가 취소되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  render() {

    console.log('render')

    return (
      <Page
        className="InquirementListPage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>문의 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>

                          <Input className="ml-3" type="select" value={this.state.isUsing} onChange={this.handleOption}>
                            <option value="0">카테고리 전체</option>
                            <option value="ASK001">가입/탈퇴</option>
                            <option value="ASK002">게시물작성</option>
                            <option value="ASK003">메신저</option>
                            <option value="ASK004">도와줘</option>
                          </Input>

                          <Input className="ml-3" type="select" value={this.state.isOnGoing} onChange={this.handleStatus}>
                            <option value="0">처리상태 전체</option>
                            <option value="ASK">대기중</option>
                            <option value="VIEW">처리중</option>
                            <option value="ANSWER">처리완료</option>
                          </Input>
                          
                        </div>

                        {/* <div>
                          <Input
                            type="text"
                            name="text"
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                            onChange={this.handleSearchText}
                          />
                          <Button outline color="secondary" value={this.state.searchText} onClick={this.handleSearch}>검색</Button>
                        </div> */}
                      </Form>

                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="80">신고번호</th>
                            <th width="80">신고일자</th>
                            <th width="100">처리상태</th>
                            <th width="100">문의유형</th>
                            <th width="180">문의내용</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.inquirements.map( (inquirement, index) => {

                              return (
                                <>
                                  <tr>
                                    <td className="align-middle"><a href="#" value={`inquirement:${inquirement.askId}`} onClick={this.handleId}>{inquirement.askId}</a></td>
                                    <td className="align-middle">{inquirement.askDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                    <td className="align-middle">{inquirement.askStatus}</td>
                                    <td className="align-middle">{inquirement.askType}</td>
                                    <td className="text-truncate" style={{maxWidth:'340px'}}><a href="#" value={`inquirement:${inquirement.askId}`} onClick={this.handleContent}>{inquirement.content}</a></td>
                                  </tr>
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.contentModal}
          toggle={this.toggleContentModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleContentModal()}>{'문의 내용'}</ModalHeader>
          <ModalBody>            
            <Label>{this.currentInquirement == null ? '' : this.currentInquirement.content}</Label>
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>{'배너 이미지'}</ModalHeader>
          <ModalBody>            
            <CardImg
              className="card-img ml-3 mb-2"
              src={(this.state.presentedBanner || {}).imageUrl}
              // key={media.mediaSeq}
              style={{ width: 435, height: 580, objectFit: 'cover', objectPosition: 'center'}}
            />
          </ModalBody>
        </Modal>

      </Page>
    ); 
  }
};

export default InquirementListPage;
