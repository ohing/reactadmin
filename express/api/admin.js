var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')


router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10
  let type = req.query.type || 0
  let searchText = (req.query.searchText || '').trim()

  let searchCondition = searchText.length == 0 ? '' : `
    and (
      user_login_id like '%${searchText}%'
      or user_real_nm like '%${searchText}%'
      or user_email like '%${searchText}%'
    )
  `

  let mysql = req.app.get('mysqlR')

  let query
  let result

  try {

    query = `
      select sql_calc_found_rows
        user_id, user_type_cd type, user_login_id account_id, user_real_nm name, user_email email
      from 
        oi_user_info
      where
        user_type_cd in (${type == 0 ? `'SUPER', 'OHING', 'TESTER'` : type == 1 ? `'SUPER'` : type == 2 ? `'OHING'` : `'TESTER`})
        and user_status_cd != 'WITHDRAW'
        and user_status_cd != 'WDSTANDBY'
        ${searchCondition}
      order by
        field(user_type_cd, 'OHING', 'SUPER') DESC,
        user_real_nm asc
      limit ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    let admins = camelcase(result, {deep:true})

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    res.send(JSON.stringify({
      admins: admins,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/', async (req, res) => {

  let userId = req.headers.userId

  let accountId = req.body.accountId
  let type = req.body.type
  let name = req.body.name
  let email = req.body.email
  let phoneNumber = req.body.phoneNumber
  let regionCode = '100260'
  let sex = req.body.sex
  let birthY = req.body.birthY
  let birthMD = req.body.birthMD
  let tag = '오잉'
  let ci = 'ohing_ci_value'

  let query 
  let result

  let mysql = req.app.get('mysqlW')

  try {

    query = `
      insert into oi_user_info (
        user_type_cd, user_login_id, user_pwd, user_real_nm, user_nick_nm, user_email, user_hp_no, region_cd, 
        sex, birth_y, birth_md, profile_open_type_cd, ci, di, user_status_cd, reg_dt, reg_user_id
      ) values (
        '${type}', '${accountId}', '831c237928e6212bedaa4451a514ace3174562f6761f6a157a2fe5082b36e2fb', '${name}', '${name}', '${email}', '${phoneNumber}', '${regionCode}', '${sex}',
        '${birthY}', '${birthMD}', 'CLOSE', '${ci}', '${ci}', 'ACTIVE', now(), ${userId}
      );
    `
    result = await mysql.execute(query)
    let targetUserId = result.insertId

    let tag = '오잉'

    query = `
      insert into oi_tag_info (
        tag_val, tag_cnt
      ) values (
        '${tag}', 1
      ) on duplicate key update 
        tag_cnt = values(tag_cnt) + 1;
    `
    result = await mysql.execute(query)
    let tagId = result.insertId

    query = `
      insert into oi_user_tag (
        user_id, tag_id, reg_dt, tag_seq
      ) values (
        ${targetUserId}, ${tagId}, now(), 1
      );
    `
    await mysql.execute(query)

    query = `
      insert into oi_notification_setting (
        user_id
      ) values (
        ${targetUserId}
      );
    `
    await mysql.execute(query)
    res.send('{}')

  } catch (e) {
    console.error(e)
    res.send('{"message": "실패하였습니다. 중복된 아이디인지 확인해 주세요."}')
  }
})

router.delete('/', async (req, res) => {

  let userIds = req.body.userIds || []
  console.log(userIds)

  if (userIds.length > 0) {

    let mysql = req.app.get('mysqlW')

    let query
    let result

    query = `
      update
        oi_user_info
      set
        user_status_cd = 'WITHDRAW'
      where
        user_id in (${userIds.join(',')});
    ` 
    console.log(query)

    try {
      await mysql.execute(query)
    } catch(e) {
      console.error(e)
    }
  }

  res.send('{}')
})

router.post('/check-id', async (req, res) => {

  let accountId = req.body.accountId
  let mysql = req.app.get('mysqlR')    

  let query
  let result

  query = `
    select
      user_id
    from
      oi_user_info
    where
      user_login_id = '${accountId}'
    limit
      1;
  ` 
  console.log(query)

  try {

    result = await mysql.execute(query)
    res.send(`{"exists":${result.length > 0}}`)

  } catch(e) {
    console.error(e)
    res.send(`{"exists":false}`)
  }
})

module.exports = router