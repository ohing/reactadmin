import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class BannerEditPage extends React.Component {

  state = {

    partners: [],

    bannerId: null,

    position: '0',
    title: '',
    partnerId: '0',
    targetType: '0',
    targetId: '',
    targetUrl: '',
    sortOrder: '',
    beginDate: null,
    beginTime: null,
    endDate: null,
    endTime: null,
    openImmediately: '0',
    imageUrl: '',
    filePath: null,

    editModal: false
  };

  currentFile = null

  componentDidMount() {

    let banner = this.props.location.state.banner

    if (banner != null) {

      let position = {
        '도와줘 상단': '1',
        '랭킹 상단': '2',
        '채팅 상단': '3',
        '마이피드 상단': '4',
        '컨텐츠 상단': '5'
      }[banner.position]

      let beginDateString = banner.bannerBeginDt
      let endDateString = banner.bannerEndDt

      let begins = beginDateString.split('T')
      let beginDate = begins[0]
      let beginTime = begins[1].substring(0, 8)

      let ends = endDateString.split('T')
      let endDate = ends[0]
      let endTime = ends[1].substring(0, 8)

      this.setState({
        bannerId: banner.bannerId,
        position: position,
        title: banner.bannerTitle,
        partnerId: banner.partnerId || '0',
        targetType: banner.targetType || '0',
        targetId: banner.targetId || '',
        targetUrl: banner.targetUrl || '',
        sortOrder: `${banner.bannerSortNo}`,
        beginDate: beginDate,
        beginTime: beginTime,
        endDate: endDate,
        endTime: endTime,
        openImmediately: banner.openYn == 'Y' ? '1' : '0',
        imageUrl: banner.imageUrl
      })
    }

    // if (notice != null) {
    //   this.setState({
    //     notice: notice,
    //     category: category,
    //     title: notice.title
    //   }, () => {
    //     this.getHTML()
    //   })
    // }

    this.getData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }
  
  getData = async (props) => {

    try {

      let result = await api.partner().call({apiName: 'getPartners', method: 'get', parameters: {
        needsAll: 1
      }})

      let partners = result.partners

      this.setState({
        partners: partners
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  handlePosition = event => {
    this.setState({
      position: event.target.value
    })
  }

  handleTitle = event => {
    this.setState({
      title: event.target.value
    })
  }

  handleTargetType = event => {

    let value = event.target.value

    let nextState = {
      targetType: value
    }

    if (!(value == '3' || value == '4' || value == '7')) {
      nextState.targetUrl = ''
    }
    if (!(value == '1' || value == '2' || value == '8')) {
      nextState.targetId = ''
    }    

    console.log(nextState)

    this.setState(nextState)
  }

  handleTargetId = event => {
    this.setState({
      targetId: event.target.value
    })
  }

  handleTargetUrl = event => {
    this.setState({
      targetUrl: event.target.value
    })
  }

  handleSortOrder = event => {
    this.setState({
      sortOrder: event.target.value
    })
  }

  handleBeginDate = event => {
    this.setState({
      beginDate: event.target.value
    })
  }

  handleBeginTime = event => {
    this.setState({
      beginTime: event.target.value
    })
  }

  handleEndDate = event => {
    this.setState({
      endDate: event.target.value
    })
  }

  handleEndTime = event => {
    this.setState({
      endTime: event.target.value
    })
  }

  handleOpen = event => {
    this.setState({
      openImmediately: event.target.value
    })
  }

  handleFile = event => {
    if (event.target.files.length > 0) {
      let file = event.target.files[0]
      this.currentFile = file
      let fileReader = new FileReader()
      fileReader.onload = event => {
        this.setState({
          filePath: event.target.result
        }) 
      }
      fileReader.readAsDataURL(file)
    }
  }

  handleCreateButton = () => {

    let position = this.state.position
    if (position == '0') {
      this.props.addNotification('노출위치를 선택해 주세요.')
      return
    }

    let title = this.state.title.trim()
    if (title.length == 0) {
      this.props.addNotification('제목을 입력해 주세요.')
      return
    }

    switch (this.state.targetType) {
      case '1':
      case '2':
      case '8': {

        let targetId = parseInt(this.state.targetId)

        if (this.state.targetType == '8') {
          if (!(Number.isInteger(targetId) && targetId > -1 && targetId < 5)) {
            this.props.addNotification('타겟 아이디를 0~4 범위에서 입력해 주세요.')
            return
          }
        } else {
          if (!(Number.isInteger(targetId) && targetId > 0)) {
            this.props.addNotification('자연수 형태의 타겟 아이디를 입력해 주세요.')
            return
          }
        }
        break
      }

      case '3':
      case '4':
      case '7': {

        let targetUrl = this.state.targetUrl.trim()
        if (targetUrl.length == 0) {
          this.props.addNotification('타겟 URL을 입력해 주세요.')
          return
        }

        try {
          let url = new URL(targetUrl)
          if (!(url.protocol === 'http:' || url.protocol === 'https:')) {
            this.props.addNotification('올바른 형태의 URL을 입력해 주세요.')
          }
        } catch (e) {
          this.props.addNotification('올바른 형태의 URL을 입력해 주세요.')
          return
        }

        break
      }

      default: {
        break
      }
    }

    let sortOrder = (this.state.sortOrder || '').trim()
    if (sortOrder.length > 0 && !Number.isInteger(parseInt(sortOrder))) {
      this.props.addNotification('정수 형태의 정렬 순서를 입력해 주세요.')
      return
    }

    if (this.state.beginDate == null || this.state.beginTime == null) {
      this.props.addNotification('시작시간을 입력해 주세요.')
      return
    }

    if (this.state.endDate == null || this.state.endTime == null) {
      this.props.addNotification('종료시간을 입력해 주세요.')
      return
    }

    if (this.currentFile == null && this.state.imageUrl == null) {
      this.props.addNotification('배너 이미지를 선택해 주세요.')
      return
    }

    this.toggleCreateModal()()
  } 

  toggleCreateModal = modalType => () => {
    if (!modalType) {
      this.setState({
        createModal: !this.state.createModal,
      })
    }
  }

  handleCreate = async event => {

    this.setState({
      createModal: false
    })

    try {

      let bannerId = this.state.bannerId

      let beginDate = this.state.beginDate
      let beginTime = this.state.beginTime

      let endDate = this.state.endDate
      let endTime = this.state.endTime

      let beginDateString = `${beginDate} ${beginTime}`
      let endDateString = `${endDate} ${endTime}`

      let parameters = {
        position: this.state.position,
        title: this.state.title.trim(),
        partnerId: this.state.partnerId,
        openImmediately: this.state.openImmediately,
        beginTime: beginDateString,
        endTime: endDateString,
      }

      if (this.currentFile != null) {
        parameters.image = this.currentFile
      } else {
        parameters.imageUrl = this.state.imageUrl
      }

      let targetType = this.state.targetType
      if (targetType != '0') {
        parameters.targetType = targetType
        switch (targetType) {
          case '1':
          case '2':
          case '8': {
            parameters.targetId = parseInt(this.state.targetId)
            break
          }

          case '3':
          case '4':
          case '7': {
            parameters.targetUrl = this.state.targetUrl.trim()
            break
          }

          default:
            break
        }
      }

      let sortOrder = this.state.sortOrder.trim()
      if (sortOrder.length > 0) {
        parameters.sortOrder = sortOrder
      }

      console.log(parameters)

      let result = await api.banner().call({apiName: 'modifyBanner', method: 'put', pathParameter: bannerId, parameters: parameters, isMultipart: true})

      this.props.addNotification('배너 정보가 수정되었습니다.')
      this.props.history.replace('/ad/banner')

    } catch(e) {
      console.error(e)
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="BannerCreatePage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>배너 편집</CardHeader>
              <CardBody>

                <Row>
                  <Label className="mx-3 mb-1">노출위치*</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.position} onChange={this.handlePosition}>
                    <option value="0">노출위치 선택</option>
                    <option value="1">도와줘 상단</option>
                    <option value="2">랭킹 상단</option>
                    <option value="3">채팅 상단</option>
                    <option value="4">마이피드 상단</option>
                    <option value="5">컨텐츠 상단</option>
                  </Input>
                  <Label className="mx-3 mb-1">제목* (제목은 유저에게 노출되지 않지만, Firebase에 집계됩니다.)</Label>
                  <Input className="mx-3 mb-3" type="text" placeholder="제목 입력" onChange={this.handleTitle} value={this.state.title}/>

                  <Label className="mx-3 mb-1">파트너* (향후 광고 파트너가 생길 경우 선택 가능하도록 개발 예정입니다.)</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.partnerId} onChange={this.handlePartner}>
                    <option value="0">인하우스</option>
                  </Input>
                  <Label className="mx-3 mb-1">타겟 대상</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.targetType} onChange={this.handleTargetType}>
                    <option value="0">이동안함</option>
                    <option value="1">글 상세</option>
                    <option value="2">프로필</option>
                    <option value="3">내부 브라우저</option>
                    <option value="4">외부 브라우저</option>
                    <option value="5">글작성</option>
                    <option value="6">도와줘작성</option>
                    <option value="7">공지사항</option>
                    <option value="8">탭</option>
                  </Input>

                  <Label className="mx-3 mb-1">{this.state.targetType == '1' || this.state.targetType == '2' || this.state.targetType == '8' ? '타겟 아이디*' : '타겟 아이디'}</Label>
                  {
                    this.state.targetType == '1' || this.state.targetType == '2' || this.state.targetType == '8' ? 
                    <Input className="mx-3 mb-3" type="text" placeholder={this.state.targetType == '1' ? '피드 아이디 입력' : this.state.targetType == '2' ? '유저 아이디 입력' : '가장 왼쪽 도와줘 탭부터 0~4 입력'} onChange={this.handleTargetId} value={this.state.targetId}/> :
                    <Input className="mx-3 mb-3" disabled type="text" placeholder="아이디 입력" onChange={this.handleTargetId} value={this.state.targetId}/>
                  }

                  <Label className="mx-3 mb-1">{this.state.targetType == '3' || this.state.targetType == '4' || this.state.targetType == '7' ? '타겟 URL*' : '타겟 URL'}</Label>
                  {
                    this.state.targetType == '3' || this.state.targetType == '4' || this.state.targetType == '7' ? 
                    <Input className="mx-3 mb-3" type="text" placeholder="url 입력" onChange={this.handleTargetUrl} value={this.state.targetUrl}/> :
                    <Input className="mx-3 mb-3" disabled type="text" placeholder="url 입력" onChange={this.handleTargetUrl} value={this.state.targetUrl}/>
                  }

                  <Label className="mx-3 mb-1">정렬 순서(미입력 시 같은 위치의 가장 마지막으로 지정됩니다.)</Label>
                  <Input className="mx-3 mb-3" type="text" placeholder="숫자 입력" onChange={this.handleSortOrder} value={this.state.sortOrder}/>

                  <Label className="mx-3 mb-1">시작시간*</Label>
                  <Input type="date" className="mx-3" name="beginDate" value={this.state.beginDate} onChange={this.handleBeginDate}/>
                  <Input type="time" className="mx-3 mt-1 mb-3" name="beginTime" value={this.state.beginTime} onChange={this.handleBeginTime}/>

                  <Label className="mx-3 mb-1">종료시간*</Label>
                  <Input type="date" className="mx-3" name="endDate" value={this.state.endDate} onChange={this.handleEndDate}/>
                  <Input type="time" className="mx-3 mt-1 mb-3" name="endTime" value={this.state.endTime} onChange={this.handleEndTime}/>

                  <Label className="mx-3 mb-1">오픈여부*</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.openImmediately} onChange={this.handleOpen}>
                    <option value="0">클로즈</option>
                    <option value="1">오픈</option>
                  </Input>

                  <Label className="mx-3 mb-1">배너 이미지* ({
                      this.state.position == '1' ? '5:2' : '5:1'
                    })
                  </Label>
                  <Input className="ml-3 mb-2" type="file" accept="image/png,image/jpeg,image/jpg" onChange={this.handleFile}/>
                  <CardImg
                    className="card-img ml-3 mb-2"
                    src={this.state.filePath != null ? this.state.filePath : this.state.imageUrl}
                    // key={media.mediaSeq}
                    style={{ width: 800, height: this.state.position == '1' ? 320 : 160, objectFit: 'cover', objectPosition: 'center'}}
                  />

                </Row>

                <Button className="mt-3" color="secondary" onClick={this.handleCreateButton}>작성완료</Button>

              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.createModal}
          toggle={this.toggleCreateModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleCreateModal()}>{'배너 편집'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">입력된 정보로 배너를 수정합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleCreateModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleCreate}>
              수정
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default BannerEditPage;
