#!/bin/sh

while true; do
  read -p "Are you sure you want to publish new version of 'dev'? (yes or no) " yn
  case $yn in 
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

echo 'Build & Minifying...'
yarn build

echo 'Commit & Pushing...'
git add .
git commit -m "Publish new version via automatically."
git push origin master

ssh -i "~/Downloads/ohing-public.pem" bitnami@3.34.243.167

echo 'Completed!'