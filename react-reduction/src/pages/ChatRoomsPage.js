import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class ChatRoomsPage extends React.Component {

  state = {
    currentPage: 0,
    rooms: [],
    rowCount: 10,
    status: 0,
    type: 0,
    anonymous: 0,
    reportedType: 0,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    searchText: '',
    selectedIds: new Set(),

    imageModal: false
  };

  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {

    this.state.rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 
    console.log('DidMount')
    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let result = await api.chat().call({apiName: 'getOpenRooms', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        status: this.state.status,
        rowCount: this.state.rowCount, 
        searchText: this.state.searchText
      }})

      let totalCount = result.totalCount
      let rooms = result.rooms

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        rooms: rooms,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })

    } catch (e) {

      console.log(e)
    }
  }

  handleStatus = event => {

    let status = parseInt(event.target.value)
    console.log('status', status)

    this.state.status = status
    this.state.currentPage = 0
    this.getData()
  }

  handleSearchText = event => {
    let searchText = event.target.value
    this.setState({
      searchText: searchText
    })
  }

  handleKeyDown = event => {
    if (event.key == 'Enter') {
      event.preventDefault();
      this.handleSearch(event)
    }
  }

  handleSearch = event => {
    this.getData()
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleRoom = event => {
    event.preventDefault();
    let roomId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/chat/room/${roomId}`) 
  }

  handleMaster = event => {
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/user/${targetUserId}`) 
  }

  handleMedia = event => {

    let mediaUrl = event.target.getAttribute('src') 

    let url = new URL(mediaUrl)
    let paths = url.pathname.split('/')
    let fileName = paths[paths.length-1]
    let pureName = fileName.split('.')[0]
    mediaUrl = `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`

    this.setState({
      currentMediaUrl: mediaUrl
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  handleTag = event => {
    event.preventDefault()
    let tag = event.target.innerText.trim()
    tag = tag.substring(1, tag.length)
    console.log(tag)

    this.props.history.push(`/keyword/tag/${tag}`)
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="ChatRoomsPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>오픈채팅방 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>
                          <Input type="select" value={this.state.status} onChange={this.handleStatus}>
                            <option value="0">상태</option>
                            <option value="1">정상</option>
                            <option value="2">숨김</option>
                          </Input>
                        </div>

                        <div>
                          <Input
                            type="text"
                            name="text"
                            value={this.state.searchText}
                            onChange={this.handleSearchText}
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                          />
                          <Button outline color="secondary" onClick={this.handleSearch}>검색</Button>
                        </div>
                      </Form>
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="100"></th>
                            <th width="62">제목</th>
                            <th width="60">방장</th>
                            <th width="40">신고횟수</th>
                            <th width="80">개설일</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.rooms.map( (room, index) => {

                              let tags = room.tags || []

                              let fileName = room.coverFileName
                              let mediaUrl = null

                              if (fileName != null) {
                                let pureName = fileName.split('.')[0]
                                mediaUrl = `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`
                              }

                              return (
                                <>
                                  <tr style={mediaUrl != null ? {borderBottom: 'none'} : {}}>
                                    <td>
                                    {
                                      mediaUrl == null ? '' :
                                      (
                                        <>
                                          <CardImg
                                            className="card-img ml-1 can-click"
                                            src={mediaUrl}
                                            value={room.roomId}
                                            style={{ width: 30, height: 30, objectFit: 'cover', objectPosition: 'center'}}
                                            onClick={this.handleMedia}/>
                                        </>
                                      )
                                    }
                                    </td>
                                    <td><a href="#" onClick={this.handleRoom} value={room.roomId}>{room.title}</a></td>
                                    <td><a href="#" onClick={this.handleMaster} value={room.masterUserId}>{room.masterNickname}</a></td>
                                    <td><a href="#" onClick={this.handleReport} value={room.roomId}>{room.reportCount}</a></td>
                                    <td>{room.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                  </tr>
                                  {
                                    tags.length == 0 ? '' :
                                      (
                                        <tr>
                                          <td colSpan="6" style={{border: 'none'}}> 
                                          <>
                                          {
                                            tags.map( (tag, index) => {
                                              return <a href="#" onClick={this.handleTag}>{` #${tag} `}</a> 
                                            })
                                          }
                                          </>
                                          </td>
                                        </tr>
                                      )
                                  }
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>미디어 자세히 보기</ModalHeader>
          <ModalBody>
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default ChatRoomsPage;
