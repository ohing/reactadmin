import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  FormFeedback,
  Label,
  Input,
  Button,
  Table
} from 'reactstrap';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class AddAdminPage extends React.Component {

  state = {
    accountId: '',
    exists: false,
    type: '0',
    name: '',
    email: 'example@ohing.net',
    phoneNumber: '01012341234',
    sex: '0',
    birthday: '2005-05-02', 
    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
  };

  timerId = null

  componentDidMount() {
    console.log('DidMount')
  }

  handleTypeOption = event => { 
    this.setState({ type: event.target.value })
  }
  handleAccountId = event => {

    if (this.timerId != null) {
      clearInterval(this.timerId)
    }

    this.timerId = setTimeout(() => {
      this.checkAccountId()
    }, 600)

    this.setState({ accountId: event.target.value })
  }
  checkAccountId = async () => {
    
    this.timerId = null

    let accountId = this.state.accountId.trim()
    if (accountId.length == 0) {
      return
    }

    try {
      let result = await api.admin().call({apiName: 'checkAccountId', method: 'post', parameters: {accountId: accountId}})
      this.setState({
        exists: result.exists == true
      })
    } catch (e) {
      console.error(e)
    }
  }
  handleAccountIdFocusOut = event => {
    console.log('focus out')
  }
  handleName = event => {
    this.setState({ name: event.target.value })
  }
  handleEmail = event => {
    this.setState({ email: event.target.value })
  }
  handlePhone = event => {
    this.setState({ phoneNumber: event.target.value })
  }
  handleSex = event => {
    this.setState({ sex: event.target.value })
  }
  handleBirthday = event => {
    this.setState({ birthday: event.target.value })
  }

  onlyNumber = event => {
    if (![0,1,2,3,4,5,6,7,8,9].includes(parseInt(event.key))) { event.preventDefault(); }
  }
 
  handleAdd = async () => {

    let parameters = JSON.parse(JSON.stringify(this.state))
    
    delete parameters.userInfo
    delete parameters.exists

    for (let key of Object.keys(parameters)) {
      let value = parameters[key]
      if (value.length == 0) {
        this.props.addNotification('필수(*) 값을 모두 입력해 주세요.')
        return
      }
    }

    console.log(this.state)

    if (this.state.userInfo.type === 'SUPER') {
      parameters.type = parameters.type === "0" ? "OHING" : "TESTER"
    } else {
      parameters.type = "TESTER"
    }

    parameters.sex = parameters.sex === "0" ? "F" : "M"

    let birthStrings = parameters.birthday.split('-')

    parameters.birthY = birthStrings[0]
    parameters.birthMD = `${birthStrings[1]}${birthStrings[2]}`

    delete parameters.birthday

    console.log(parameters)
    
    try {
      
      let result = await api.admin().call({apiName: 'createAdmin', method: 'put', parameters: parameters})

      if (result.message != null) {
        this.props.addNotification(result.message)
      } else {
        this.props.addNotification('추가되었습니다.')
        this.props.history.replace('/admin')
      }

    } catch (e) {
      console.error(e)
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AddAdminPage"
      >
        <Row key='B'>
          <Col>
            <Card className="mb-3">
              <CardHeader>관리자 추가</CardHeader>
              <CardBody>
                <Form>
                  <FormGroup row>
                    <Label for="Type" sm={2}>
                      타입*
                    </Label>
                    <Col sm={10}>
                      <Input type="select" name="type" value={this.state.type} onChange={this.handleTypeOption}>
                        { 
                          this.state.userInfo.type === 'SUPER' 
                          ? <><option value="0">일반관리자</option><option value="1">테스터</option></>
                          : <option value="0">테스터</option> 
                        }
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="ID" sm={2}>
                      아이디*
                    </Label>
                    <Col sm={10} >
                      {
                        this.state.exists 
                        ? <Input invalid type="text" name="id" placeholder="아이디를 입력해주세요." value={this.state.accountId} onChange={this.handleAccountId}/>
                        : <Input type="text" name="id" placeholder="아이디를 입력해주세요." value={this.state.accountId} onChange={this.handleAccountId}/>
                      }
                      <FormFeedback>
                        이미 사용중인 아이디입니다.
                      </FormFeedback>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="Name" sm={2}>
                      이름*
                    </Label>
                    <Col sm={10}>
                      <Input type="text" name="name" placeholder="이름을 입력해주세요." value={this.state.name} onChange={this.handleName}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="Email" sm={2}>
                      이메일*
                    </Label>
                    <Col sm={10}>
                      <Input type="email" name="email" placeholder="이메일 주소를 입력해주세요." value={this.state.email} onChange={this.handleEmail}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="Phone" sm={2}>
                      휴대폰번호*
                    </Label>
                    <Col sm={10}>
                      <Input type="tel" name="phone" onKeyPress={this.onlyNumber} placeholder="휴대폰 번호를 입력해주세요.(하이픈(-) 제외)" value={this.state.phoneNumber} onChange={this.handlePhone}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="Sex" sm={2}>
                      성별*
                    </Label>
                    <Col sm={10}>
                      <Input type="select" name="sex" value={this.state.sex} onChange={this.handleSex}>
                        { 
                          <><option value="0">여자</option><option value="1">남자</option></>
                        }
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="Birth" sm={2}>
                      생년월일*
                    </Label>
                    <Col sm={10}>
                      <Input type="date" name="birthday" value={this.state.birthday} onChange={this.handleBirthday}
                      />
                    </Col>
                  </FormGroup>
                </Form>
                <Button className="mx-1" color="secondary" onClick={this.handleAdd}>추가</Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Page>
    ); 
  }
};

export default AddAdminPage;
