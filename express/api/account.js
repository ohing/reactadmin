var express = require('express');
var router = express.Router();

const crypto = require('crypto')
const { v4: uuidv4 } = require('uuid');

router.post('/', async (req, res) => {

  console.log(req.body)

  let parameters = req.body

  let id = parameters.id
  let password = parameters.password

  let passwordHash = crypto.createHash('sha256').update(password).digest('hex')

  console.log(id, passwordHash)

  let mysql = req.app.get('mysqlW')
  let query
  let result

  query = `
    select 
      ui.user_id, ui.user_type_cd type, ui.user_real_nm name, ui.user_email email,
      case when fi.org_file_nm is null then null else concat('https://${process.env.stage == 'PROD' ? 'd1msh2x81vktjt' : 'd25s42vo6uiaxs'}.cloudfront.net/', fi.org_file_nm) end profile_image_url
    from 
      oi_user_info ui
      left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
      left join oi_file_info fi on upi.file_id = fi.file_id
    where
      ui.user_login_id = '${id}'
      and ui.user_pwd = '${passwordHash}'
      and (ui.user_type_cd = 'SUPER' or ui.user_type_cd = 'OHING')
    limit 
      1;
  `
  console.log(mysql)
  console.log(query)
  result = await mysql.execute(query)
  console.log(222)
  if (result.length == 0) {
    console.log('no user')
    res.send('{}'); 
    return
  }

  let userId = result[0].user_id
  let type = result[0].type
  let name = result[0].name
  let email = result[0].email
  let profileImageUrl = result[0].profile_image_url

  let token = uuidv4()

  query = `
    insert into oi_admin_token (
      user_id, token
    ) values (
      ${userId}, '${token}'
    ) on duplicate key update
      reg_dt = now();
  `
  await mysql.execute(query)

  console.log(token)

  let response = {
    token: token,
    userInfo: {
      userId: userId,
      type: type,
      name: name,
      email: email,
      profileImageUrl: profileImageUrl
    }
  }

  res.send(JSON.stringify(response))
})

router.post('/password', async (req, res) => {
  
  let userId = req.headers.userId
  let password = (req.body.password || '').trim()
  console.log(userId, password)

  if (password.length == 0) {
    res.status(403)
    res.send('Forbidden')
    return 
  }

  let passwordHash = crypto.createHash('sha256').update(password).digest('hex')

  let mysql = req.app.get('mysqlW')

  let query = `
    update
      oi_user_info
    set
      user_pwd = '${passwordHash}'
    where
      user_id = ${userId};
  `
  console.log(query)
  await mysql.execute(query)

  res.send('{}')
})

router.post('/sign-out', async (req, res) => {
  
  let token = req.headers['token']
  
  let mysql = req.app.get('mysqlW')
  
  let query = `
    delete from 
      oi_admin_token 
    where 
      token = '${token}';
  `
  await mysql.execute(query)

  res.send('{}')
})

module.exports = router