import React from 'react';
import PropTypes from 'utils/propTypes';

import classNames from 'classnames';

import { Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

const TextDetailWidget = ({
  bgColor,
  title,
  subtitle,
  className,
  ...restProps
}) => {
  const classes = classNames('cr-widget', className, {
    [`bg-${bgColor}`]: bgColor,
  });
  return (
    <Card inverse className={classes} {...restProps}>
      <CardBody>
        <CardTitle>{title}</CardTitle>
        <CardSubtitle>{subtitle}</CardSubtitle>
      </CardBody>
    </Card>
  );
};

TextDetailWidget.propTypes = {
  bgColor: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

TextDetailWidget.defaultProps = {
  bgColor: 'primary',
};

export default TextDetailWidget;
