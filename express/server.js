const express = require('express');

const app = express();

const dotenv = require("dotenv");
const path = require("path");

dotenv.config({
  path: path.resolve(
    process.cwd(),
    ".env"
  )
})

const PORT = 3300

const mysql = require('./rds.js')
const redis = require('./redis.js')

app.set('mysqlW', mysql.writable())

setRedis = async () => {
  let redis3 = await redis.writable(3)
  let redis7 = await redis.writable(7)
  app.set('redis3', redis3)
  app.set('redis7', redis7)
}
setRedis()

if (process.cwd().includes('karl')) {
  app.set('mysqlR', app.get('mysqlW'))
} else {
  app.set('mysqlR', mysql.readable())
}

app.use(express.json())

app.use(async (req, res, next) => {

  console.log(req.originalUrl)

  if (req.originalUrl.includes('tester') || (!req.originalUrl.includes('test') && !(req.originalUrl == '/api/account/' && req.method == 'POST'))) {

    let token = req.headers.token

    if (token == null) {
      res.status(401)
      res.send('Unauthorized')
      return  
    }

    let mysql = app.get('mysqlR')
    
    let query = `
      select
        user_id
      from
        oi_admin_token
      where
        token = '${token}'
        and reg_dt > date_add(now(), interval -10 hour)
      limit 1;
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      console.log('Logout forced.')
      res.status(401)
      res.send('Unauthorized')
      return  
    }

    console.log('Pass')

    let userId = result[0].user_id
    req.headers.userId = userId

  } else {

    console.log('Login Request')
  }

  next()
})

let accountApi = require('./api/account.js')
app.use('/api/account', accountApi)

let dashboardApi = require('./api/dashboard.js')
app.use('/api/dashboard', dashboardApi)

let adminApi = require('./api/admin.js')
app.use('/api/admin', adminApi)

let memoApi = require('./api/memo.js')
app.use('/api/memo', memoApi)

let userApi = require('./api/user.js')
app.use('/api/user', userApi)

let feedApi = require('./api/feed.js')
app.use('/api/feed', feedApi)

let chatApi = require('./api/chat.js')
app.use('/api/chat', chatApi)

let commentApi = require('./api/comment.js')
app.use('/api/comment', commentApi)

let pushApi = require('./api/push.js')
app.use('/api/push', pushApi)

let noticeApi = require('./api/notice.js')
app.use('/api/notice', noticeApi)

let keywordApi = require('./api/keyword.js')
app.use('/api/keyword', keywordApi)

let partnerApi = require('./api/partner.js')
app.use('/api/partner', partnerApi)

let bannerApi = require('./api/banner.js')
app.use('/api/banner', bannerApi)

let popupApi = require('./api/popup.js')
app.use('/api/popup', popupApi)

let ohingOnApi = require('./api/ohingOn.js')
app.use('/api/ohing-on', ohingOnApi)

let reportApi = require('./api/report.js')
app.use('/api/report', reportApi)

let inquirementApi = require('./api/inquirement.js')
app.use('/api/inquirement', inquirementApi)

let utilApi = require('./api/util.js')
app.use('/api/util', utilApi)
 
app.listen(PORT, () => {
  console.log(`Server run : http://localhost:${PORT}/`)
})