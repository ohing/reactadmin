var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')


router.put('/', async (req, res) => {

  try {

    let mysql = req.app.get('mysqlW')

    let userId = req.headers.userId

    let targetType = req.body.targetType
    let targetId = req.body.targetId
    let memo = req.body.memo

    let query
    let result

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        '${targetType}', ${targetId}, '${memo}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:memoId', async (req, res) => {

  try {

    let mysql = req.app.get('mysqlW')

    let userId = req.headers.userId

    let memoId = req.params.memoId

    res.send(`${memoId}`)

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

module.exports = router