import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label
} from 'reactstrap';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class AdminPage extends React.Component {

  state = {
    currentPage: 0, 
    admins: [],
    rowCount: 10,
    requestType: 0,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    searchText: '',
    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
    deleteModal: false
  };

  selectedIds = new Set([])

  componentDidMount() {

    console.log('componentDidMount')
    this.fetchData()
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      // history.replace(undefined, undefined)
    }
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }
  
  getData = async (props) => {

    try {

      let rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 

      let result = await api.admin().call({apiName: 'getAdmins', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        type: this.state.requestType, 
        rowCount: rowCount, 
        searchText: this.state.searchText
      }})
      let totalCount = result.totalCount
      let admins = result.admins

      console.log('userInfo', this.state.userInfo)

      let totalPage = parseInt(totalCount / rowCount) + (totalCount % rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        admins: admins,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
        rowCount: rowCount
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })

      this.selectedIds = new Set([])

    } catch (e) {

      console.log(e)
    }
  }

  deleteAdmins = async () => {

    try {

      await api.admin().call({apiName: 'deleteAdmins', method: 'delete', parameters: {
        userIds: Array.from(this.selectedIds)
      }})

      this.getData()

      this.props.addNotification('삭제되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handleOption = event => {

    let type = parseInt(event.target.value)
    console.log('type', type)

    this.state.requestType = type
    this.getData()
  }

  handleSearchText = event => {
    let searchText = event.target.value
    this.setState({
      searchText: searchText
    })
  }

  handleKeyDown = event => {
    if (event.key == 'Enter') {
      event.preventDefault();
      this.handleSearch(event)
    }
  }

  handleSearch = event => {
    console.log('searchText', this.state.searchText)
    this.getData()
  }

  handleChecked = event => {
    let checked = event.target.checked
    let userId = parseInt(event.target.id.split(':')[1])
    if (checked) {
      this.selectedIds.add(userId)
    } else {
      this.selectedIds.delete(userId)
    }
  }

  handlePrevious = () => {
    console.log()
  }

  handlePage = event => {
    event.preventDefault();
    let page = parseInt(event.target.innerText) - 1

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = () => {
    
  }

  handleAdd = () => {
    this.props.history.push('/admin/add' )  
  }

  handleRemove = () => {
    if (this.selectedIds.size > 0) {
      this.toggleDeleteModal()()
    }
  }

  handleUser = event => {
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/user/${targetUserId}`) 
  }

  toggleDeleteModal = modalType => () => {
    if (!modalType) {
      this.setState({
        deleteModal: !this.state.deleteModal,
      })
    }
  }

  handleDelete = () => {
    this.deleteAdmins()
    this.setState({
      deleteModal: false
    })
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>관리자 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <Input type="select" value={this.state.requestType} onChange={this.handleOption}>
                          <option value="0">타입 전체</option>
                          <option value="1">슈퍼관리자</option>
                          <option value="2">일반관리자</option>
                          <option value="3">테스터</option>
                        </Input>

                        <div>
                          <Input
                            type="text"
                            name="text"
                            value={this.state.searchText}
                            onChange={this.handleSearchText}
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                          />
                          <Button outline color="secondary" onClick={this.handleSearch}>검색</Button>
                        </div>
                      </Form>
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="40"></th>
                            <th width="120">타입</th>
                            <th width="300">아이디</th>
                            <th width="160">이름</th>
                            <th>이메일</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.admins.map( (admin, index) => {
                              return (
                                <tr key={admin.userId}>
                                  <th scope="row"><Input type="checkbox" className="mx-auto" id={`userId:${admin.userId}`} onChange={this.handleChecked} style={{
                                    visibility: (admin.type === 'SUPER' || this.state.userInfo.userId === admin.userId || (this.state.userInfo.type === 'OHING' && admin.type === 'OHING')) ? 'hidden': 'visible'
                                  }}/></th>
                                  <td>{admin.type === 'SUPER' ? '슈퍼관리자' : admin.type === 'OHING' ? '일반관리자' : '테스터'}</td>
                                  <td><a href="#" onClick={this.handleUser} value={admin.userId}>{admin.accountId}</a></td>
                                  <td>{admin.name}</td>
                                  <td>{admin.email}</td>
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                    <Button className="mx-1" color="secondary" onClick={this.handleAdd}>추가</Button>
                    <Button className="mx-1" color="primary" onClick={this.handleRemove}>삭제</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>{'관리자 삭제'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택된 관리자를 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDelete}>
              삭제
            </Button>{' '}
            
          </ModalFooter>
        </Modal>

      </Page>
    ); 
  }
};

export default AdminPage;
