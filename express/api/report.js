const fs = require('fs')

var express = require('express');

const multer = require('multer');
const upload = multer({
  dest: 'uploads/'
})

var router = express.Router();
var axios = require('axios');
var parser = require('node-html-parser')

const camelcase = require('camelcase-keys')
const { v4: uuidv4 } = require('uuid');

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3()
const cloudfront = new AWS.CloudFront();
const lambda = new AWS.Lambda()

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10

  try {

    let query
    let result

    let requestType = (req.query.requestType || '').trim()
    let isCompleted = req.query.isCompleted

    let typeCondition = requestType == '0' ? '' : `
      and r.report_type_cd like '%${requestType}%'
    `
    
    let completedCondition = isCompleted == '1' ? `
      and r.status_cd = 'NEW'
    ` : isCompleted == '2' ? `
      and r.status_cd = 'END'
    ` : ''
    

//     [{
// 	"code": "RT011",
// 	"codeNm": "타인 사칭, 10대 아님"
// }, {
// 	"code": "RT012",
// 	"codeNm": "음란물, 폭력물 게재"
// }, {
// 	"code": "RT013",
// 	"codeNm": "괴롭힘, 혐오발언"
// }, {
// 	"code": "RT014",
// 	"codeNm": "지적 재산권 침해"
// }, {
// 	"code": "RT015",
// 	"codeNm": "스팸, 허가되지 않은 판매"
// }, {
// 	"code": "RT999",
// 	"codeNm": "기타"
// }]

    let mysql = req.app.get('mysqlR')
    
    query = `
      select sql_calc_found_rows
        r.report_id, r.target_type_cd, r.target_id, r.report_type_cd, r.report_content, r.status_cd, r.report_user_id, r.report_dt 
      from 
        oi_report r
      where
        1=1
        ${typeCondition}
        ${completedCondition}
      order by 
        r.report_id desc
      limit
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    if (result.length == 0) {
      res.send(JSON.stringify({
        reports: [],
        totalCount: 0
      }))
      return
    }

    let reportIdsMap = {}
    result.forEach(row => {
      let types = []
      let typesMap = {
        'RT011': '사칭/10대아님',
        'RT012': '음란/폭력물',
        'RT013': '괴롭힘/혐오',
        'RT015': '스팸/미허가판매',
        'RT999': '기타'
      }
      row.report_type_cd.split('|').forEach(type => {
        if (type.length > 0) {
          let typeValue = typesMap[type]
          if (typeValue != null) {
            types.push(typeValue)
          }
        }
      })
      row.types = types
      row.medias = []
      reportIdsMap[row.report_id] = row
    })

    let reportIds = Object.keys(reportIdsMap)

    query = `
      select found_rows() rows
    `
    result = await mysql.execute(query)
    let rows = result[0].rows

    query = `
      select 
        rmi.report_id, fi.media_type_cd type, substring_index(fi.org_file_nm, '/', -1) file_name
      from 
        oi_report_media_info rmi
        left join oi_file_info fi on rmi.file_id = fi.file_id
      where
        report_id in (${reportIds.join(',')})
      order by 
        rmi.report_id desc,
        rmi.media_seq asc;
    `
    result = await mysql.execute(query)

    for (let row of result) {
      reportIdsMap[row.report_id].medias.push(row)
    }

    let reports = Object.values(reportIdsMap)
    reports = reports.sort( (lhs, rhs) => {
      return lhs.report_id > rhs.report_id ? -1 : lhs.report_id < rhs.report_id ? 1 : 0
    })

    reports = camelcase(reports, {deep: true})

    res.send(JSON.stringify({
      reports: reports,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:reportId/complete', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId
  let reportId = req.params.reportId

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_report
      set
        status_cd = 'END'
      where
        report_id = ${reportId};
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.post('/:reportId/cancel-complete', async (req, res) => {

  console.log(req.headers, req.query, req.queries, req.body, req.file)

  let userId = req.headers.userId
  let reportId = req.params.reportId

  try {

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      update
        oi_report
      set
        status_cd = 'NEW'
      where
        report_id = ${reportId};
    `
    await mysql.execute(query)

    res.send('{}')

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


module.exports = router