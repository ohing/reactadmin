import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class NoticeCreatePage extends React.Component {

  state = {
    category: '0',
    title: ''
  };

  editor

  container = `
    <!DOCTYPE html>
    <html lang="ko">

    <head>
      <meta charset="utf-8">
      <title>ohing</title>
      <link rel="stylesheet" href="./css/style.css">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="shortcut icon" href="./img/favicon.ico">
      <!-- Stylesheets -->
      <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;400;500&display=swap" rel="stylesheet">
    </head>

    <body>

      <div class="container">
        <!-- 
        <hgroup>
          <h1>{{title}}</h1>
          <h3>{{date}}</h3>
        </hgroup>

        <hr />
        -->

        <hgcgroup>
          {{content}}
        </hgcgroup>

        <footer>
          <p></p>
        </footer>
      </div> <!-- end container -->

    </body>

    </html>
  `

  componentDidMount() {
    console.log('DidMount')

    // const script =  document.createElement('script')
    // script.src = '/js/htmeditor.min.js'
    // script.async = false
    // script.setAttribute('htmeditor_textarea', 'htmeditor')

    // document.body.appendChild(script)
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  handleCategory = event => {
    this.setState({
      category: event.target.value
    })
  }

  handleTitle = event => {
    this.setState({
      title: event.target.value
    })
  }

  handleComplete = async () => {

    let category = {
      '0': '이벤트',
      '1': '공지',
      '2': '업데이트',
    }[this.state.category]

    if (category == null) {
      return
    }

    let title = this.state.title.trim()
    if (title.length == 0) {
      this.props.addNotification('제목을 입력해 주세요.')
      return
    }

    let content = this.editor.getContent().trim()

    if (content.length == 0) {
      this.props.addNotification('본문을 입력해 주세요.')
    }

    let date = new Date()
    let month = `${date.getMonth()+1}`.padStart(2, '0')
    let day = `${date.getDate()}`.padStart(2, '0')
    let hours = `${date.getHours()}`.padStart(2, '0')
    let minutes = `${date.getMinutes()}`.padStart(2, '0')
    let dateString = `${date.getFullYear()}-${month}-${day} ${hours}:${minutes}`

    let titleString = `[${category}] ${title}`

    let container = this.container
    container = container.replace('{{title}}', titleString).replace('{{date}}', dateString).replace('{{content}}', content)

    let parameters = {
      category: category,
      title: title,
      body: container
    }

    try {

      let result = await api.notice().call({apiName: 'createNotice', parameters, method: 'put'})
      
      this.props.addNotification('공지사항 작성이 완료되었습니다.')
      this.props.history.replace('/notice')

    } catch (e) {
      console.error(e)
    }
  }

  render() {

    console.log('render')

    return (
      <Page
        className="NoticeCreatePage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>공지사항 작성</CardHeader>

              <CardBody>
                <Row>
                  <Label className="mx-3 mb-1">카테고리*</Label>
                  <Input className="mx-3 mb-3" type="select" value={this.state.category} onChange={this.handleCategory}>
                    <option value="0">이벤트</option>
                    <option value="1">공지</option>
                    <option value="2">업데이트</option>
                  </Input>
                  <Label className="mx-3 mb-1">제목*</Label>
                  <Input className="mx-3 mb-3" type="text" placeholder="제목 입력" onChange={this.handleTitle} value={this.state.title}/>
                </Row>
                <Row>
                  <Col>
                    <Label className="mb-1">본문*</Label>
                    <Editor
                      apiKey="r09leto1eukgg5osw1zkjopapducl9c04vmoxbfdycxkzmfa"
                      onInit={ (evt, editor) => {
                        this.editor = editor
                      }}
                      init={{
                        height: 500,
                        // menubar: false,
                        plugins: [
                          'advlist autolink lists link image charmap print preview anchor',
                          'searchreplace visualblocks code fullscreen',
                          'insertdatetime media table paste code help wordcount',
                        ],
                        toolbar: 'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | help',
                        content_style: 'body { font-size:14px }'
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Button className="ml-3" onClick={this.handleComplete}>작성 완료</Button>
                </Row>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </Page>
    ); 
  }
};

export default NoticeCreatePage;
