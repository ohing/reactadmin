var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10
  let status = req.query.status || 0
  let type = req.query.type || 0
  let anonymous = req.query.anonymous || 0
  let reportedType = req.query.reportedType || 0
  let searchText = (req.query.searchText || '').trim()

  let statusCondition = status == 1 ? `
    and f.feed_except_yn = 'N'
  ` : status == 2 ? `
    and f.feed_except_yn = 'Y'
  ` : ''

  let typeCondition = type == 1 ? `
    and f.feed_type_cd = 'FEED'
  ` : type == 2 ? `
    and f.feed_type_cd = 'HELP'
  ` : ''

  let anonymousCondition = anonymous == 1 ? `
    and f.anony_yn = 'Y'
  ` : anonymous == 2 ? `
    and f.anony_yn = 'N'
  ` : ''

  let reportedTypeCondition = reportedType == 1 ? `
    and r.status_cd = 'NEW' and r.report_id is not null
  ` : ''

  let searchCondition = searchText.length == 0 ? '' : `
    and (
      f.feed_content like '%${searchText}%'
      or ui.user_login_id like '%${searchText}%'
    )
  `

  let mysql = req.app.get('mysqlR')

  let query
  let result

  try {    

    query = `
      select sql_calc_found_rows
        f.feed_id, f.feed_type_cd type, f.feed_except_yn is_blocked, 
        case
          when f.feed_except_yn = 'Y' then f.upd_dt
          else null
        end block_dt,
        f.anony_yn is_anonymous,
        f.feed_content content, f.reg_dt created_at,
        ui.user_id user_id, ui.user_login_id account_id,
        (select count(*) from oi_report where target_type_cd = 'FEED' and target_id = f.feed_id) reported_count,
        (select count(*) from oi_report where target_type_cd = 'FEED' and target_id = f.feed_id and status_cd = 'NEW') uncompleted_report_count
      from 
        oi_feed_info f
        join oi_user_info ui on f.reg_user_id = ui.user_id
        left join oi_report r on f.feed_id = r.target_id and r.target_type_cd = 'FEED'
      where
        f.encoded_yn = 'Y'
        ${statusCondition}
        ${typeCondition}
        ${anonymousCondition}
        ${reportedTypeCondition}
        ${searchCondition}
      order by
        f.reg_dt desc
      limit 
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    query = `
      select found_rows() count
    `
    let countResult = await mysql.execute(query)
    let count = countResult[0].count

    if (result.length == 0) {
      res.send(JSON.stringify({
        feeds: [],
        totalCount: count
      }))
      return
    }

    let feedIdMap = {}

    for (let row of result) {
      row.is_blocked = row.is_blocked == 'Y'
      row.is_anonymous = row.is_anonymous == 'Y'
      row.medias = []
      row.tags = []
      feedIdMap[row.feed_id] = row
    }
    
    query = `
      select 
        fmi.feed_id, fmi.media_seq, 
        fi.media_type_cd type, substring_index(fi.org_file_nm, '/', -1) file_name
      from 
        oi_feed_media_info fmi
        join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id in (${Object.keys(feedIdMap).join(',')})
      order by
        fmi.media_seq asc;

    `
    console.log(query)
    result = await mysql.execute(query)

    for (let row of result) {
      feedIdMap[row.feed_id].medias.push(row)
    }

    query = `
      select 
        ft.feed_id, ti.tag_val
      from
        oi_tag_info ti
        join oi_feed_tag ft on ti.tag_id = ft.tag_id
        join oi_feed_info f on ft.feed_id = f.feed_id
      where
        f.feed_id in (${Object.keys(feedIdMap).join(',')})
      order by
        ft.tag_seq asc;
    `
    console.log(query)
    result = await mysql.execute(query)
    
    for (let row of result) {
      feedIdMap[row.feed_id].tags.push(row.tag_val)
    }

    let feeds = Object.values(feedIdMap)
    feeds.sort( (lhs, rhs) => {
      return lhs.created_at < rhs.created_at ? 1 : lhs.created_at > rhs.created_at ? -1 : 0
    })

    console.log(feeds)

    feeds = camelcase(feeds, {deep:true})

    res.send(JSON.stringify({
      feeds: feeds,
      totalCount: count
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.get('/:feedId', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetFeedId = req.params.feedId
    
    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select
        f.feed_id, f.feed_type_cd type, f.feed_except_yn is_blocked, 
        case
          when f.feed_except_yn = 'Y' then f.upd_dt
          else null
        end block_dt,
        f.anony_yn is_anonymous,
        f.feed_content content, f.reg_dt created_at,
        ui.user_id user_id, ui.user_login_id account_id,
        (select count(*) from oi_report where target_type_cd = 'FEED' and target_id = f.feed_id) reported_count,
        (select count(*) from oi_report where target_type_cd = 'FEED' and target_id = f.feed_id and status_cd = 'NEW') uncompleted_report_count
      from
        oi_feed_info f 
        join oi_user_info ui on f.reg_user_id = ui.user_id
      where
        feed_id = ${targetFeedId};
    `
    result = await mysql.execute(query)

    let feed = result[0]    
    feed.is_blocked = feed.is_blocked == 'Y'
    feed.is_anonymous = feed.is_anonymous == 'Y'
    feed.medias = []

    query = `
      select 
        fmi.feed_id, fmi.media_seq, 
        fi.media_type_cd type, substring_index(fi.org_file_nm, '/', -1) file_name
      from 
        oi_feed_media_info fmi
        join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id = ${targetFeedId};
    `
    result = await mysql.execute(query)
    feed.medias = result

    query = `
      select  
        ti.tag_val
      from 
        oi_tag_info ti
        join oi_feed_tag ft on ti.tag_id = ft.tag_id
      where
        ft.feed_id = ${targetFeedId}
      order by
        ft.tag_seq asc;
    `
    result = await mysql.execute(query)
    feed.tags = result.map(row => {
      return row.tag_val
    })

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        am.target_type_cd = 'FEED'
        and am.target_id = ${targetFeedId}
      order by memo_id desc
    `
    feed.memos = await mysql.execute(query)

    feed = camelcase(feed, {deep:true})

    res.send(JSON.stringify(feed))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


router.put('/:feedId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetFeedId = req.params.feedId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      update 
        oi_feed_info
      set
        feed_except_yn = 'Y',
        upd_dt = now()
      where
        feed_id = ${targetFeedId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'FEED', ${targetFeedId}, '(시스템) 차단하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:feedId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetFeedId = req.params.feedId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      update 
        oi_feed_info
      set
        feed_except_yn = 'N',
        upd_dt = now()
      where
        feed_id = ${targetFeedId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'FEED', ${targetFeedId}, '(시스템) 차단 해제하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


module.exports = router