var express = require('express');
var router = express.Router();

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

router.get('/', async (req, res) => {

  console.log(req.headers, req.query, req.queries)

  let userId = req.headers.userId

  let page = req.query.currentPage || 0
  let rowCount = req.query.rowCount || 10
  let type = req.query.type || 0
  let status = req.query.status || 0
  let openType = req.query.openType || 0
  let reportedType = req.query.reportedType || 0
  let searchText = (req.query.searchText || '').trim()

  let statusCondition = status == 1 ? `
    and user_status_cd = 'ACTIVE'
  ` : status == 2 ? `
    and user_status_cd = 'BLOCK'
  ` : `
    and user_status_cd in ('ACTIVE', 'BLOCK')
  `

  let openTypeCondition = openType == 1 ? `
    and profile_open_type_cd = 'OPEN'
  ` : openType == 2 ? `
    and profile_open_type_cd = 'FRIEND'
  ` : openType == 3 ? `
    and profile_open_type_cd = 'CLOSE'
  ` : ''

  let reportedTypeCondition = reportedType == 1 ? `
    and r.status_cd = 'NEW' and r.report_id is not null
  ` : ''

  let searchCondition = searchText.length == 0 ? '' : `
    and (
      user_login_id like '%${searchText}%'
      or user_real_nm like '%${searchText}%'
      or user_email like '%${searchText}%'
    )
  `

  let mysql = req.app.get('mysqlR')

  let query
  let result

  try {    

    query = `
      select
        ui.user_id, ui.user_type_cd type, replace(ui.user_login_id, '알 수 없는 사용자::', '') account_id, 
        ui.user_real_nm name, ui.user_email email, ui.reg_dt created_at, ui.upd_dt updated_at,
        case 
          when ui.profile_open_type_cd = 'OPEN' THEN '전체공개'
          when ui.profile_open_type_cd = 'FRIEND' THEN '친구공개'
          when ui.profile_open_type_cd = 'CLOSE' THEN '비공개'
          else '이상'
        end open_type,
        (select count(*) from oi_report where target_type_cd = 'USER' and target_id = ui.user_id) reported_count,
        (select count(*) from oi_report where target_type_cd = 'USER' and target_id = ui.user_id and status_cd = 'NEW') uncompleted_report_count,
        udi.os_type_cd
      from 
        oi_user_info ui
        left join oi_user_device_info udi on ui.user_id = udi.user_id and udi.login_yn = 'Y'
        left join oi_report r on ui.user_id = r.target_id and r.target_type_cd = 'USER'
      where
        1=1
        ${statusCondition}
        ${openTypeCondition}
        ${reportedTypeCondition}
        ${searchCondition}
      group by 
	      ui.user_id
      order by
        ui.user_id desc
      limit 
        ${page * rowCount}, ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    let users = camelcase(result, {deep:true})

    query = `
      select
        count(*) count
      from 
        oi_user_info ui
        left join oi_report r on ui.user_id = r.target_id and r.target_type_cd = 'USER'
      where
        1=1
        ${statusCondition}
        ${openTypeCondition}
        ${reportedTypeCondition}
        ${searchCondition}
      group by 
	      ui.user_id
        ;
    `
    result = await mysql.execute(query)
    let rows = result[0].count

    res.send(JSON.stringify({
      users: users,
      totalCount: rows
    }))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})


router.get('/:userId', async (req, res) => {

  try {

    let targetUserId = req.params.userId
    
    let query
    let result

    let mysql = req.app.get('mysqlR')

    query = `
      select 
        following.count following_count, follower.count follower_count, jjim.count jjim_count, jjimed.count jjimed_count,
        reported.count reported_count, uncompleted_report.count uncompleted_report_count,
        user_info.*
      from
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.user_id = ui.user_id where 
          ur.relation_user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') following,
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.relation_user_id = ui.user_id where 
          ur.user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') follower,
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.user_id = ui.user_id where 
          ur.relation_user_id = ${targetUserId} and ur.relation_type_cd = '05' and ui.profile_open_type_cd != 'CLOSE') jjim,
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.relation_user_id = ui.user_id where 
          ur.user_id = ${targetUserId} and ur.relation_type_cd = '05' and ui.profile_open_type_cd != 'CLOSE') jjimed,
        (select count(*) count from oi_report where target_type_cd = 'USER' and target_id = ${targetUserId}) reported,
        (select count(*) count from oi_report where target_type_cd = 'USER' and target_id = ${targetUserId} and status_cd = 'NEW') uncompleted_report,
        (select 
          ui.user_id, ui.user_login_id account_id, user_real_nm name, ui.user_nick_nm nickname,
          ui.user_type_cd type, ui.user_status_cd status, ui.profile_open_type_cd open_type,
          ui.user_hp_no, ui.user_email, ui.sex, ui.region_cd, ui.birth_y, ui.birth_md,
          ui.reg_dt created_at, ui.ci,
          substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          substring_index(fi2.org_file_nm, '/', -1) background_file_name,
          udi.os_type_cd
        from 
          oi_user_info ui
          left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
          left join oi_file_info fi on pi.file_id = fi.file_id
          left join oi_user_profile_image pi2 on ui.user_id = pi2.user_id and pi2.image_type_cd = 'BG'
          left join oi_file_info fi2 on pi2.file_id = fi2.file_id
          left join oi_user_device_info udi on ui.user_id = udi.user_id and udi.login_yn = 'Y'
        where 
          ui.user_id = ${targetUserId}) user_info;
    `
    result = await mysql.execute(query)

    if (result.length == 0) {
      res.send(null)
      return
    }

    let userInfo = result[0]

    query = `
      select 
        ti.tag_val
      from
        oi_user_info ui
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ui.user_id = ${targetUserId}
      order by 
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)
    userInfo.tags = result.map(row => {
      return row.tag_val
    })

    if (userInfo.tags.length == 1 && userInfo.tags[0] == null) {
      userInfo.tags = []
    }

    query = `
      select
        block_seq, block_days, reason, reg_dt
      from
        oi_user_block
      where
        user_id = ${targetUserId};
    `
    userInfo.blocks = await mysql.execute(query)

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        am.target_type_cd = 'USER'
        and am.target_id = ${targetUserId}
      order by 
        memo_id desc
    `
    userInfo.memos = await mysql.execute(query)

    let params = {
      FunctionName: `api-new-user-${process.env.stage}-isBlackList`,
      Payload: JSON.stringify({
        pathParameters: {
          targetUserId: targetUserId
        },
        queryStringParameters: {
          ci: userInfo.ci
        }
      })
    }
    let blacklistResult = await lambda.invoke(params).promise()
    let payload = JSON.parse(blacklistResult.Payload)
    let body = JSON.parse(payload.body)
    userInfo.isBlackList = body.isBlackList

    console.log(userInfo)
    
    userInfo = camelcase(userInfo, {deep:true})

    res.send(userInfo)

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:userId/blacklist', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      select 
        ci 
      from 
        oi_user_info 
      where 
        user_id = ${targetUserId}
    `
    result = await mysql.execute(query)
    let ci = result[0].ci

    query = `
      select
        user_id
      from
        oi_user_info
      where
        ci = '${ci}';
    `
    result = await mysql.execute(query)
    let userIds = result.map(row => {
      return row.user_id
    })
    
    query = `
      update 
        oi_user_info
      set
        user_status_cd = 'BLOCK'
      where
        ci = '${ci}';
    `
    await mysql.execute(query)

    values = []

    for (let oneUserId of userIds) {
      values.push(`
        ( 'USER', ${oneUserId}, '(시스템) 블랙리스트 및 차단 등록하였습니다. 사유: ${reason}', ${userId} )
      `)
    }

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values
        ${values};
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        am.target_id = ${targetUserId}
      order by
        am.memo_id desc
      limit
        1;
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    let params = {
      FunctionName: `api-new-user-${process.env.stage}-setBlackList`,
      Payload: JSON.stringify({
        pathParameters: {
          targetUserId: targetUserId
        },
        queryStringParameters: {
          ci: ci
        }
      })
    }
    let blackListResult = await lambda.invoke(params).promise()
    console.log(blackListResult)

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:userId/blacklist', async (req, res) => {

  console.log('aaaaa')

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      select
        ci
      from 
        oi_user_info
      where
        user_id = ${targetUserId};
    `
    result = await mysql.execute(query)
    let ci = result[0].ci

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'USER', ${targetUserId}, '(시스템) 블랙리스트 해제하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    console.log(process.env.stage)

    let params = {
      FunctionName: `api-new-user-${process.env.stage}-removeBlackList`,
      Payload: JSON.stringify({
        pathParameters: {
          targetUserId: targetUserId
        },
        queryStringParameters: {
          ci: ci
        }
      })
    }
    let blackListResult = await lambda.invoke(params).promise()
    console.log(blackListResult)

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:userId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let blockDays = req.body.days || 1
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      select 
        ifnull(max(block_seq), 0) seq
      from 
        oi_user_block 
      where 
        user_id = ${targetUserId};
    `
    result = await mysql.execute(query)

    let nextSequence = result[0].seq + 1

    query = `
      insert into oi_user_block (
        user_id, block_seq, block_days, reason
      ) values (
        ?, ?, ?, ?
      )
    `
    await mysql.execute(query, [targetUserId, nextSequence, blockDays, reason])

    query = `
      update 
        oi_user_info
      set
        user_status_cd = 'BLOCK'
      where
        user_id = ${targetUserId};
    `
    await mysql.execute(query)

    query = `
      select
        block_seq, block_days, reason, reg_dt
      from
        oi_user_block
      where
        user_id = ${targetUserId}
      order by 
        block_seq desc
      limit
        1;
    `
    result = await mysql.execute(query)

    let blockInfo = result[0]

    let memoText = `(시스템) ${blockDays}일 차단하였습니다. 사유: ${reason}`

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'USER', ${targetUserId}, ?, ${userId}
      );
    `
    result = await mysql.execute(query, [memoText])
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)

    let memoInfo = result[0]

    let response = camelcase({blockInfo: blockInfo, memoInfo: memoInfo}, {deep:true})

    res.send(JSON.stringify(response))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:userId/block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      update 
        oi_user_info
      set
        user_status_cd = 'ACTIVE'
      where
        user_id = ${targetUserId};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'USER', ${targetUserId}, '(시스템) 차단 해제하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify({blockInfo: null, memoInfo: memoInfo}))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.put('/:userId/image-block', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let type = (req.body.type || '').trim()
    let url = (req.body.url || '').trim()
    if (type.length == 0 || url.length == 0) {
      res.status(403)
      res.send('Forbidden')
      return
    }
    let reason = (req.body.reason || '').trim()
    if (reason.length == 0) {
      reason = '미입력'
    }
    
    let query
    let result

    let mysql = req.app.get('mysqlW')
    
    query = `
      delete from
        oi_user_profile_image
      where
        user_id = ${targetUserId}
        and image_type_cd = '${type === 'Profile' ? 'PF' : 'BG'}';
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'USER', ${targetUserId}, '(시스템) <a href="${url}">${type === 'Profile' ? '프로필' : '배경'} 이미지</a>를 차단하였습니다. 사유: ${reason}', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)
    let memoInfo = camelcase(result[0], {deep:true})

    res.send(JSON.stringify(memoInfo))

  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

router.delete('/:userId/tag', async (req, res) => {

  try {

    let userId = req.headers.userId

    let targetUserId = req.params.userId
    let tag = (req.body.tag || '').trim()

    let query
    let result

    let mysql = req.app.get('mysqlW')

    query = `
      select 
        ti.tag_val
      from 
        oi_user_tag ut
          join oi_tag_info ti on ut.tag_id = ti.tag_id
      where 
        ut.user_id = ${targetUserId}
      order by
        ut.tag_seq asc;
    `
    console.log(query)
    result = await mysql.execute(query)

    let tags = result.map(row => {
      return row.tag_val
    })

    let index = tags.indexOf(tag)
    if (index !== -1) {
      tags.splice(index, 1)
    }

    query = `
      delete from
        oi_user_tag
      where
        user_id = ${targetUserId};
    `
    await mysql.execute(query)

    values = []

    let seq = 0
    for (let tag of tags) {
      seq += 1
      values.push(`
        ( ${targetUserId}, (select tag_id from oi_tag_info where tag_val = '${tag}'), now(), ${seq} )
      `)
    }

    query = `
      insert into oi_user_tag (
        user_id, tag_id, reg_dt, tag_seq
      ) values 
        ${values.join(',')};
    `
    await mysql.execute(query)

    query = `
      insert into oi_admin_memo (
        target_type_cd, target_id, memo, admin_user_id
      ) values (
        'USER', ${targetUserId}, '(시스템) 태그 [${tag}]를 삭제하였습니다.', ${userId}
      );
    `
    result = await mysql.execute(query)
    let memoId = result.insertId

    query = `
      select 
        am.memo_id, am.memo, am.reg_dt,
        ui.user_id, ui.user_login_id
      from 
        oi_admin_memo am
        left join oi_user_info ui on am.admin_user_id = ui.user_id
      where
        memo_id = ${memoId};
    `
    result = await mysql.execute(query)

    let memo = result[0]

    let resultInfo = {
      memoInfo: memo,
      tags: tags
    }
    
    let camelcased = camelcase(resultInfo, {deep:true})
    
    res.send(JSON.stringify(camelcased))


  } catch (e) {
    console.error(e)
    res.status(403)
    res.send('Forbidden')
    return 
  }
})

module.exports = router