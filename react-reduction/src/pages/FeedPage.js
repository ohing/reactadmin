import React from 'react';
import Avatar from 'components/Avatar';

import defaultProfileImage from 'assets/img/users/default_200.png';
import defaultBackgroundImage from 'assets/img/users/default_profile_bg.png';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  CardImg,
  Col, 
  Row, 
  // Form,
  FormGroup,
  // FormFeedback,
  Label,
  Input,
  Button,
  // Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class FeedPage extends React.Component {

  state = {
    feedId: '',
    type: '',
    status: '',
    userId: '',
    accountId: '',
    text: '',
    medias: [],
    createdAt: '',
    tags: [],
    reported: null,

    userInfo: JSON.parse(window.localStorage.getItem('userInfo')),
    memoText: '',
    memos: [],
    modal: false,
    blockReason: '',
    imageModal: false,
    imageModalType: '',
    imageBlockModal: false,
    imageBlockReason: '',
    sendSMSModal: false,
    sendSMS: '',
    sendEmailModal: false,
    sendEmail: '',
  };

  targetFeedId = this.props.match.params.feedId
  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {
    console.log('DidMount')
    console.log(this.props)

    this.getData()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevProps.match.params.userId != this.props.match.params.userId) {
      this.targetFeedId = this.props.match.params.userId
      window.scrollTo(0, 0);
      this.getData()
    }
  }

  typeString = (feedInfo) => {
    return feedInfo.type == 'FEED' ? '게시물' : '도와줘'
  }

  statusString = (feedInfo) => {
    return feedInfo.isBlocked ? '차단됨' : '정상'
  }

  createdAtString = (feedInfo) => {
    let createdAt = feedInfo.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')
    return createdAt
  }

  reportedString = (feedInfo) => {
    return (
      feedInfo.reportedCount > 0 
      ? <a href="#" onClick={this.handleReported}>{`${feedInfo.uncompletedReportCount}/${feedInfo.reportedCount}`}</a>
      : '0/0'
    )
  }

  getData = async () => {
    
    let result = await api.feed().call({apiName: 'getFeed', pathParameter: this.targetFeedId, method: 'get'})
    let type = this.typeString(result)
    let status = this.statusString(result)
    let createdAt = this.createdAtString(result)
    let reported = this.reportedString(result)

    this.setState({
      feedId: `${result.feedId}`,
      type: type,
      status: status,
      userId: result.userId,
      accountId: result.accountId,
      text: result.content,
      medias: result.medias,
      createdAt: createdAt,
      tags: result.tags,
      reported: reported,
      memos: result.memos,
      modal: false,
    })
  }

  handleAcccountId = event => {
    event.preventDefault()
    let userId = event.target.getAttribute('value')
    this.props.history.push(`/user/${userId}`) 
  }

  handleTag = event => {
    event.preventDefault()
    let tag = event.target.getAttribute('value')
    this.props.history.push(`/tag/${tag}`) 
  }

  handleMemoUser = event => {
    event.preventDefault()
    let userId = event.target.getAttribute('value')
    this.props.history.push(`/user/${userId}`) 
  }

  handleMemo = event => {
    this.setState({
      memoText: event.target.value
    })
  }

  handleMemoRegister = async () => {
    let memo = this.state.memoText.trim()

    if (memo.length == 0) {
      this.props.addNotification('메모를 입력해 주세요.')
      return
    }

    try {
      let parameters = {
        targetType: 'FEED',
        targetId: this.targetFeedId,
        memo: memo
      }
      let result = await api.memo().call({apiName: 'registerMemo', method: 'put', parameters: parameters})

      let memos = [result, ...this.state.memos]

      this.setState({
        memos: memos,
        memoText: ''
      })

    } catch (e) {

      console.log(e)
    }
  }

  handleMemoFeed = event => {
    
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))

    console.log(targetUserId)

    this.props.history.push(`/user/${targetUserId}`) 
  }

  toggleBlockModal = modalType => () => {

    if (!modalType) {
      this.setState({
        modal: !this.state.modal,
      })
    }
  }

  handleBlockReason = event => {
    this.setState({
      blockReason: event.target.value
    })
  }

  handleBlock = async () => {

    try {

      let apiName = this.state.status === '차단됨' ? 'unblockFeed' : 'blockFeed'
      let method = this.state.status === '차단됨' ? 'delete' : 'put'

      let nextStatus = this.state.status === '차단됨' ? '정상' : '차단됨'
      let message = this.state.status === '차단됨' ? '차단 해제되었습니다.' : '차단되었습니다.'

      let result = await api.feed().call({apiName: apiName, method: method, pathParameter: this.targetFeedId, parameters: {reason: this.state.blockReason}})
      let memos = [result, ...this.state.memos]

      this.setState({
        modal: false,
        memos: memos,
        blockReason: '',
        status: nextStatus,
      })
       
      this.props.addNotification(message)

    } catch (e) {
      console.error(e)
    }
  }
  
  handleMedia = event => {

    let feedId = parseInt(event.target.getAttribute('value'))
    let mediaUrl = event.target.getAttribute('src') 
    let mediaType = event.target.getAttribute('type') 
    let mediaSeq = parseInt(event.target.getAttribute('seq'))

    let url = new URL(mediaUrl)
    let paths = url.pathname.split('/')
    let fileName = paths[paths.length-1]

    if (mediaType === 'MOVIE') {  
      let pureName = fileName.split('.')[0]
      mediaUrl = `${this.mediaCloudfrontUrl}/mov/${pureName}/mp4/${pureName}.mp4`
    }

    this.setState({
      currentMediaType: mediaType,
      currentMediaUrl: mediaUrl,
      currentFileName: fileName,
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  downloadImage = () => {

    let fileName = this.state.currentFileName
    let splits = fileName.split('.')
    let pureName = splits[0]
    let fileExtension = splits[1]

    let mediaType = this.state.currentMediaType

    let url = this.state.currentMediaUrl

    console.log(pureName, fileExtension)
    let element = document.createElement('a');
    element.setAttribute('href', url);
    element.setAttribute('download', fileName);
    document.body.appendChild(element);
    element.click();

    // this.setState({
    //   currentMediaType: null,
    //   currentMediaUrl: null,
    //   currentFileName: null,
    //   imageModal: false
    // })
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="FeedPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>게시물 정보</CardHeader>
              <CardBody>
                <Row>
                  <Col sm={5}>
                    <Row>
                      <Label for="IDHeader" className="font-weight-bold h-1" sm={4}>
                        아이디
                      </Label>
                      <Label for="ID" className="h-1" sm={6}>
                        {this.state.feedId}
                      </Label>
                    </Row>
                    <Row>
                      <Label for="TypeHeader" className="font-weight-bold h-1" sm={4}>
                        타입
                      </Label>
                      <Label for="Type" className="h-1" sm={6}>
                        {this.state.type}
                      </Label>
                    </Row>
                    <Row>
                      <Label for="StatusHeader" className="font-weight-bold h-1" sm={4}>
                        상태
                      </Label>
                      <Label for="Status" className="h-1" sm={6}>
                        {this.state.status}
                      </Label>
                    </Row>
                    <Row>
                      <Label for="AccountIDHeader" className="font-weight-bold h-1" sm={4}>
                        작성자
                      </Label>
                      <Label for="AccountID" className="h-1" sm={6}>
                        <a value={this.state.userId} href="#" onClick={this.handleAcccountId}>{this.state.accountId}</a>
                      </Label>
                    </Row>
                    <Row>
                      <Label for="CreatedAtHeader" className="font-weight-bold h-1" sm={4}>
                        작성일시
                      </Label>
                      <Label for="CreatedAt" className="h-1" sm={6}>
                        {this.state.createdAt}
                      </Label>
                    </Row>

                  </Col>

                  <Col sm={7}>
                    <Row>
                      <Label for="TagHeader" className="font-weight-bold h-1" sm={3}>
                        관심사 태그
                      </Label>

                      <Label for="Tag" className="h-1" sm={7}>
                      {
                        this.state.tags.map( (tag, i) => {
                          return (
                            <a key={tag} className="mr-1" href="#" value={tag} onClick={this.handleTag}>#{tag}</a>
                          )
                        })
                      } 
                      </Label>
                    </Row>
                    <Row>
                      <Label for="ReportedHeader" className="font-weight-bold h-1" sm={3}>
                        신고당함
                      </Label>
                      <Label for="Reported" className="h-1" sm={7}>
                        {this.state.reported}
                      </Label>
                    </Row>
                    <Row>
                      <Label for="TextHeader" className="font-weight-bold h-1" sm={3}>
                        본문
                      </Label>
                      <Label for="Text" className="h-1" sm={7}>
                        {this.state.text}
                      </Label>
                    </Row>

                  </Col>

                </Row>
                <Row>
                  {
                    this.state.medias.map( (media, index) => {

                      let fileName = media.fileName
                      let pureName = fileName.split('.')[0]

                      let mediaUrl = media.type === 'MOVIE' 
                      ? `${this.mediaCloudfrontUrl}/mov/${pureName}/jpg/${pureName}.0000000.jpg`
                      : `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`

                      console.log(mediaUrl)

                      let icon = media.type === 'MOVIE'
                      ? <MdVideocam className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,20%)'}}/>
                      : <MdPhoto className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,20%)'}}/>
                      
                      return (
                        <>
                          <CardImg
                            className="card-img ml-1 mb-2 can-click"
                            src={mediaUrl}
                            value={media.feedId}
                            key={media.mediaSeq}
                            seq={media.mediaSeq}
                            type={media.type}
                            style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                            onClick={this.handleMedia}/>
                          {icon}
                        </>
                      )
                    })
                  }
                </Row>

              </CardBody>

              {
                this.state.status === '' ? '' :
                <>
                <Button className="mx-3 mb-3" color={this.state.status === '차단됨' ? 'warning' : 'dark'} onClick={this.toggleBlockModal()}>{this.state.status === '차단됨' ? '차단 해제' : '차단'}</Button>
                <Modal
                  isOpen={this.state.modal}
                  toggle={this.toggleBlockModal()}
                  className={this.props.className}>
                  <ModalHeader toggle={this.toggleBlockModal()}>{this.state.status === '차단됨' ? '게시물 차단 해제' : '게시물 차단'}</ModalHeader>
                  <ModalBody>
                    <Label>{this.state.status === '차단됨' ? '이 게시물을 차단 해제하시겠습니까?' : '이 게시물을 차단하시겠습니까?'}</Label>
                    <Input type="text" className="my-3" id="blockReason" placeholder={this.state.status === '차단됨' ? '차단 해제 사유를 입력해주세요.' : '차단 사유를 입력해주세요.'} onChange={this.handleBlockReason} value={this.state.blockReason}/>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={this.toggleBlockModal()}>
                      취소
                    </Button>
                    <Button color={this.state.status === '차단됨' ? 'warning' : 'dark'} onClick={this.handleBlock}>
                      {this.state.status === '차단됨' ? '차단 해제' : '차단'}
                    </Button>{' '}
                    
                  </ModalFooter>
                </Modal>
                </>
              }

            </Card>

            <Card className="mb-3">
            <CardHeader>메모 등록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label>* 메모는 한 번 등록하면 삭제할 수 없습니다.</Label> <Button className="float-right mx-1 mb-3" color="secondary" onClick={this.handleMemoRegister}>등록</Button>
                      <Input type="textarea" name="text" value={this.state.memoText} onChange={this.handleMemo} placeholder="메모를 입력해 주세요."/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card className="mb-3">
            <CardHeader>메모</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    {
                      this.state.memos.map( (memo, i) => {

                        return (
                          <div key={memo.memoId}>
                            <Label><a href="#" onClick={this.handleMemoUser} value={memo.userId}>{memo.userLoginId}</a></Label>
                            <Label>&nbsp;&nbsp;{memo.regDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</Label>
                            <br/>
                            <Label dangerouslySetInnerHTML={{__html: memo.memo}}></Label>
                            <br/>
                            <br/>
                          </div>
                        )
                      })
                    }
                  </Col>
                </Row>
              </CardBody>
            </Card>

          </Col>
        </Row>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>미디어 자세히 보기</ModalHeader>
          <ModalBody>
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
          <ModalFooter>

            {/* <Button color="dark" onClick={this.downloadImage}>
              다운로드
            </Button> */}
            
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

      </Page>
    ); 
  }
};


export default FeedPage;
