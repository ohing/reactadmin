var express = require('express');
var router = express.Router();

const axios = require('axios')
const crypto = require('crypto')

const camelcase = require('camelcase-keys')

function getByteLength(s,b,i,c){
  for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
  return b;
}

router.get('/test', (req, res) => { 
  res.send(JSON.stringify(req.headers))
})

router.post('/sms', async (req, res) => {

  console.log(req.body)

  let smsHost = 'https://sens.apigw.ntruss.com'
  let smsPath = '/sms/v2/services/ncp:sms:kr:260973021178:ohingevent/messages'
  let accessKey = 'Q8DrLQzcgsfsCHsgJQbi'
  let secretKey = '2yRrhOROO3qi7Zx4UX9hUIvxnTK8Rtvfjj6w4LgZ'

  let recipients = req.body.recipients
  let content = req.body.content

  let length = getByteLength(content)
  let adulatedBytes = parseInt(length / 4 * 3)

  try {

    const params = {
      type: adulatedBytes > 70 ? 'LMS' : 'SMS',
      contentType: 'COMM',
      countryCode: '82',
      from: '07040105552',
      content: content,
      messages: recipients
    }
    
    const body = JSON.stringify(params)
    const timestamp = Date.now()

    const hmac = crypto.createHmac('sha256', secretKey);
    hmac.write(`POST ${smsPath}\n${timestamp}\n${accessKey}`)
    hmac.end()

    const signature = hmac.read().toString('base64')

    const result = await axios.post(`${smsHost}${smsPath}`, body, {
      headers: {
        'Content-Type': 'application/json',
        'x-ncp-apigw-timestamp': timestamp,
        'x-ncp-iam-access-key': accessKey,
        'x-ncp-apigw-signature-v2': signature
      }
    })
    console.log(result)
    res.send(JSON.stringify(result))

  } catch (e) {

    console.error(e)
    res.send(JSON.stringify(e))
  }
})

router.post('/email', async (req, res) => {

  res.send('')
})

module.exports = router