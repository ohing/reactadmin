import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  FormGroup,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class OhingOnListPage extends React.Component {

  state = {
    
    isUsing: '0',
    isOnGoing: '0',
    currentPage: 0,
    banners: [],
    presentedBanner: null,

    rowCount: 10,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,

    selectedIds: new Set(),

    imageModal: false,
    deleteModal: false
  };

  componentDidMount() {
    console.log('DidMount')

    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  } 
  
  getData = async (props) => {

    try {

      let result = await api.ohingOn().call({apiName: 'getBanners', method: 'get', parameters: {
        isUsing: this.state.isUsing,
        isOnGoing: this.state.isOnGoing,
        currentPage: this.state.currentPage, 
        rowCount: this.state.rowCount, 
      }})

      let totalCount = result.totalCount
      let banners = result.banners

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        banners: banners,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup,
        selectedIds: new Set([])
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })
      
    } catch (e) {

      console.log(e)
    }
  }

  deleteBanners = async () => {

    try {

      await api.OhingOn().call({apiName: 'deleteBanners', method: 'delete', parameters: {
        bannerIds: Array.from(this.state.selectedIds)
      }})

      this.getData()
      this.props.addNotification('삭제되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleAddButton = () => {
    this.props.history.push('/ohing-on-create')
  }

  handleOption = event => {
    this.setState({
      requestType: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleProducer = event => {
    this.setState({
      producer: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleUsing = event => {
    this.setState({
      isUsing: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleOnGoing = event => {
    this.setState({
      isOnGoing: event.target.value
    }, () => {
      this.getData()
    })
  }

  handleTitle = event => {
    event.preventDefault()
    let bannerId = parseInt(event.target.id.split(':')[1])
    let banner = this.state.banners.filter(banner => {
      return banner.bannerId == bannerId
    })[0]

    this.setState({
      presentedBanner: banner
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {
    if (!modalType) {
      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  handleLanding = event => {
    event.preventDefault()
    let bannerId = parseInt(event.target.id.split(':')[1])
    let banner = this.state.banners.filter(banner => {
      return banner.bannerId == bannerId
    })[0]
    switch (banner.landingTarget) {
      case 1: {
        this.props.history.push(`/feed/${banner.targetId}`)
        break
      }
      case 2: {
        this.props.history.push(`/user/${banner.targetId}`)
        break
      }
      case 3:
      case 4:
      case 7: {
        window.open(banner.targetUrl)
        break
      }

      default: {
        break
      }
    }
  }

  handleChecked = event => {
    let checked = event.target.checked
    let bannerId = parseInt(event.target.id.split(':')[1])
    let selectedIds = this.state.selectedIds
    if (checked) {
      selectedIds.add(bannerId)
    } else {
      selectedIds.delete(bannerId)
    }
    console.log(checked, bannerId, selectedIds)
    this.setState({
      selectedIds: selectedIds
    })
  }

  handleDeleteButton = () => {
    if (this.state.selectedIds.size > 0) {
      this.toggleDeleteModal()()
    }
  }

  toggleDeleteModal = modalType => () => {
    if (!modalType) {
      this.setState({
        deleteModal: !this.state.deleteModal,
      })
    }
  }

  handleDelete = () => {
    this.deleteBanners()
    this.setState({
      deleteModal: false
    })
  }

  handleEdit = event => {
    let bannerId = parseInt(event.target.id.split(':')[1])
    let banner = this.state.banners.filter(banner => {
      return banner.bannerId == bannerId
    })[0]
    this.props.history.push({
      pathname: `/ohing-on/${bannerId}`,
      state: {banner: banner}
    })
  }

  handleFix = event => {
    let noticeId = parseInt(event.target.id.split(':')[1])
    let notice = this.notice(noticeId)

    this.fix(noticeId, !notice.fixed)
  }

  handleUse = event => {
    let bannerId = parseInt(event.target.id.split(':')[1])
    let banner = this.state.banners.filter(banner => {
      return banner.bannerId == bannerId
    })[0]

    this.open(bannerId, banner.openYn == 'Y')
  }

  open = async (bannerId, isOpen) => {

    try {

      await api.ohingOn().call({apiName: isOpen ? 'closeBanner' : 'openBanner', method: 'post', pathParameter: bannerId})

      this.getData()
      this.props.addNotification('사용여부가 변경되었습니다.')

    } catch (e) {
      console.error(e)
    }
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="BannerListPage"
      >
        
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>What's Ohing On? 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>

                          <Input className="ml-3" type="select" value={this.state.isUsing} onChange={this.handleUsing}>
                            <option value="0">사용여부 전체</option>
                            <option value="1">사용함</option>
                            <option value="2">사용안함</option>
                          </Input>

                          <Input className="ml-3" type="select" value={this.state.isOnGoing} onChange={this.handleOnGoing}>
                            <option value="0">현재 노출여부 전체</option>
                            <option value="1">현재 노출중</option>
                            <option value="2">현재 노출안됨</option> 
                          </Input>
                          
                        </div>

                        {/* <div>
                          <Input
                            type="text"
                            name="text"
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                            onChange={this.handleSearchText}
                          />
                          <Button outline color="secondary" value={this.state.searchText} onClick={this.handleSearch}>검색</Button>
                        </div> */}
                      </Form>

                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="40"></th>
                            <th width="120">제목</th>
                            <th width="120">랜딩</th>
                            <th width="100">사용설정</th>
                            <th width="180">기간</th>
                            <th width="40">정렬</th>
                            <th width="100"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.banners.map( (banner, index) => {

                              let landingTarget
                              let targetId
                              let targetUrl
                              switch (banner.landingTarget) {
                                case 1: { 
                                  if (banner.targetId != null) {
                                    landingTarget = `글 상세-${banner.targetId }`
                                    targetId = banner.targetId 
                                  }
                                  break 
                                }
                                case 2: { 
                                  if (banner.targetId != null) {
                                    landingTarget = '프로필'
                                    targetId = banner.targetId 
                                  }
                                  break 
                                }
                                case 3: { 
                                  if (banner.targetUrl != null) {
                                    landingTarget = '내부 웹' 
                                    targetUrl = banner.targetUrl 
                                  }
                                  break 
                                }
                                case 4: { 
                                  if (banner.targetUrl != null) {
                                    landingTarget = '외부 웹' 
                                    targetUrl = banner.targetUrl 
                                  }
                                  break 
                                }
                                case 5: { 
                                  landingTarget = '글 작성' 
                                  break 
                                }
                                case 6: { 
                                  landingTarget = '도와줘 작성' 
                                  break 
                                }
                                case 7: { 
                                  if (banner.targetUrl != null) {
                                    landingTarget = '공지사항' 
                                    targetUrl = banner.targetUrl 
                                  }
                                  break 
                                }
                                case 8: { 
                                  if (banner.targetId != null) {
                                    landingTarget = `탭-${banner.targetId}`
                                  }
                                  break 
                                }

                                case 9: {
                                  landingTarget = '공지사항 홈'
                                }
                                default:
                                  break
                              }

                              

                              if (landingTarget === undefined) {
                                landingTarget = '이동안함'
                              }

                              return (
                                <>
                                  <tr>
                                    <td><Input type="checkbox" className="mx-auto align-middle" id={`bannerId:${banner.bannerId}`} onChange={this.handleChecked} checked={this.state.selectedIds.has(banner.bannerId)}/></td>
                                    <td className="text-truncate align-middle" style={{maxWidth:'240'}}><a href="#" id={`bannerId:${banner.bannerId}`} onClick={this.handleTitle}>{banner.bannerTitle}</a></td>
                                    <td className="align-middle">{targetId === undefined && targetUrl === undefined ? landingTarget : <a href="#" id={`banner:${banner.bannerId}`} onClick={this.handleLanding}>{landingTarget}</a>}</td>
                                    <td className="align-middle">{banner.openYn == 'Y' ? '사용함' : '사용안함'}</td>
                                    <td className="align-middle">{`${banner.bannerBeginDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '').substring(0, 16)} ~ ${banner.bannerEndDt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '').substring(0, 16)}`}</td>
                                    <td className="align-middle">{banner.bannerSortNo}</td>
                                    <td className="align-middle">
                                      <Button outline className="mx-1" id={`banner:${banner.bannerId}`} color="dark" onClick={this.handleEdit} style={{height: '32px'}}>편집</Button>
                                      <Button outline className="mx-1" id={`banner:${banner.bannerId}`} color={banner.openYn == 'Y' ? 'primary' : 'secondary'} onClick={this.handleUse} style={{height: '32px'}}>{banner.openYn == 'Y' ? '사용안함' : '사용'}</Button>
                                    </td>
                                  </tr>
                                  
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                    <Button className="mx-1" color="secondary" onClick={this.handleAddButton}>작성</Button>
                    <Button className="mx-1" color="primary" onClick={this.handleDeleteButton}>삭제</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>{'OHING ON 이미지'}</ModalHeader>
          <ModalBody>            
            <CardImg
              className="card-img ml-3 mb-2 rounded-circle border"
              src={(this.state.presentedBanner || {}).imageUrl}
              // key={media.mediaSeq}
              style={{ width: 430, height: 430, objectFit: 'cover', objectPosition: 'center'}}
            />
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.deleteModal}
          toggle={this.toggleDeleteModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleDeleteModal()}>{'항목 삭제'}</ModalHeader>
          <ModalBody>            
            <Label className="ml-2">선택된 OHING ON 항목을 삭제합니다.</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDeleteModal()}>
              취소
            </Button>
            <Button color="primary" onClick={this.handleDelete}>
              삭제
            </Button>{' '}
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default OhingOnListPage;
