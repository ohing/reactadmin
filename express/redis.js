const Redis = require('ioredis')
const fs = require('fs')
const net = require('net')
const { Client } = require('ssh2')

const RedisClient = class {
    
    #primaryHost = process.env.stage == 'prod' ? 'opr-sns-chat-cache-redis.onz7hw.ng.0001.apn2.cache.amazonaws.com' : 'dev-mqtt-redis-aws-001.onz7hw.0001.apn2.cache.amazonaws.com'
    #replicaHost = process.env.stage == 'prod' ? 'opr-sns-chat-cache-redis-ro.onz7hw.ng.0001.apn2.cache.amazonaws.com' : 'dev-mqtt-redis-aws-001.onz7hw.0001.apn2.cache.amazonaws.com'
    #port = '6379'

    createIntermediateServer = (connectionListener) => {
        return new Promise((resolve, reject) => {
            const server = net.createServer(connectionListener);
            server.once('error', reject);
            server.listen(0, () => resolve(server));
        });
    }

    connectToRedis = (options) => {
        const redis = new Redis(options);
        return new Promise((resolve, reject) => {
            redis.once('error', reject);
            redis.once('ready', () => resolve(redis));
        });
    }    

    #client = null
    #server = null

    client = (host, db) => {

        return new Promise( async (resolve, reject) => {

          if (process.cwd().includes('karl')) {

            let sshClient = new Client()

            sshClient.on('ready', async () => {

                let server

                try {
                    server = await this.createIntermediateServer(socket => {
                        sshClient.forwardOut(socket.remoteAddress, socket.remotePort, host, this.#port, (error, stream) => {
                            if (error) {
                                socket.end()
                                reject(error)
                            }
                            socket.pipe(stream).pipe(socket)
                        })
                    })

                } catch (e) {
                    reject(e)
                }

                try {
                    let redis = await this.connectToRedis({
                        host: '127.0.0.1',
                        port: server.address().port,
                        db: db
                    })

                    this.#server = server
                    this.#client = redis

                    resolve(redis)

                } catch (e) {
                    reject(e)
                }

            }).connect({
                host: '13.209.236.48',
                port: 22,
                username: 'ec2-user',
                privateKey: fs.readFileSync('./ohing-public.pem')
            })

          } else {

            console.log(host)

            try {
              let redis = await this.connectToRedis({
                host: host,
                port: this.#port,
                db: db
              })
              resolve(redis)
            } catch (e) {
              reject(e)
            }
          }
        })
    }
    
    readable(db) {
      return this.client(this.#replicaHost, db)        
    }
    
    writable(db) {
      return this.client(this.#primaryHost, db)
    }
}

module.exports = new RedisClient()
