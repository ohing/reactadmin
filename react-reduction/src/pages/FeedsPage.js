import React from 'react';

import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form,
  Input,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  CardImg,
  Icon
} from 'reactstrap';

import { MdPhoto, MdVideocam } from 'react-icons/md';

import Page from 'components/Page';
// import { NumberWidget, IconWidget } from 'components/Widget';

// import { iconWidgetsData, numberWidgetsData } from 'demos/widgetPage';

import API from 'utils/api'
const api = new API()

class FeedsPage extends React.Component {

  state = {
    currentPage: 0,
    feeds: [],
    rowCount: 10,
    status: 0,
    type: 0,
    anonymous: 0,
    reportedType: 0,
    pages: [],
    pagesInGroup: 10,
    previousPageEnabled: false,
    nextPageEnabled: false,
    searchText: '',
    imageModal: false
  };

  // mediaCloudfrontUrl = process.env.stage == 'PROD' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'
  mediaCloudfrontUrl = process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net'

  componentDidMount() {

    console.log('Stage::', process.env.REACT_STAGE)

    console.log('env', process.env.REACT_STAGE == 'prod' ? 'https://d1msh2x81vktjt.cloudfront.net' : 'https://d25s42vo6uiaxs.cloudfront.net')

    this.state.rowCount = parseInt((window.innerHeight - 508) / 52 + 1) 
    console.log('DidMount')
    this.fetchData()
  }

  componentWillUnmount() {
    console.log('WillUnmount')
  }

  fetchData() {

    const {history, location} = this.props

    if (!location.state) {
      this.getData()
    } else {
      this.setState({...location.state})
      history.replace(undefined, undefined)
    }
  }
  
  getData = async (props) => {

    try {

      let result = await api.feed().call({apiName: 'getFeeds', method: 'get', parameters: {
        currentPage: this.state.currentPage, 
        status: this.state.status,
        type: this.state.type, 
        anonymous: this.state.anonymous,
        reportedType: this.state.reportedType,
        rowCount: this.state.rowCount, 
        searchText: this.state.searchText
      }})

      let totalCount = result.totalCount
      let feeds = result.feeds

      let totalPage = parseInt(totalCount / this.state.rowCount) + (totalCount % this.state.rowCount > 0 ? 1 : 0)
      let pageGroup = parseInt(this.state.currentPage / this.state.pagesInGroup)
      let totalPageGroup = parseInt(totalPage / this.state.pagesInGroup) + (totalPage % this.state.pagesInGroup > 0 ? 1 : 0)

      let pages = []

      for (let i = 0; i < this.state.pagesInGroup; i++) {

        let page = pageGroup * this.state.pagesInGroup + i

        if (totalPage <= page) {
          break
        }

        pages.push(page)
      }

      this.setState({
        feeds: feeds,
        previousPageEnabled: pageGroup > 0,
        pages: pages,
        nextPageEnabled: pageGroup+1 < totalPageGroup
      }, () => {
        this.props.history.replace(undefined, { ...this.state })
      })

    } catch (e) {

      console.log(e)
    }
  }

  handleStatus = event => {

    let status = parseInt(event.target.value)
    console.log('status', status)

    this.state.status = status
    this.state.currentPage = 0
    this.getData()
  }

  handleType = event => {

    let type = parseInt(event.target.value)
    console.log('type', type)

    this.state.type = type
    this.state.currentPage = 0
    this.getData()
  }

  handleAnonymous = event => {

    let anonymous = parseInt(event.target.value)
    console.log('anonymous', anonymous)

    this.state.anonymous = anonymous
    this.state.currentPage = 0
    this.getData()
  }

  handleReportedType = event => {

    let reportedType = parseInt(event.target.value)
    console.log('reportedType', reportedType)

    this.state.reportedType = reportedType
    this.state.currentPage = 0
    this.getData()
  }

  handleSearchText = event => {
    let searchText = event.target.value
    this.setState({
      searchText: searchText
    })
  }

  handleKeyDown = event => {
    if (event.key == 'Enter') {
      event.preventDefault();
      this.handleSearch(event)
    }
  }

  handleSearch = event => {
    this.getData()
  }

  handlePrevious = event => {
    event.preventDefault();
    console.log('previous')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let previousGroup = pageGroup - 1
    let previousPage = previousGroup * this.state.pagesInGroup + (this.state.pagesInGroup - 1)
    this.state.currentPage = previousPage
    this.getData()
  }

  handlePage = event => {
    event.preventDefault();
    
    let page = parseInt(event.target.innerText) - 1
    console.log('page', page)

    if (this.state.currentPage == page) {
      return
    }

    this.state.currentPage = page
    this.getData()
  }

  handleNext = event => {
    event.preventDefault();
    console.log('next')
    let pageGroup = parseInt(this.state.currentPage / 10)
    let nextGroup = pageGroup + 1
    let nextPage = nextGroup * this.state.pagesInGroup
    this.state.currentPage = nextPage
    this.getData()
  }

  handleFeed = event => {
    event.preventDefault();
    let targetFeedId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/feed/${targetFeedId}`) 
  }

  handleUser = event => {
    event.preventDefault();
    let targetUserId = parseInt(event.target.getAttribute('value'))
    this.props.history.push(`/user/${targetUserId}`) 
  }

  handleReported = event => {
    event.preventDefault();
    let feedId = parseInt(event.target.getAttribute('value'))
  }

  handleContent = event => {
    event.preventDefault();
    let content = event.target.getAttribute('value')
    this.setState({
      currentContent: content
    }, () => {
      this.toggleContentModal()()
    })
  }

  toggleContentModal = modalType => () => {
    if (!modalType) {
      this.setState({
        contentModal: !this.state.contentModal,
      })
    }
  }

  handleMedia = event => {

    let feedId = parseInt(event.target.getAttribute('value'))
    let mediaUrl = event.target.getAttribute('src') 
    let mediaType = event.target.getAttribute('type') 
    let mediaSeq = parseInt(event.target.getAttribute('seq'))

    if (mediaType === 'MOVIE') {
      let url = new URL(mediaUrl)
      let paths = url.pathname.split('/')
      let fileName = paths[paths.length-1]
      let pureName = fileName.split('.')[0]
      mediaUrl = `${this.mediaCloudfrontUrl}/mov/${pureName}/mp4/${pureName}.mp4`
    }

    this.setState({
      currentMediaType: mediaType,
      currentMediaUrl: mediaUrl
    }, () => {
      this.toggleImageModal()()
    })
  }

  toggleImageModal = modalType => () => {

    if (!modalType) {

      this.setState({
        imageModal: !this.state.imageModal,
      })
    }
  }

  handleTag = event => {
    event.preventDefault()
    let tag = event.target.innerText.trim()
    tag = tag.substring(1, tag.length)
    console.log(tag)

    this.props.history.push(`/keyword/tag/${tag}`)
  }
  
  render() {

    console.log('render')

    return (
      <Page
        className="AdminPage"
      >
        <Row key='A'>
          <Col>
            <Card className="mb-3">
              <CardHeader>게시물 목록</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Form className="mb-3" inline style={{justifyContent: 'space-between'}}>
                        <div>
                          <Input type="select" value={this.state.status} onChange={this.handleStatus}>
                            <option value="0">상태</option>
                            <option value="1">정상</option>
                            <option value="2">차단</option>
                          </Input>
                          <Input className="ml-3" type="select" value={this.state.type} onChange={this.handleType}>
                            <option value="0">타입</option>
                            <option value="1">피드</option>
                            <option value="2">도와줘</option>
                          </Input>
                          <Input className="ml-3" type="select" value={this.state.anonymous} onChange={this.handleAnonymous}>
                            <option value="0">익명여부</option>
                            <option value="1">익명</option>
                            <option value="2">익명아님</option>
                          </Input>
                          <Input className="ml-3" type="select" value={this.state.reportedType} onChange={this.handleReportedType}>
                            <option value="0">신고처리상태</option>
                            <option value="1">미처리 있음</option>
                          </Input>
                        </div>

                        <div>
                          <Input
                            type="text"
                            name="text"
                            value={this.state.searchText}
                            onChange={this.handleSearchText}
                            placeholder="검색어"
                            className="mx-1"
                            onKeyDown={this.handleKeyDown}
                          />
                          <Button outline color="secondary" onClick={this.handleSearch}>검색</Button>
                        </div>
                      </Form>
                      <Table {...{ ['hover']: true }}>
                        <thead>
                          <tr>
                            <th width="62">ID</th>
                            <th width="60">상태</th>
                            <th width="70">타입</th>
                            <th width="80">익명여부</th>
                            <th width="60">작성자</th>
                            <th width="300">내용</th>
                            <th width="80">신고</th>
                            <th width="180">작성일</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.feeds.map( (feed, index) => {

                              let medias = feed.medias
                              let tags = feed.tags

                              return (
                                <>
                                  <tr style={(medias.length + tags.length) > 0 ? {borderBottom: 'none'} : {}}>
                                    <td><a href="#" onClick={this.handleFeed} value={feed.feedId}>{feed.feedId}</a></td>
                                    <td>{feed.isBlocked ? `차단(${(feed.blockDt || '').replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')})` : '정상'}</td>
                                    <td>{feed.type == 'HELP' ? '도와줘' : '피드'}</td>
                                    <td>{feed.isAnonymous ? '익명' : '익명아님'}</td>
                                    <td><a href="#" onClick={this.handleUser} value={feed.userId}>{feed.accountId}</a></td>
                                    <td className="text-truncate" style={{maxWidth:'340px'}}><a href="#" onClick={this.handleContent} value={feed.content}>{feed.content}</a></td>
                                    <td>{feed.reportedCount > 0 ? <a href="#" value={feed.feedId} onClick={this.handleReported}>{feed.uncompletedReportCount}/{feed.reportedCount}</a> : '0/0'}</td>
                                    <td>{feed.createdAt.replace(/-/gi, '.').replace('T', ' ').replace('.000Z', '')}</td>
                                  </tr>
                                  {
                                    tags.length == 0 ? '' :
                                      (
                                        <tr>
                                          <td colSpan="8" style={{border: 'none'}}> 
                                          <>
                                          {
                                            tags.map( (tag, index) => {
                                              return <a href="#" onClick={this.handleTag}>{` #${tag} `}</a> 
                                            })
                                          }
                                          </>
                                          </td>
                                        </tr>
                                      )
                                  }
                                  {
                                    medias.length == 0 ? '' : 
                                    (
                                      <tr>
                                        <td colSpan="8" style={{border: 'none'}}> 
                                        {
                                          medias.map( (media, index) => {

                                            let fileName = media.fileName
                                            let pureName = fileName.split('.')[0]

                                            let mediaUrl = media.type === 'MOVIE' 
                                            ? `${this.mediaCloudfrontUrl}/mov/${pureName}/jpg/${pureName}.0000000.jpg`
                                            : `${this.mediaCloudfrontUrl}/img/${pureName}/${fileName}`

                                            let icon = media.type === 'MOVIE'
                                            ? <MdVideocam className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,-280%)'}}/>
                                            : <MdPhoto className="nav-item-icon" style={{color: 'white', filter: 'drop-shadow(0px 0px 3px black)', transform: 'translate(-110%,-280%)'}}/>
                                            
                                            return (
                                              <>
                                                <CardImg
                                                  className="card-img ml-1 can-click"
                                                  src={mediaUrl}
                                                  value={feed.feedId}
                                                  key={media.mediaSeq}
                                                  seq={media.mediaSeq}
                                                  type={media.type}
                                                  style={{ width: 100, height: 100, objectFit: 'cover', objectPosition: 'center'}}
                                                  onClick={this.handleMedia}/>
                                                {icon}
                                              </>
                                            )
                                          })
                                        }
                                        </td>
                                      </tr>
                                    )  
                                  }
                                </>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                      
                      <nav aria-label="Page navigation">
                        <ul className="pagination" id="pagination" style={{justifyContent: 'center'}}>
                          <li className={this.state.previousPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Previous" onClick={this.handlePrevious}>
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>
                          {this.state.pages.map( (page, index) => {
                            return (
                              <li className={page == this.state.currentPage ? 'page-item active' : 'page-item'} key={page}>
                                <a className="page-link" href="#" onClick={this.handlePage}>{page+1}</a>
                              </li>
                            )
                          })}
                          <li className={this.state.nextPageEnabled ? "page-item" : "page-item disabled"}>
                            <a className="page-link" href="#" aria-label="Next" onClick={this.handleNext}>
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.contentModal}
          toggle={this.toggleContentModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleContentModal()}>텍스트 자세히 보기</ModalHeader>
          <ModalBody>
            <Label>
              {this.state.currentContent}
            </Label>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleContentModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.imageModal}
          toggle={this.toggleImageModal()}
          className={this.props.className}>
          <ModalHeader toggle={this.toggleImageModal()}>미디어 자세히 보기</ModalHeader>
          <ModalBody>
            {
              this.state.currentMediaType === 'MOVIE' ? 
              <video
                className="card-img ml-2 mt-2"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
                controls
              />
              :
              <CardImg
                className="card-img ml-2 mt-2 can-click"
                src={this.state.currentMediaUrl}
                style={{ width: 450, height: 'auto'}}
              /> 
            }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleImageModal()}>
              확인
            </Button>
            
          </ModalFooter>
        </Modal>
        
      </Page>
    ); 
  }
};

export default FeedsPage;
