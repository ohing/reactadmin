
export default class APIManager {

  #accountAPIClient = null
  account() {
    if (this.#accountAPIClient == null) {
      this.#accountAPIClient = new AccountAPIClient()
    }
    return this.#accountAPIClient
  }

  #dashboardAPIClient = null
  dashboard() {
    if (this.#dashboardAPIClient == null) {
      this.#dashboardAPIClient = new DashboardAPIClient()
    }
    return this.#dashboardAPIClient
  }

  #adminAPIClient = null
  admin() {
    if (this.#adminAPIClient == null) {
      this.#adminAPIClient = new AdminAPIClient()
    }
    return this.#adminAPIClient
  }

  #memoAPIClient = null
  memo() {
    if (this.#memoAPIClient == null) {
      this.#memoAPIClient = new MemoAPIClient()
    }
    return this.#memoAPIClient
  }

  #userAPIClient = null
  user() {
    if (this.#userAPIClient == null) {
      this.#userAPIClient = new UserAPIClient()
    }
    return this.#userAPIClient
  }

  #feedAPIClient = null
  feed() {
    if (this.#feedAPIClient == null) {
      this.#feedAPIClient = new FeedAPIClient()
    }
    return this.#feedAPIClient
  }

  #chatAPIClient = null
  chat() {
    if (this.#chatAPIClient == null) {
      this.#chatAPIClient = new ChatAPIClient()
    }
    return this.#chatAPIClient
  }

  #commentAPIClient = null
  comment() {
    if (this.#commentAPIClient == null) {
      this.#commentAPIClient = new CommentAPIClient()
    }
    return this.#commentAPIClient
  }

  #pushAPIClient = null
  push() {
    if (this.#pushAPIClient == null) {
      this.#pushAPIClient = new PushAPIClient()
    }
    return this.#pushAPIClient
  }

  #noticeAPIClient = null
  notice() {
    if (this.#noticeAPIClient == null) {
      this.#noticeAPIClient = new NoticeAPIClient()
    }
    return this.#noticeAPIClient
  }

  #keywordAPIClient = null
  keyword() {
    if (this.#keywordAPIClient == null) {
      this.#keywordAPIClient = new KeywordAPIClient()
    }
    return this.#keywordAPIClient
  }

  #partnerAPIClient = null
  partner() {
    if (this.#partnerAPIClient == null) {
      this.#partnerAPIClient = new PartnerAPIClient()
    }
    return this.#partnerAPIClient
  }

  #bannerAPIClient = null
  banner() {
    if (this.#bannerAPIClient == null) {
      this.#bannerAPIClient = new BannerAPIClient()
    }
    return this.#bannerAPIClient
  }

  #popupAPIClient = null
  popup() {
    if (this.#popupAPIClient == null) {
      this.#popupAPIClient = new PopupAPIClient()
    }
    return this.#popupAPIClient
  }

  #ohingOnAPIClient = null
  ohingOn() {
    if (this.#ohingOnAPIClient == null) {
      this.#ohingOnAPIClient = new OhingOnAPIClient()
    }
    return this.#ohingOnAPIClient
  }

  #reportAPIClient = null
  report() {
    if (this.#reportAPIClient == null) {
      this.#reportAPIClient = new ReportAPIClient()
    }
    return this.#reportAPIClient
  }

  #inquirementAPIClient = null
  inquirement() {
    if (this.#inquirementAPIClient == null) {
      this.#inquirementAPIClient = new InquirementAPIClient()
    }
    return this.#inquirementAPIClient
  }

  #utilAPIClient = null
  util() {
    if (this.#utilAPIClient == null) {
      this.#utilAPIClient = new UtilAPIClient()
    }
    return this.#utilAPIClient
  }
}

class APIClient {

  beginPath() {
    return ''
  }

  apiPath(apiName) {
    return ''
  }

  async call({apiName, method = 'get', pathParameter = null, parameters = null, isMultipart = false}) {

    return new Promise( (resolve, reject) => {

      let host = window.location.protocol + '//' + window.location.host;

      let path = this.apiPath(apiName, pathParameter)

      console.log(apiName, path)

      if (path == null) {
        resolve({})
        return
      }

      let urlString = `${host}/api${this.beginPath()}${path}`
      console.log('url', urlString)

      let url = new URL(urlString)

      console.log('api call', method, url, parameters)

      let headers = {
        'token': window.localStorage.getItem('token')
      }

      if (!isMultipart) {
        headers['Content-Type'] = 'application/json'
      }

      let options = {
        method: method,
        headers: headers
      }

      if (parameters != null) {
        if (method === 'get') {
          Object.keys(parameters).forEach(key => {
            url.searchParams.append(key, parameters[key])
          })
        } else {

          if (isMultipart) {
            let formData = new FormData()
            Object.keys(parameters).forEach(key => {
              formData.append(key, parameters[key])
            })
            options.body = formData
          } else {
            options.body = JSON.stringify(parameters)
          }
        }
      }

      fetch(url, options)
      .then(response => {
        if (response.status == 401) {
          window.localStorage.removeItem('token')
          window.localStorage.removeItem('userInfo')
          window.location.href = '/login'
          return null
        } else {
          return response.json()
        }
      })
      .then(json => {

        console.log(json)

        if (json != null) {
          resolve(json)
        } else {
          resolve({})
        }
      })
      .catch(e => {
        console.log(e)
        reject(e)
      })
    })
  }
}

class AccountAPIClient extends APIClient {

  beginPath() {
    return '/account'
  }

  apiPath(apiName) {
    return {
      signIn: '/',
      changePassword: '/password',
      signOut: '/sign-out'
    }[apiName]
  }
}

class DashboardAPIClient extends APIClient {

  beginPath() {
    return '/dashboard'
  }

  apiPath(apiName) {
    return {
      dashboardData: '/',
    }[apiName]
  }
}

class AdminAPIClient extends APIClient {

  beginPath() {
    return '/admin'
  }

  apiPath(apiName) {
    return {
      getAdmins: '/',
      createAdmin: '/',
      deleteAdmins: '/',
      checkAccountId: '/check-id'
    }[apiName]
  }
}

class MemoAPIClient extends APIClient {

  beginPath() {
    return '/memo'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      registerMemo: '/',
      getMemos: '/',
      deleteMemo: `/${pathParameter}`,
    }[apiName]
  }
}

class UserAPIClient extends APIClient {

  beginPath() {
    return '/user'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getUsers: `/`,
      getUser: `/${pathParameter}`,
      blockUser: `/${pathParameter}/block`,
      unblockUser: `/${pathParameter}/block`,
      imageBlock: `/${pathParameter}/image-block`,
      setBlackList: `/${pathParameter}/blacklist`,
      removeBlackList: `/${pathParameter}/blacklist`,
      deleteTag: `/${pathParameter}/tag`,
    }[apiName]
  }
}

class FeedAPIClient extends APIClient {

  beginPath() {
    return '/feed'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getFeeds: `/`,
      getFeed: `/${pathParameter}`,
      blockFeed: `/${pathParameter}/block`,
      unblockFeed: `/${pathParameter}/block`,
    }[apiName]
  }
}

class ChatAPIClient extends APIClient {

  beginPath() {
    return '/chat'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getOpenRooms: `/open-rooms`,
      getRoomInfo: `/rooms/${pathParameter}`,
      unblockRoom: `/rooms/${pathParameter}/block`,
      blockRoom: `/rooms/${pathParameter}/block`,
    }[apiName]
  }
}

class CommentAPIClient extends APIClient {

  beginPath() {
    return '/comment'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getComments: `/`,
    }[apiName]
  }
}

class PushAPIClient extends APIClient {

  beginPath() {
    return '/push'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getTesters: `/tester`,
      addTester: `/tester`,
      deleteTesters: `/tester`,
      testersToUse: `/tester/use`,
      testersToUnuse: `/tester/unuse`,
      getMessages: `/message`,
      createMessage: `/message`,
      deleteMessages: `/message`,
      sendTestMessage: `/message/${pathParameter}/to-tester`,
      sendWholeMessage: `/message/${pathParameter}/to-whole`,
      reserveMessage: `/message/${pathParameter}/reserve`,
      cancelReserve: `/message/${pathParameter}/reserve`,
    }[apiName]
  }
}

class NoticeAPIClient extends APIClient {

  beginPath() {
    return '/notice'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getNotices: `/`,
      createNotice: `/`,
      deleteNotices: `/`,
      editNotice: `/${pathParameter}`,
      fix: `/${pathParameter}/fix`,
      use: `/${pathParameter}/use`,
      getHTML: `/${pathParameter}/html`
    }[apiName]
  }
}

class KeywordAPIClient extends APIClient {

  beginPath() {
    return '/keyword'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getRecommendTags: `/recommend`,
      setRecommendTags: `/recommend`,
      getPopularTags: `/popular`,
      getSearchKeywords: `/search`,
      getBannedKeywords: `/banned`,
      createBannedKeyword: `/banned`,
      deleteBannedKeywords: `/banned`,
      getTagInfo: `/tag/${pathParameter}`,
    }[apiName]
  }
}

class PartnerAPIClient extends APIClient {
  beginPath() {
    return '/partner'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getPartners: `/`,
      createPartner: `/`,      
    }[apiName]
  }
}

class BannerAPIClient extends APIClient {
  beginPath() {
    return '/banner'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getBanners: `/`,
      createBanner: `/`,
      modifyBanner: `/${pathParameter}`,
      openBanner: `/${pathParameter}/open`,
      closeBanner: `/${pathParameter}/close`,
      deleteBanners: `/`,
    }[apiName]
  }
}

class PopupAPIClient extends APIClient {
  beginPath() {
    return '/popup'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getPopups: `/`,
      createPopup: `/`,
      modifyPopup: `/${pathParameter}`,
      openPopup: `/${pathParameter}/open`,
      closePopup: `/${pathParameter}/close`,
      deletePopups: `/`,
    }[apiName]
  }
}

class OhingOnAPIClient extends APIClient {
  beginPath() {
    return '/ohing-on'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getBanners: `/`,
      createBanner: `/`,
      modifyBanner: `/${pathParameter}`,
      openBanner: `/${pathParameter}/open`,
      closeBanner: `/${pathParameter}/close`,
      deleteBanners: `/`,
    }[apiName]
  }
}

class ReportAPIClient extends APIClient {
  beginPath() {
    return '/report'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getReports: `/`,
      completeReport: `/${pathParameter}/complete`,
      cancelCompleteReport: `/${pathParameter}/cancel-complete`
    }[apiName]
  }
}

class InquirementAPIClient extends APIClient {
  beginPath() {
    return '/inquirement'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      getInquirements: `/`,
      getInquirement: `/${pathParameter}`,
      check: `/${pathParameter}/check`,
      answer: `/${pathParameter}/answer`,
    }[apiName]
  }
}

class UtilAPIClient extends APIClient {

  beginPath() {
    return '/util'
  }

  apiPath(apiName, pathParameter = null) {
    return {
      sendSMS: `/sms`,
    }[apiName]
  }
}